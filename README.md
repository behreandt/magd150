# MAGD 150
## Introduction to Media Arts and Game Development
## Spring 2017

* Meetings
    * Lecture (01C): McGraw 101, Friday 12:30 p.m. - 01:45 pm
    * Lab Section (01): McGraw 127, Monday 12:30 p.m. - 01:45 p.m.
    * Lab Section (02): McGraw 127, Wednesday 12:30 p.m. - 01:45 p.m.

* Instructor: Jeremy Behreandt
* Office: Andersen Library - Original Building 1217J
* Office Hours: Wednesday, Friday 09:00 a.m. - 11:30 a.m.
    * Also available by appointment
* Email: behreandjj28@uww.edu
* Course Website
    * Primary: Desire 2 Learn
    * Secondary: [Bitbucket](https://bitbucket.org/behreandt/magd150/)

### Course Description

This course provides an overview of multimedia design and explores the nonlinear historical development from old to new media. Introduction to Media Arts and Game Development is an interdisciplinary course: students majoring in MAGD will take this course to prepare for advanced projects in the curriculum; students majoring in a different field will be encouraged to develop literacy in new media and digital technology.

Aspects of the discipline to be surveyed by this course include infrastructural and technological influence on new media; design, planning and production of multimedia; video games; and the formation of culture around new media artifacts.

Assignments are structured to build student skills in project management, symbolic logic, visual design and basic software programming. In weekly lab sections, students will learn to program in Processing.

### Course Objectives

During the semester, students will be encouraged to consider the following:

* Which principles of design are best suited to the creation of new media?
* How will an understanding of the history of media inform its future role in society?
* How does user interactivity change the production of cultural artifacts?
* How is artistic practice influenced by technology and communications media?
* What is the ethical relationship between creator and consumer of new media?

By the course's end, students will be able to

* present and defend ideas on new media through oral presentation and discussion;
* use media tools to present ideas, provoke emotional response and elicit discussion;
* plan and execute key stages in the design process from design documentation to user experience testing to presentation of final product;
* apply programming concepts to build interactive experiences;

### Course Materials

The primary tool for this course is the Processing Integrated Development Environment (IDE). Processing is available on the computers in McGraw 127. Both the IDE itself and supporting documentation (examples, tutorials) are available for download at [https://processing.org/](https://processing.org/).

The rental textbook for this course at the [UWW Bookstore](http://www.uwwhitewaterbookstore.com/) is Daniel Shiffman's [_Learning Processing_](http://learningprocessing.com/) 2nd edition (2015).

Students are encouraged to make frequent backups of digital work (using flash drives, external hard drives, email and/or other source control methods) and to maintain an archive of coursework using University provided storage on [Google Drive](http://www.uww.edu/icit/services/google).

This is not to be construed as a comprehensive list of necessary hardware or software. If the production of additional media are required, it is left to student discretion which tool set best suits his or her artistic process.

### Etiquette

All students enrolled in this course are encouraged and expected to take ownership of it, and as such to act as active producers of - rather than as passive receptacles for - knowledge. A few guidelines to facilitate this ownership are as follows:

* There are multiple alternatives to learning in a traditional classroom. It is assumed that you have opted into this course not only to learn the subject matter, but because a face-to-face, collaborative learning environment suits your learning style and personal growth. By not attending labs and lectures, you inhibit the education of other students in withdrawing your knowledge and perspective. Thus, you are expected to attend lectures and labs.
    * Medical and family emergencies will be considered excused absences.

* Please consider the heart of what it means to 'attend'. If you plan to attend, arrive at the agreed upon meeting time; if you must leave before the conclusion, do so discreetly. If another class member has been acknowledged by the room, then to interrupt, ignore or otherwise undermine their address is disrespectful. Even though digital devices such as smart phones and laptops are integral to coursework, they also provide numerous distractions; using them inappropriately will be considered disrespectful. Students found to disrupt the classroom environment will be asked to leave.

* Electronic communications between you, as student, and myself, as instructor, are to be considered semi-formal, and as contributing to this course's records in the event of any dispute. As such, please use only your institutionally provided (UWW) email account and direct correspondence to my UWW email. In the subject, include the class name and topic (e.g., "MAGD150: Assignment clarification"); in the body, adhere to conventional English grammar and syntax, beginning with a salutation and concluding with a valediction.

* Since topics covered in this course are of a technical nature, observe common practices when asking others to assist with problematic code via email:
    * Either include the complete code or provide a link to it so that the recipient can run your program.
    * Detail the results you expected and the difference between expected and actual results. If necessary, include screen captures, copy Exceptions and/or stack traces from logs or consoles.
    * List any solutions you've attempted, including documentation you may have referenced.
    * Separate large problems or questions into smaller, more specific ones, which can be addressed one at a time.

### Academic Misconduct

To uphold the goal of this course as a one in which students learn to create digital artwork, any code or media contained within an assignment submitted for evaluation are presumed to be the work of the student unless attributed. 'Media' may include image, sound or other digitally storable work. Attribution should include the proper name of an author if available, as well as a Uniform Resource Locator (URL) leading to evidence that the resource is in the intellectual commons (i.e., not copyrighted or registered as a trademark). Students who violate academic integrity will be subject to procedures outlined in UWS Chapter 14, which may include being asked to redo an assignment, receiving a failing grade in the assignment or in the course.

### UWW Statement

The University of Wisconsin-Whitewater is dedicated to a safe, supportive and non-discriminatory learning environment.� It is the responsibility of all undergraduate and graduate students to familiarize themselves with University policies regarding Special Accommodations, Academic Misconduct, Religious Beliefs Accommodation, Discrimination and Absence for University Sponsored Events (for details please refer to the Schedule of Classes; the 'Rights and Responsibilities' section of the Undergraduate Catalog; the Academic Requirements and Policies and the Facilities and Services sections of the Graduate Catalog; and the 'Student Academic Disciplinary Procedures ([UWS Chapter 14](https://docs.legis.wisconsin.gov/code/admin_code/uws/14)); and the 'Student Nonacademic Disciplinary Procedures' ([UWS Chapter 17](https://docs.legis.wisconsin.gov/code/admin_code/uws/17/Title)).

### Grading Policy

It is your responsibility to track your performance over the course of the semester. Grades will be posted on D2L for your review. Requirements for the successful completion of an assignment will be enumerated and assigned point values.

This course engages with the aesthetic dimension of human experience, wherein the imagination plays a strong role; some requirements will be less specific than others, and taste may play a role in their evaluation. Please ask for clarification of requirements in advance of the assignment deadline.

If you feel that I have made an error recording your grade, please email me to politely request either an explanation or correction.

Since this course aims to acclimate you to project management and to the expectation that you deliver a product to a client according to specifications by the agreed-upon deadline, __late assignments will not be accepted.__

Letter | Lower Bound | Upper Bound | Interpretation
------ | -----------: | -----------: | --------------
A | 94 | 100 | Outstanding achievement; student performance surpasses course expectations.
A- | 90 | 93 | Excellent performance; student clearly exceeds course requirements.
B+ | 87 | 89 | High achievement; student substantially meets requirements and criteria.
B | 84 | 86 | Good work; student meets the course requirements and criteria.
B- | 80 | 83 | Most of the course requirements and criteria are met.
C+ | 77 | 79 | Acceptable performance in the class; student meets nearly all course requirements and criteria.
C | 74 | 76 | Average performance.
C- | 70 | 73 | Below average performance.
D+ | 67 | 69 | Below average performance.
D | 64 | 66 | Below average performance.
D- | 60 | 63 | Below average performance.
F | 0 | 59 | Poor/Fail

### Assignment Schedule

Subject to change per discretion of the instructor.

Unit | Wednesday Lab | Monday Lab | Description | Points
----: | ------------- | ---------- | ----------- | ------:
01 | 01/25/2017 | 01/30/2017 | Beginnings | 100
02 | 02/01/2017 | 02/06/2017 | Color | 100
03 | 02/08/2017 | 02/13/2017 | Math | 100
04 | 02/15/2017 | 02/20/2017 | Dynamism | 100
05 | 02/22/2017 | 02/27/2017 | Intersections | 100
06 | 03/01/2017 | 03/06/2017 | Organization | 100
07 | 03/08/2017 | 03/13/2017 | Transformation | 100
08 | 03/15/2017 | 03/27/2017 | Collection | 100
09 | 03/29/2017 | 04/03/2017 | Type | 100
10 | 04/05/2017 | 04/10/2017 | 3D | 100
 | 04/12/2017 | 04/17/2017 | Project Design Document | 100
 | 04/19/2017 | 04/24/2017 | Project (Due 05/05/2017) | 200
 | 05/08/2017 | 05/08/2017 | [Final Exam](http://www.uww.edu/Documents/registrar/Schedule_of_Classes/Spring_2017/2017%20Spring%20Exam%20Schedule.pdf) (12:15 - 2:15 p.m.) | 240
 | | | Attendance | 260
 | | | Total | 1800

### Lecture Schedule

Subject to change per discretion of the instructor.

Week | Date | Topic
----: | ---- | -----
01 | 01/20/2017 | What are New Media?
02 | 01/27/2017 | Design: Intro
03 | 02/03/2017 | Design: Visual
04 | 02/10/2017 | Design: Motion
05 | 02/17/2017 | Game Day
06 | 02/24/2017 | Sick Day
07 | 03/03/2017 | Design: Interaction
08 | 03/10/2017 | History: Literacy
09 | 03/17/2017 | History: Information
10 | 03/31/2017 | Game Day
11 | 04/07/2017 | History: Media Art
12 | 04/21/2017 | History: Video Games
13 | 04/28/2017 | Review
14 | 05/05/2017 | Project Presentation
