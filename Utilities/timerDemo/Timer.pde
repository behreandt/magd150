class Timer {
  String name;
  int start, stop, elapsed, 
    scheduledStart, scheduledStop;
  boolean started, stopped, 
    startScheduled, stopScheduled;

  // Constructors.
  Timer() {
    this("", 0, 0);
  }

  Timer(int startSched) {
    this("", startSched, 0);
  }

  Timer(int startSched, int stopSched) {
    this("", startSched, stopSched);
  }

  Timer(String n) {
    this(n, 0, 0);
  }

  Timer(String n, int startSched) {
    this(n, startSched, 0);
  }

  // Primary constructor, to which previous constructors
  // defer. If the name is an empty String, the timer
  // will be named after the class.
  Timer(String n, int startSched, int stopSched) {
    name = n != "" ? n : getClass().getSimpleName();
    if (startSched > 0) {
      scheduleStart(startSched);
    }
    if (stopSched > startSched) {
      scheduleStop(stopSched);
    }
  }

  // To make it easier to view diagnostic information about
  // the time, its toString method has been overridden.
  public String toString() {
    return String.format("name:\t%1s"
      + "\nstop:\t%2$06d"
      + "\nstart:\t%3$06d"
      + "\nelapsed:\t%4$06d"
      + "\nscheduled start:\t%5$06d"
      + "\nscheduled stop:\t%6$06d", 
      name, stop, start, elapsed(), scheduledStart, scheduledStop);
  }

  int elapsed() {
    if (started && stopped) {
      return stop - start;
    } else if (started && !stopped) {
      return millis() - start;
    } else if (!started && !stopped) {
      return 0;
    }
    return -1;
  }

  void reset() {
    start = 0;
    stop = 0;
    scheduledStart = 0;
    scheduledStop = 0;
    started = false;
    stopped = false;
    startScheduled = false;
    stopScheduled = false;
  }

  int start() {
    if (!started && !stopped) {
      start = millis();
      started = true;
      scheduledStart = 0;
      startScheduled = false;
    } else if (started) {
      reset();
      start = millis();
      started = true;
      scheduledStart = 0;
      startScheduled = false;
    }
    return start;
  }

  int start(int seconds) {
    start();
    scheduleStop(seconds);
    return start;
  }

  int stop() {
    if (started && !stopped) {
      stop = millis();
      stopped = true;
      scheduledStop = 0;
      stopScheduled = false;
    }
    return stop;
  }

  void tick() {
    if (!started
      && startScheduled
      && millis() >= scheduledStart) {
      start();
    }
    if (!stopped
      && stopScheduled
      && millis() >= scheduledStop) {
      stop();
    }
  }

  int scheduleStart(int seconds) {
    if (!started && !stopped) {
      scheduledStart = millis() + (seconds * 1000);
      startScheduled = true;
    }
    return scheduledStart;
  }

  int scheduleStop(int seconds) {
    if (!stopped) {
      scheduledStop = millis() + (seconds * 1000);
      stopScheduled = true;
    }
    return scheduledStop;
  }
}