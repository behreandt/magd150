// Create a variable of the type Timer.
Timer timer;
int secondsToStop = 5;

void setup() {
  surface.setTitle("Timer Demo");
  surface.setResizable(true);
  pixelDensity(displayDensity());
  size(420, 420);
  background(64);
  noStroke();
  textAlign(RIGHT, TOP);
  textSize(16);
  frameRate(60);

  // Construct the timer. You can optionally
  // give it a name, a scheduled start and
  // a scheduled stop.
  //timer = new Timer();
  timer = new Timer("Diagnostic", 5, 8);
}

void draw() {
  timer.tick();
  background(32);

  // Change the color of the text based on the status
  // of the timer.
  if (timer.started && timer.stopped) {
    fill(255, 0, 0);
  } else if (timer.started && !timer.stopped) {
    fill(0, 255, 0);
  } else {
    fill(255, 255, 0);
  }
  text(timer.toString(), width, height / 2.0);

  // For purposes of comparison, show default clock.
  fill(255);
  text("frames: " + frameCount
    + "\nmilliseconds: " + millis()
    + "\nfps: " + nfs(frameRate, 0, 2), width, 0);
}

void mouseReleased() {
  timer.start(secondsToStop);
}