// Declare a variable of the type Camera.
Camera cam;
KeyListener kl;
PVector position;
float speed = 1.5;
PFont geneva;
PMatrix defaultMatrix;

void setup() {
  surface.setTitle("Camera Follow Demo");
  pixelDensity(displayDensity());
  size(640, 420, P3D);
  background(32);

  defaultMatrix = getMatrix();
  position = new PVector(width * 0.5, height * 0.5);
  geneva = loadFont("Geneva-16.vlw");

  // Create a key listener which will control the camera's
  // zoom and manual pan functionality.
  kl = new KeyListener();
  // Create a camera which is controlled by the key listener
  // created above.
  cam = new Camera(kl);
}

void draw() {
  background(32);
  // Draws a checkered floor for reference.
  floor(12);
  // Draws an ellipse controlled by key press.
  mover();

  // The argument given to pan will be the position toward
  // which the camera moves when a pan key is not being pressed.
  cam.pan(position);
  // Updates the camera.
  cam.draw();

  // Draws a blue hello world box.
  gui();
}

void floor(int tiles) {
  pushStyle();
  noStroke();
  rectMode(CORNER);
  float w = width / float(tiles);
  float h = height / float(tiles);
  for (int y = 0; y < tiles; ++y) {
    for (int x = 0; x < tiles; ++x) {
      fill(x % 2 == y % 2 ? 64 : 0);
      rect(x * w, y * h, w, h);
    }
  }
  popStyle();
}

void mover() {
  if (kl.get(87)) {
    position.y = position.y >= 0
      ? position.y - speed
      : 0;
  }

  if (kl.get(65)) {
    position.x = position.x >= 0
      ? position.x - speed
      : 0;
  }

  if (kl.get(68)) {
    position.x = position.x <= width
      ? position.x + speed
      : width;
  }

  if (kl.get(83)) {
    position.y = position.y <= height
      ? position.y + speed
      : height;
  }

  pushStyle();
  fill(0, 127, 255);
  noStroke();
  ellipse(position.x, position.y, 30, 30);
  popStyle();
}

void gui() {
  pushMatrix();
  pushStyle();
  hint(DISABLE_DEPTH_TEST);
  setMatrix(defaultMatrix);
  textSize(16);
  textFont(geneva);
  textAlign(LEFT, TOP);
  strokeWeight(3);
  stroke(255);
  fill(0, 0, 204, 127);
  rectMode(CORNERS);
  rect(10, 10, width - 10, 100, 5);
  fill(0);
  text("HELLO\nWORLD", 16, 16);
  fill(255);
  text("HELLO\nWORLD", 15, 15);
  hint(ENABLE_DEPTH_TEST);
  popStyle();
  popMatrix();
}

void keyPressed() {
  kl.keyPressed(keyCode);
}

void keyReleased() {
  kl.keyReleased(keyCode);
}