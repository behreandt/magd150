class Camera {
  PVector pos, 
    lookAt, 
    up = new PVector(0, 1, 0);
  float fov = PI / 3.0, 
    aspect, 
    panSpeed = 0.05, 
    zoomSpeed = 0.025, 
    defaultZPos, 
    overviewZPos, 
    nearClip, 
    farClip, 
    freeCamPan;
  int panUp = UP, 
    panLeft = LEFT, 
    panDown = DOWN, 
    panRight = RIGHT, 
    zoomKey = 32;
  boolean isOrthographic;
  KeyListener kl;

  Camera() {
    this(null, 
      new PVector(width / 2.0, height / 2.0, height / 1.155), 
      new PVector(width / 2.0, height / 2.0, 0));
  }

  Camera(KeyListener _kl) {
    this(_kl, 
      new PVector(width / 2.0, height / 2.0, height / 1.155), 
      new PVector(width / 2.0, height / 2.0, 0));
  }

  Camera(PVector _pos, PVector _lookAt) {
    this(null, _pos, _lookAt);
  }

  Camera(KeyListener _kl, PVector _pos, PVector _lookAt) {
    pos = _pos;
    lookAt = _lookAt;
    defaultZPos = pos.z;
    overviewZPos = defaultZPos * 7.5;
    freeCamPan = height / 60.0;
    aspect = width / float(height);
    nearClip = pos.z / 10.0;
    farClip = pos.z * 10.0;
    kl = _kl;
    if (kl != null) { 
      kl.add(panUp, panLeft, panDown, panRight, zoomKey);
    }
  }

  void draw() {
    if (kl != null) { 
      zoom();
    }
    if (isOrthographic) {
      ortho();
    } else {
      perspective(fov, 
        aspect, 
        nearClip, 
        farClip);
    }
    camera(pos.x, pos.y, pos.z, 
      lookAt.x, lookAt.y, lookAt.z, 
      up.x, up.y, up.z);
  }

  void pan(float x, float y) {
    pan(new PVector(x, y));
  }

  // Recenter on the target if no pan keys are being
  // pressed.
  void pan(PVector target) {
    if (kl != null
      && (kl.get(panUp) || kl.get(panDown)
      || kl.get(panLeft) || kl.get(panRight))) {
      if (kl.get(panUp)) {
        pos.y -= this.freeCamPan;
      }

      if (kl.get(panLeft)) {
        pos.x -= this.freeCamPan;
      }

      if (kl.get(panDown)) {
        pos.y += this.freeCamPan;
      }
      if (kl.get(panRight)) {
        pos.x += this.freeCamPan;
      }
    } else {
      pos.x = lerp(pos.x, target.x, panSpeed);
      pos.y = lerp(pos.y, target.y, panSpeed);
    }
    lookAt.x = pos.x;
    lookAt.y = pos.y;
  }

  // Transition to the default or overview z position
  // depending on whether or not the zoom key is pressed.
  void zoom() {
    if (kl.get(zoomKey)) {
      pos.z += (overviewZPos - pos.z) * zoomSpeed;
    } else {
      pos.z += (defaultZPos - pos.z) * zoomSpeed;
    }
  }
}