/* Press Q, W, E to randomize the color of each light.
* Press A, S, D to turn the light on or off.
*/

Light light1;
Light light2;
Light light3;

float a;

void setup() {
  size(640, 420, P3D);
  background(64);
  noStroke();

  light1 = new Light(Lights.Directional, 
    0, 54, 127);
  light2 = new Light(Lights.Ambient, 
    0, 127, 54);
  light3 = new Light(Lights.Point, 
    127, 54, 0, 
    width / 2.0, height, 0);
}

void draw() {
  background(32);

  light3.x = mouseX;
  light3.y = mouseY;

  light1.draw();
  light2.draw();
  light3.draw();

  pushMatrix();
  translate(width * 0.25, height / 2.0, -height / 1.5);
  sphere(height / 3.0);
  popMatrix();

  pushMatrix();
  translate(width * 0.75, height / 2.0, -height / 1.5);
  rotateY(a);
  rotateZ(a);
  box(height / 3.0);
  popMatrix();

  a += 0.01;
}

void keyPressed() {
  if (key == 'A' || key == 'a') {
    light1.on = !light1.on;
  } else if (key == 'S' || key == 's') {
    light2.on = !light2.on;
  } else if (key == 'D' || key == 'd') {
    light3.on = !light3.on;
  } else if (key == 'Q' || key == 'q') {
    light1.setColor(random(127), random(127), random(127));
    println(light1);
  } else if (key == 'W' || key == 'w') {
    light2.setColor(random(54), random(54), random(54));
    println(light2);
  } else if (key == 'E' || key == 'e') {
    light3.setColor(random(127), random(127), random(127));
    println(light3);
  }
}