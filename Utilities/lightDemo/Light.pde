enum Lights {
  Ambient, Directional, Point, Spot
}

class Light {
  private float r = 255, g = 244, b = 214, 
    x, y, z, 
    nx = -0.5, ny = 0.5, nz = -0.5, 
    angle = QUARTER_PI, concentration = 600, 
    specr = 128, specg = 128, specb = 128, 
    lightFallConst = 1, lightFallLin, lightFallQuad;
  private Lights type;
  private boolean on = true;

  public Light() {
    this(Lights.Directional, 
      255, 244, 214, 
      width / 2.0, height / 2.0, 0, 
      -0.5, 0.5, -0.5, 
      QUARTER_PI, 600);
  }

  public Light(Lights t) {
    this(t, 255, 244, 214, 
      width / 2.0, height / 2.0, 0, 
      -0.5, 0.5, -0.5, 
      QUARTER_PI, 600);
  }

  public Light(Lights t, float _r, float _g, float _b) {
    this(t, _r, _g, _b, 
      width / 2.0, height / 2.0, 0, 
      -0.5, 0.5, -0.5, 
      QUARTER_PI, 600);
  }

  public Light(Lights t, color c) {
    this(t, c >> 16 & 0xff, c >> 8 & 0xff, c & 0xff, 
      width / 2.0, height / 2.0, 0, 
      -0.5, 0.5, -0.5, 
      QUARTER_PI, 600);
  }

  public Light(Lights t, color c, 
    float _x, float _y, float _z) {
    this(t, c >> 16 & 0xff, c >> 8 & 0xff, c & 0xff, 
      _x, _y, _z, 
      _x, _y, _z, 
      QUARTER_PI, 600);
  }

  public Light(Lights t, 
    float _r, float _g, float _b, PVector v) {
    this(t, _r, _g, _b, 
      v.x, v.y, v.z, 
      v.x, v.y, v.z, 
      QUARTER_PI, 600);
  }

  public Light(Lights t, color c, PVector v) {
    this(t, c >> 16 & 0xff, c >> 8 & 0xff, c & 0xff, 
      v.x, v.y, v.z, 
      v.x, v.y, v.z, 
      QUARTER_PI, 600);
  }

  public Light(Lights t, color c, PVector v, PVector n, 
    float _angle, float _concentration) {
    this(t, c >> 16 & 0xff, c >> 8 & 0xff, c & 0xff, 
      v.x, v.y, v.z, 
      n.x, n.y, n.z, 
      _angle, _concentration);
  }

  public Light(Lights t, 
    float _r, float _g, float _b, 
    float _x, float _y, float _z) {
    this(t, _r, _g, _b, 
      _x, _y, _z, 
      _x, _y, _z, 
      QUARTER_PI, 600);
  }

  public Light(Lights t, 
    float _r, float _g, float _b, 
    float _x, float _y, float _z, 
    float _nx, float _ny, float _nz, 
    float _angle, float _concentration) {
    r = _r;
    g = _g;
    b = _b;
    x = _x;
    y = _y;
    z = _z;
    nx = constrain(_nx, -1, 1);
    ny = constrain(_ny, -1, 1);
    nz = constrain(_nz, -1, 1);
    angle = _angle;
    concentration = _concentration;
    type = t;
  }

  public String toString() {
    if (type == Lights.Ambient || type == Lights.Point) {
      return "[" + type
        + ",\n" + r + ", " + g +", " + b
        + ",\n" + x + ", " + y + ", " + z + "]";
    } else if (type == Lights.Directional) {
      return "[" + type
        + ",\n" + r + ", " + g +", " + b
        + ",\n" + nx + ", " + ny + ", " + nz + "]";
    } else {
      return "[" + type
        + ",\n" + r + ", " + g +", " + b
        + ",\n" + x + ", " + y + ", " + z
        + ",\n" + nx + ", " + ny + ", " + nz
        + ",\n" + angle + ", " + concentration + "]";
    }
  }

  public void draw() {
    if (on) {
      lightSpecular(specr, specg, specb);
      lightFalloff(lightFallConst, lightFallLin, lightFallQuad);
      if (type == Lights.Ambient) {
        ambientLight(r, g, b, x, y, z);
      } else if (type == Lights.Point) {
        pointLight(r, g, b, x, y, z);
      } else if (type == Lights.Spot) {
        spotLight(r, g, b, x, y, z, nx, ny, nz, 
          angle, concentration);
      } else if (type == Lights.Directional) {
        directionalLight(r, g, b, nx, ny, nz);
      }
    }
  }

  public void setColor(float _r, float _g, float _b) {
    r = _r;
    g = _g;
    b = _b;
  }

  public void setColor(color c) {
    r = c >> 16 & 0xff; 
    g = c >> 8 & 0xff;
    b = c & 0xff;
  }

  public void setPos(float _x, float _y, float _z) {
    x = _x;
    y = _y;
    z = _z;
  }

  public void setPos(PVector v) {
    x = v.x;
    y = v.y;
    z = v.z;
  }

  public void setDir(float _nx, float _ny, float _nz) {
    nx = constrain(_nx, -1, 1);
    ny = constrain(_ny, -1, 1);
    nz = constrain(_nz, -1, 1);
  }

  public void setDir(PVector n) {
    nx = constrain(n.x, -1, 1);
    ny = constrain(n.y, -1, 1);
    nz = constrain(n.z, -1, 1);
  }

  public void setSpecular(float _r, float _g, float _b) {
    specr = _r;
    specg = _g;
    specb = _b;
  }

  public void setSpecular(color c) {
    specr = c >> 16 & 0xff; 
    specg = c >> 8 & 0xff;
    specb = c & 0xff;
  }

  public void setFalloff(float constant, float linear, float quadratic) {
    lightFallConst = constant;
    lightFallLin = linear;
    lightFallQuad = quadratic;
  }

  public color getColor() {
    return color(r, g, b);
  }

  public PVector getPos() {
    return new PVector(x, y, z);
  }

  public PVector getDir() {
    return new PVector(nx, ny, nz);
  }

  public color getSpecular() {
    return color(specr, specg, specb);
  }
}