class KeyListener {
  private HashMap<Integer, Boolean> pressed = 
    new HashMap<Integer, Boolean>();

  public KeyListener() {
    this(UP, DOWN, LEFT, RIGHT, SHIFT);
  }

  public KeyListener(Integer... admissibleKeys) {
    int size = admissibleKeys.length;
    for (int i = 0; i < size; ++i) {
      pressed.put(admissibleKeys[i], false);
    }
  }
  
  public String toString() {
    return pressed.toString();
  }

  public Boolean get(Integer kc) {
    return pressed.containsKey(kc) ? pressed.get(kc) : false;
  }

  public void add(Integer... newKeys) {
    int size = newKeys.length;
    for (int i = 0; i < size; ++i) {
      pressed.put(newKeys[i], false);
    }
  }

  public void keyPressed(Integer kc) {
    pressed.put(kc, true);
  }

  public void keyReleased(Integer kc) {
    pressed.put(kc, false);
  }
}