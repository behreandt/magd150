public KeyListener kl;
public FirstPersonController fps;
public int tiles;
public float scale, a;

public void setup() {
  surface.setTitle("First Person Camera Demo");
  pixelDensity(displayDensity());
  size(640, 420, P3D);
  background(0);
  noStroke();
  kl = new KeyListener();
  fps = new FirstPersonController(kl);
  tiles = 16;
  scale = 500;
  surface.setTitle("FPS Camera");
}

public void draw() {
  background(32);
  lights();
  drawFloor(tiles, scale);
  fps.draw();
}

public void keyPressed() {
  kl.keyPressed(keyCode);
}

public void keyReleased() {
  kl.keyReleased(keyCode);
}

public void drawFloor(int tiles, float scale) {
  float x = 0, 
    z = 0, 
    min = tiles * -scale * 0.5, 
    max = tiles * scale * 0.5;
  for (int i = 0; i < tiles; ++i) {
    for (int j = 0; j < tiles; ++j) {

      if (i % 2 == j % 2) {
        fill(map(i, 0, tiles, 0, 255), map(j, 0, tiles, 0, 255), 127);
      } else {
        fill(0, map(i, 0, tiles, 255, 127), map(j, 0, tiles, 255, 127));
      }

      x = map(i, 0, tiles, min, max);
      z = map(j, 0, tiles, min, max);

      beginShape();
      emissive(54, 54, 54);
      vertex(x, height, z);
      vertex(x + scale, height, z);
      vertex(x + scale, height, z + scale);
      vertex(x, height, z + scale);
      endShape(CLOSE);
    }
  }
}