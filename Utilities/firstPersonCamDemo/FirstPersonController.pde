class FirstPersonController {
  PVector pos, 
    lookAt, 
    up = new PVector(0, 1, 0);
  float fov = PI / 3.0, 
    aspect, 
    nearClip, 
    farClip, 
    moveSpeed = 12.5, 
    radius = 300, 
    avatarHeight = 400, 
    incr = 0.01, 
    angle, 
    pitchConstraint, 
    lookSmoothing = 0.025, 
    yawConstraint, 
    leftDeadZone, rightDeadZone;
  boolean noCursor = false;
  int leftKey = 65, 
    rightKey = 68, 
    backKey = 83, 
    forwardKey = 87;
  KeyListener kl;

  FirstPersonController() {
    this(null, new PVector(
      width * 0.5, height * 0.5, 0));
  }

  FirstPersonController(KeyListener _kl) {
    this(_kl, new PVector(
      width * 0.5, height * 0.5, 0));
  }

  FirstPersonController(PVector _pos) {
    this(null, _pos);
  }

  FirstPersonController(KeyListener _kl, PVector _pos) {
    if (noCursor) {
      noCursor();
    }
    pitchConstraint = height * 0.125;
    leftDeadZone = width * 0.375;
    rightDeadZone = width * 0.625;
    pos = _pos;
    lookAt = _pos.copy();
    lookAt.z -= height / 1.155;
    aspect = width / float(height);
    nearClip = pos.z / 100.0;
    farClip = pos.z * 100.0;
    kl = _kl;
    if (kl != null) {
      kl.add(leftKey, rightKey, forwardKey, backKey);
    }
  }

  void draw() {
    move();
    look();
    perspective(fov, aspect, nearClip, farClip);
    camera(pos.x, pos.y, pos.z, 
      lookAt.x, lookAt.y, lookAt.z, 
      up.x, up.y, up.z);
  }

  void move() {
    if (kl.get(forwardKey)) {
      pos.x += sin(angle) * moveSpeed;
      pos.z += cos(angle) * moveSpeed; 
    }

    if (kl.get(backKey)) {
      pos.x -= sin(angle) * moveSpeed;
      pos.z -= cos(angle) * moveSpeed;
    }

    if (kl.get(leftKey)) {
      pos.x += sin(angle + HALF_PI) * moveSpeed;
      pos.z += cos(angle + HALF_PI) * moveSpeed;
    }

    if (kl.get(rightKey)) {
      pos.x -= sin(angle + HALF_PI) * moveSpeed;
      pos.z -= cos(angle + HALF_PI) * moveSpeed;
    }
  }

  void look() {
    if (focused) {
      if (mouseX > rightDeadZone && mouseX < width) { 
        angle -= incr;
      }
      if (mouseX < leftDeadZone && mouseX > 0) { 
        angle += incr;
      }

      float point = map(mouseY - height / 2.0, 
        height, 0, 
        height - pitchConstraint + avatarHeight, 
        pitchConstraint);
      lookAt.x = radius * sin(angle) + pos.x;
      lookAt.y += (point - lookAt.y) * lookSmoothing;
      lookAt.z = radius * cos(angle) + pos.z;
    }
  }
}