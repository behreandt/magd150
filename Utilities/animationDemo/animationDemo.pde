StateMachine<Animation> animator;
Animation idle, walk;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Animation Demo");
  pixelDensity(displayDensity());
  
  // This keeps the pixel art crisp even if it is scaled up.
  noSmooth();
  size(420, 420);
  background(64);
  idle = new Animation("Idle", /* Name */
    12, /* Frame Rate */
    96, 96, /* Width, Height */
    loadImage("idle0.png"), loadImage("idle1.png"), 
    loadImage("idle2.png"), loadImage("idle3.png"));
    
  // When associated with an actor object, these
  // offset's would likely be 0, 0, since the
  // animation would be equal to the actor's position.
  idle.offsetX = width * 0.5;
  idle.offsetY = height * 0.5;

  walk = new Animation("Walk", /* Name */
    6, /* Frame Rate */
    96, 96,  /* Width, Height */
    loadImage("walk0.png"), loadImage("walk1.png"), 
    loadImage("walk2.png"), loadImage("walk3.png"), 
    loadImage("walk4.png"), loadImage("walk5.png"), 
    loadImage("walk6.png"));
  walk.offsetX = width * 0.5;
  walk.offsetY = height * 0.5;

  animator = new StateMachine<Animation>(idle, walk);
}

void draw() {
  background(32);
  animator.current.draw();
}

void keyPressed() {
  if (keyCode == RIGHT) {
    animator.set("Walk");
  }
}

void keyReleased() {
  animator.set("Idle");
}