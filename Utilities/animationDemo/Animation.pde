class Animation extends State {
  String name = "Animation";
  PImage[] frames;

  int size = 0; 
  int osc = 1;
  int current = 0;
  int interval = 24;
  int pivot = CENTER;

  float offsetX = 0; 
  float offsetY = 0;
  float w = 64; 
  float h = 64;
  
  boolean oscillate = true;

  Animation(String _name, PImage... _frames) {
    this(_name, 24, 64, 64, _frames);
  }

  Animation(String _name, int _interval, 
    PImage... _frames) {
    this(_name, _interval, 64, 64, _frames);
  }


  Animation(String _name, int _interval, 
    float _w, float _h, 
    PImage... _frames) {
    name = _name;
    interval = _interval <= 0 ? 1 : _interval;
    w = _w;
    h = _h;
    frames = _frames;
    size = frames.length;
  }

  public String toString() {
    return name;
  }

  void enter() {
  }

  void draw() {
    advance();
    pushStyle();
    imageMode(pivot);
    image(frames[current], 
      offsetX, offsetY, 
      w, h);
    popStyle();
  }

  void exit() {
  }

  void advance() {

    // If the frameCount is cleanly divisible by
    // the interval or frame rate of the animation.
    if (frameCount % interval == 0) {

      // If you want the animation to start at frame 0
      // once it's reached the last frame.
      if (!oscillate) {
        current = (current + 1) % size;
      } else {

        // If you want the animation to ping-pong
        // between the first and last frames.
        current += osc;
        if (current == 0
          || current >= size - 1) {
          osc *= -1;
        }
      }
    }
  }
}