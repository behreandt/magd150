class StateMachine<E extends State> {
  E current;
  E previous;

  HashMap<String, E> states =
    new HashMap<String, E>();

  StateMachine(E... sts) {
    int sz = sts.length;
    for (int i = 0; i < sz; ++i) {
      sts[i].machine = this;
      states.put(sts[i].toString(), sts[i]);
    }
    if (sz > 0) {
      set(sts[0]);
    }
  }

  String toString() {
    return states.values().toString();
  }

  void add(E s) {
    s.machine = this;
    states.put(s.toString(), s);
  }

  E remove(String request) {
    E removed = states.remove(request);
    if (removed != null) {
      removed.machine = null;
    }
    return removed;
  }

  E get(String request) {
    return states.get(request);
  }

  void set(String request) {
    E s = states.get(request);
    if (s != null) {
      previous = current;
      current = s;
      if (previous != null) {
        previous.exit();
      }
      current.enter();
    } else {
      println("The state " + request + " could not be found.");
    }
  }

  void set(E s) {
    previous = current;
    current = s;
    if (previous != null) {
      previous.exit();
    }
    current.enter();
  }
}