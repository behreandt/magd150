abstract class State {
  StateMachine machine;
  
  abstract void enter();
  abstract void draw();
  abstract void exit();
  
  public String toString() {
    return getClass().getSimpleName();
  }
}