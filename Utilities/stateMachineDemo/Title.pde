class Title extends GameState {
  
  void draw() {
    pushStyle();
    background(255, 127, 0);
    textAlign(CENTER, CENTER);
    textSize(42);
    fill(255);
    text("TITLE", width * 0.5, height * 0.5);
    popStyle();
  }
  
  void transition(StateMachine sm) {
    sm.set("Playing");
  }
}