abstract class GameState extends State {
  abstract void transition(StateMachine sm);

  void enter() {
    println(this + " entered.");
  }

  void exit() {
    println(this + " exited.");
  }
}