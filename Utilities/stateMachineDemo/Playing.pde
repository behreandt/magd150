class Playing extends GameState {

  float x, y, r, size, 
    destX, destY, destR, destSize;
  color current, target;

  // The @ symbol prefaces an annotation in Java. This
  // annotation specifies that Playing's enter function
  // overrides the enter function that Playing would
  // have inherited from its super- or parent-class
  // GameState. Some programming languages require you
  // to specify that one function overrides another;
  // here it is optional.
  @Override
    void enter() {
    // If you want to call the baseline function
    // of a parent (GameState, in this case) which
    // you are overriding, you can say, super.foo();
    super.enter();
    background(0, 127, 255);
    x = width * 0.5;
    y = height * 0.5;
    r = 2;
    size = 75;
    destX = random(0, width);
    destY = random(0, height);
    destR = random(0, 20);
    destSize = random(25, 75);
    current = color(127, 255, 0);
    target = color(random(54, 255), random(54, 255), random(54, 255));
  }

  void draw() {
    x = lerp(x, destX, 0.025);
    y = lerp(y, destY, 0.025);
    size = lerp(size, destSize, 0.025);
    r = lerp(r, destR, 0.025);
    current = lerpColor(current, target, 0.025);
    if (frameCount % 200 == 0) {
      destX = random(0, width);
      destY = random(0, height);
      destR = random(0, 20);
      destSize = random(25, 75);
      target = color(random(54, 255), random(54, 255), random(54, 255));
    }
    pushStyle();
    noStroke();
    fill(0, 127, 255, 24);
    rect(0, 0, width, height);
    rectMode(CENTER);
    pushMatrix();
    translate(x, y);
    rotate(frameCount / 45.0);
    fill(current);
    rect(0, 0, size, size, r);
    popMatrix();
    textAlign(CENTER, CENTER);
    textSize(42);
    fill(255);
    text("PLAYING!", width / 2.0, height / 5.0);
    popStyle();
  }

  void transition(StateMachine sm) {
    sm.set("Title");
  }
}