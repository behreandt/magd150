// Create a state machine object.
StateMachine<GameState> sm;

void setup() {
  surface.setResizable(true);
  surface.setTitle("State Machine Demo");
  pixelDensity(displayDensity());
  size(420, 420);
  background(64);

  // Add new objects which inherit from the class
  // State to the StateMachine.
  sm = new StateMachine<GameState>(new Title(), 
    new Playing());
}

void draw() {

  // Display the current state.
  sm.current.draw();
}

void mousePressed() {

  // Hand off user input events to the state machine's
  // current state.
  sm.current.mousePressed(sm);
}