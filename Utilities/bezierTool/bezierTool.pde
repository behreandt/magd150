// To learn more about curvilinear form, please see
// General: https://en.wikipedia.org/wiki/B%C3%A9zier_curve
// JavaScript: https://youtu.be/dXECQRlmIaE , https://youtu.be/2hL1LGMVnVM
// Unity: http://catlikecoding.com/unity/tutorials/curves-and-splines/

float wasdX, wasdY, arrowX, arrowY, ijklX, ijklY;
float speed, initialSpeed, accel;

void setup() {
  surface.setTitle("Bezier Tool");
  surface.setResizable(true);
  pixelDensity(displayDensity());
  size(420, 420);
  background(32);

  wasdX = random(width / 4.0, width / 2.0);
  wasdY = random(height / 4.0, height / 2.0);
  arrowX = random(width / 2.0, width * 3 / 4.0);
  arrowY = random(height / 2.0, height * 3 / 4.0);
  ijklX = random(width / 2.0, width / 2.0);
  ijklY = random(height / 2.0, height / 2.0);

  initialSpeed = speed = 1;
  accel = 0.0075;

  textAlign(CENTER, TOP);
  textSize(14);
}

void draw() {
  noCursor();
  background(32);
  grid(50);

  pushStyle();
  noFill();
  strokeWeight(1);
  stroke(255, 0, 0);
  line(wasdX, wasdY, mouseX, mouseY);

  stroke(0, 255, 0);
  line(wasdX, wasdY, ijklX, ijklY);
  line(mouseX, mouseY, arrowX, arrowY);

  strokeWeight(2);
  stroke(255);
  bezier(wasdX, wasdY, 
    ijklX, ijklY, 
    arrowX, arrowY, 
    mouseX, mouseY);
  popStyle();

  if (keyPressed) {
    if (key == 'W' || key == 'w') {
      wasdY -= speed;
      speed += accel;
    } else if (key == 'S' || key == 's') {
      wasdY += speed;
      speed += accel;
    }

    if (key == 'A' || key == 'a') {
      wasdX -= speed;
      speed += accel;
    } else if (key == 'D' || key == 'd') {
      wasdX += speed;
      speed += accel;
    }

    if (key == 'I' || key == 'i') {
      ijklY -= speed;
      speed += accel;
    } else if (key == 'K' || key == 'k') {
      ijklY += speed;
      speed += accel;
    }

    if (key == 'J' || key == 'j') {
      ijklX -= speed;
      speed += accel;
    } else if (key == 'L' || key == 'l') {
      ijklX += speed;
      speed += accel;
    }

    if (keyCode == UP) {
      arrowY -= speed;
      speed += accel;
    } else if (keyCode == DOWN) {
      arrowY += speed;
      speed += accel;
    }

    if (keyCode == LEFT) {
      arrowX -= speed;
      speed += accel;
    } else if (keyCode == RIGHT) {
      arrowX += speed;
      speed += accel;
    }
  } else {
    speed = initialSpeed;
  }

  pushStyle();
  fill(255, 255, 0);
  textAlign(CENTER, CENTER);
  text("a1 (" + round(wasdX) + ", " + round(wasdY) + ")", 
    wasdX, wasdY);
  text("a2 (" + mouseX + ", " + mouseY + ")", 
    mouseX, mouseY);
  text("c1 (" + round(ijklX) + ", " + round(ijklY) + ")", 
    ijklX, ijklY);
  text("c2 (" + round(arrowX) + ", " + round(arrowY) + ")", 
    arrowX, arrowY);
  fill(255, 85);
  textAlign(CENTER, BOTTOM);
  text("Anchor Point 1: WASD\n"
    + "Control Point 1: IJKL\n"
    + "Control Point 2: Arrows\n"
    + "Anchor Point 2: Mouse", 
    width / 2.0, height);
  popStyle();
}

void grid(float unit) {
  stroke(64);
  for (float x = unit; x < width; x += unit) {
    line(x, 0, x, height);
  }
  for (float y = unit; y < height; y += unit) {
    line(0, y, width, y);
  }
}