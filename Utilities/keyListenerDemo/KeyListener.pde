class KeyListener {
  // A key listener uses a map (a.k.a. dictionary)
  // to correlate an integer value, the key code,
  // with true (key pressed) or false (key released).
  // A Map is a form of collections which belongs to
  // the java.util library. It contains a set of
  // Key-Value Pairs. Since both the key and the value
  // could by any type of data, the angle brackets, < >
  // are used to tell the map what kind of data it
  // should accept.
  private java.util.Map<Integer, Boolean> pressed = 
    new java.util.HashMap<Integer, Boolean>();

  // A Constructor with no parameters. This calls
  // the following constructor, providing some
  // default initial keys. These upper-case constants
  // are actually numeric values, so if you use println
  // to see what is in the key listener, you will see
  // the numbers, since there is no character for them.
  public KeyListener() {
    this(UP, /* 38 */
      DOWN, /* 40 */
      LEFT, /* 37 */
      RIGHT, /* 39 */
      SHIFT); /* 16 */
  }

  // A constructor that accepts a variable number
  // of arguments (varargs, signified by the ellipsis,
  // '...') which are treated as an array. A for-loop goes
  // through the array and places the keys in the map.
  public KeyListener(Integer... admissibleKeys) {
    for (int i = 0, size = admissibleKeys.length; i < size; ++i) {
      pressed.put(admissibleKeys[i], false);
    }
  }

  public String toString() {
    //return pressed.toString();
    StringBuilder s = new StringBuilder();
    for (Integer k : pressed.keySet()) {
      s.append(char(k) + " " + k + "\n");
    }
    return s.toString();
  }

  // Returns the true or false value for the key. If the dictionary
  // doesn't contain the key, then false is returned. This uses a
  // ternary operator, which is a condensed syntax for an if-else
  // conditional statement.
  public Boolean get(Integer kc) {
    return pressed.containsKey(kc) ? pressed.get(kc) : false;
  }

  // Adds new keys to an existing key listener.
  public void add(Integer... newKeys) {
    for (int i = 0, size = newKeys.length; i < size; ++i) {
      pressed.put(newKeys[i], false);
    }
  }

  // When the key is pressed, the value for that key
  // is set to true.
  public void keyPressed(Integer kc) {
    pressed.put(kc, true);
  }

  // When the key is released, the value for that key
  // is set to false.
  public void keyReleased(Integer kc) {
    pressed.put(kc, false);
  }
}