// Declare a variable of the type KeyListener.
KeyListener kl;

float x, y, radius, speed;
int north, south, east, west;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Key Listener Demo");
  pixelDensity(displayDensity());
  size(420, 420);
  background(32);
  ellipseMode(RADIUS);
  colorMode(HSB, 359, 99, 99);
  noStroke();

  x = width * 0.5;
  y = height * 0.5;
  radius = 30;
  speed = 2;

  north = 87; /* W */
  west = 65; /* A */
  south = 83; /* S */
  east = 68; /* D */

  // Construct the key listener.
  // If no arguments are provided, the arrow keys
  // and shift key will be provided as a default.
  //kl = new KeyListener();

  kl = new KeyListener(north, south, east, west);

  println(kl);
}

void draw() {
  //background(32);

  // In draw, use if(kl.get(desiredKey)) to trigger
  // a behavior.
  if (kl.get(north)) {
    y = y >= 0 ? y - speed : height;
  }

  if (kl.get(west)) {
    x = x >= 0 ? x - speed : width;
  }

  if (kl.get(east)) {
    x = x <= width ? x + speed : 0;
  }

  if (kl.get(south)) {
    y = y <= height ? y + speed : 0;
  }

  radius = random(10, 50);
  fill(frameCount % 360, 100, 100, 27);
  ellipse(x, y, radius, radius);
}

// Call the key listener's keyPressed and keyReleased
// method from the global keyPressed and keyReleased
// functions.
void keyPressed() {
  //println(key + " " + keyCode);
  kl.keyPressed(keyCode);
}

void keyReleased() {
  kl.keyReleased(keyCode);
}