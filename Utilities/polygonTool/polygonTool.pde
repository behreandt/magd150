int sides;
float centerX, centerY, 
  theta, radius, speed, 
  initialSpeed, accel, thetaStd;
boolean paused;

void setup() {
  surface.setTitle("Polygon Tool");
  surface.setResizable(true);
  size(420, 420);
  pixelDensity(displayDensity());
  background(0);
  textAlign(CENTER, CENTER);
  
  sides = 3;
  centerX = width * 0.5;
  centerY = height * 0.5;
  theta = 0;
  radius = height * 0.3333;
  paused = true;
  initialSpeed = speed = 1;
  accel = 0.05;
  thetaStd = radians(15);
}

void draw() {
  if (!paused) { 
    theta += 0.01;
  }

  if (keyPressed) {
    if (key == CODED) {
      if (keyCode == LEFT) {
        radius = radius <= 1 ? 1 : radius - speed;
      } else if (keyCode == RIGHT) {
        radius = radius + speed;
      }
      speed += accel;
    } else {
      if (key == 'W' || key == 'w') { 
        centerY -= speed;
      } else if (key == 'S' || key == 's') { 
        centerY += speed;
      }
      if (key == 'A' || key == 'a') { 
        centerX -= speed;
      } else if (key == 'D' || key == 'd') { 
        centerX += speed;
      }
      speed += accel;
    }
  } else {
    speed = initialSpeed;
  }

  if (mousePressed && mouseButton == LEFT) {
    theta = atan2(mouseY - centerY, mouseX - centerX);
  }

  background(32);
  grid(50);
  polygon(centerX, centerY, radius, sides, theta);
  fill(255, 85);
  textAlign(CENTER, BOTTOM);
  text("WASD: Move center\n"
    + "DOWN: Decrease sides, UP: Increase sides\n"
    + "LEFT: Decrease radius, RIGHT: Increase radius\n"
    + "SPACE: Rotate 15 degrees\n"
    + "LEFT MOUSE: Point to cursor, RIGHT MOUSE: Rotate", 
    width / 2.0, height);
}

void keyReleased() {
  if (keyCode == UP) {
    sides = sides >= 16 ? 16 : sides + 1;
  } else if (keyCode == DOWN) {
    sides = sides <= 3 ? 3 : sides - 1;
  } else if (key == ' ') {
    theta = thetaStd * round(theta/thetaStd);
    theta += thetaStd;
  }
}

void mousePressed() {
  if (mouseButton == RIGHT) {
    paused = !paused;
  }
}

void polygon(float x, float y, float r, 
  int sides, float rotation) {
  float t, vx, vy, nx, ny;
  fill(255);
  textAlign(CENTER, CENTER);
  text("(" + round(x) + ", " + round(y) + ")", 
    x, y);
  fill(0, 255, 127);
  text(round(degrees(theta) % 360) + " degrees", 
    x, y + 14);
  noFill();
  strokeWeight(1);
  stroke(204, 0, 204);
  line(x, y, x + cos(rotation) * r, y + sin(rotation) * r);
  stroke(0, 127, 255);

  beginShape();
  for (int i = 0; i < sides; ++i) {
    t = map(i, 0, sides, 0, TWO_PI);
    // Variables which are scaled to world space, but
    // only track local rotation.
    //wx = cos(t) * r;
    //wy = sin(t) * r;

    // Variables which account for rotation, and are
    // scaled to a unit size (-0.5 to 0.5)
    nx = cos(rotation + t);
    ny = sin(rotation + t);

    // Variables that track world scale and rotation.
    vx = x + nx * r;
    vy = y + ny * r;
    vertex(vx, vy);
    pushStyle();
    fill(255);
    text("(" + round(vx) + ", "
      + round(vy) + ")", 
      vx, vy);
    fill(255, 255, 0);
    text("(" + nf(nx / 2.0, 1, 2) + ", "
      + nf(ny / 2.0, 1, 2) + ")", 
      vx, vy + 14);
    popStyle();
  }
  endShape(CLOSE);
}

void grid(float unit) {
  stroke(64);
  for (float x = unit; x < width; x += unit) {
    line(x, 0, x, height);
  }
  for (float y = unit; y < height; y += unit) {
    line(0, y, width, y);
  }
}