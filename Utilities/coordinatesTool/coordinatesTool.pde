ArrayList<PVector> points = new ArrayList<PVector>();

void setup() {
  surface.setTitle("Coordinates Tool");
  surface.setResizable(true);
  size(450, 450);
  pixelDensity(displayDensity());
  background(64);
  textAlign(LEFT, BOTTOM);
  fill(255);
}

void draw() {
  background(32);

  // Draw grid.
  pushStyle();
  stroke(64);
  for (float x = 50; x < width; x += 50) {
    line(x, 0, x, height);
  }

  for (float y = 50; y < height; y += 50) {
    line(0, y, width, y);
  }
  popStyle();

  // Show collected points.
  pushStyle();
  strokeWeight(1);
  stroke(0, 127, 255);
  noFill();
  beginShape();
  for(PVector p : points) {
    vertex(p.x, p.y);
    text("(" + round(p.x) + ", " + round(p.y) + ")", 
    p.x, p.y);
  }
  endShape(CLOSE);
  popStyle();

  // Show current position.
  pushStyle();
  fill(255, 255, 0);
  text("(" + mouseX + ", " + mouseY + ")", 
    mouseX, mouseY);
  popStyle();
}

void mousePressed() {
  if (mouseButton == LEFT) {
    points.add(new PVector(mouseX, mouseY));
  } else if (mouseButton == RIGHT && points.size() > 0) {
    points.remove(points.size() - 1);
  }
}