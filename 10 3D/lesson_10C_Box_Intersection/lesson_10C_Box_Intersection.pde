float x1, y1, z1, 
  w1, h1, d1, 
  x2, y2, z2, 
  w2, h2, d2;
color fill1, fill2;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Box Intersection");
  pixelDensity(displayDensity());
  size(420, 420, P3D);
  background(64);
  noStroke();

  // Box 1
  x1 = width * 0.25;
  y1 = height * 0.5;
  z1 = 10;
  w1 = 100;
  h1 = 150;
  d1 = 50;
  fill1 = color(255, 0, 0);

  // Box 2
  x2 = width * 0.75;
  y2 = height * 0.5;
  z2 = -10;
  w2 = h2 = d2 = 75;
  fill2 = color(0, 0, 255);
}

void draw() {
  background(32);
  // Sets the default lighting.
  lights();

  // Box 1
  pushMatrix();
  translate(x1, y1, z1);
  fill(fill1);
  box(w1, h1, d1);
  popMatrix();

  // Box 2
  pushMatrix();
  translate(x2, y2, z2);
  fill(fill2);
  box(w2, h2, d2);
  popMatrix();

  // Intersection is the same in 3D as in 2D, 
  // except that the z dimension must also be
  // checked, resulting in 6 comparisons.
  if (x1 - (w1 / 2.0) < x2 + (w2 / 2.0)
    && x1 + (w1 / 2.0) > x2 - (w2 / 2.0)
    && y1 - (h1 / 2.0) < y2 + (h2 / 2.0)
    && y1 + (h1 / 2.0) > y2 - (h2 / 2.0)
    && z1 - (d1 / 2.0) < z2 + (d2 / 2.0)
    && z1 + (d1 / 2.0) > z2 - (d2 / 2.0)) {
    fill1 = color(255, 127, 0);
    fill2 = color(0, 127, 255);
  } else {
    fill1 = color(255, 0, 0);
    fill2 = color(0, 0, 255);
  }

  if (keyPressed) {
    if (key == CODED) {
      if (keyCode == UP) {
        y1--;
      }
      if (keyCode == LEFT) {
        x1--;
      }
      if (keyCode == RIGHT) {
        x1++;
      }
      if (keyCode == DOWN) {
        y1++;
      }
    } else {
      if (key == 'w' || key == 'W') {
        y2--;
      }
      if (key == 'a' || key == 'A') {
        x2--;
      }
      if (key == 's' || key == 'S') {
        y2++;
      }
      if (key == 'd' || key == 'D') {
        x2++;
      }
      if (key == 'q' || key == 'Q') {
        z2--;
      }
      if (key == 'e' || key == 'E') {
        z2++;
      }
      if (key == ',' || key == '<') {
        z1--;
      }
      if (key == '.' || key == '>') {
        z1++;
      }
    }
  }
}