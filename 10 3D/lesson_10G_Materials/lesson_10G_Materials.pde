int count;
float[] shinies;
color[] specs, emitters, ambiance;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Materials");
  pixelDensity(displayDensity());
  size(640, 420, P3D);
  background(64);
  noStroke();

  count = 7;
  shinies = new float[count];
  specs = new color[count];
  emitters = new color[count];
  ambiance = new color[count];
  changeMaterials();
}

void draw() {
  background(32);

  directionalLight(127, 115, 102, 0.5, 0.5, -0.5);

  for (int i = 0; i < count; ++i) {
    pushStyle();

    ambient(ambiance[i]);
    specular(specs[i]);
    emissive(emitters[i]);
    pushMatrix();
    translate(map(i, 0, count, width * 0.125, width * 0.875), 
      height * 0.333, 
      map(i, 0, count, -100, 100));
    scale(height / count / 1.25);
    rotateY(frameCount / 90.0);
    rotateZ(frameCount / 90.0);

    box(1, 1, 1);
    popMatrix();

    pushMatrix();
    translate(map(i, -1, count, 0, width), 
      height * 0.666, 
      map(i, 0, count, 100, -100));
    scale(height / count / 1.75);
    shininess(shinies[i]);
    sphere(1);
    popMatrix();
    popStyle();
  }
}

void mousePressed() {
  changeMaterials();
}

void changeMaterials() {
  for (int i = 0; i < count; ++i) {
    shinies[i] = random(0.01, 50);
    specs[i] = color(random(0, 204), 
      random(0, 204), 
      random(0, 204));
    emitters[i] = color(random(0, 204), 
      random(0, 204), 
      random(0, 204));
    ambiance[i] = color(random(0, 204), 
      random(0, 204), 
      random(0, 204));
  }
}