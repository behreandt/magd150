# Assignment 10
## 3D
### 100 points

![3D](preview.png)

### References

* Daniel Shiffman, [_Learning Processing_](http://learningprocessing.com/examples/) Chapter 14
* Shiffman, [P3D Tutorial](https://processing.org/tutorials/p3d/)

### Instructions

1. Work in pairs of two. (30 pts.)
    * One person will create a 2D [texture](https://processing.org/reference/texture_.html) using Processing's [save](https://processing.org/reference/save_.html) feature.
    * One person will create the 3D sketch to which the texture will be applied.
    * Each person will submit their respective sketch to the dropbox.

2. Create and use at least 2 lights. (10 pts.)
    * [Ambient Light](https://processing.org/reference/ambientLight_.html)
    * [Directional Light](https://processing.org/reference/directionalLight_.html)
    * [Point Light](https://processing.org/reference/pointLight_.html)

3. Animate the [camera](https://processing.org/reference/camera_.html). (10 pts.)

4. Use at least 2 3D shapes. (10 pts.)
    * [Box](https://processing.org/reference/box_.html)
    * [Sphere](https://processing.org/reference/sphere_.html)
    * [PShape](https://processing.org/reference/PShape.html)

5. Choose 3-5 colors from the color palette of the painting assigned to the group. (10 pts.)

6. Ensure the texture tiles seamlessly. (10 pts.)

7. Ensure that the sketch runs without errors. The main tab of the sketch should be the only tab with global-scope setup and draw functions. (10 pts.)

8. Ensure that the main tab of the sketch shares the same name as the sketch folder. Name the sketch folder and main tab according to the following convention: s17_magd150_lab10_yourlastname. Compress the sketch folder into a .zip file and upload to D2L. (10 pts.)

__Any code or media within a submission are presumed to be the work of the student(s) unless otherwise stated. When media or code from outside sources are used, a link to the source and notice of attribution at the top of the main sketch in a comment should be provided. Media and code used are expected to be from the intellectual commons (i.e., not copyrighted or registered as a trademark) and to be altered substantially by the student(s). On first incident, students whose work does not meet this standard will have one week to resubmit the assignment for half credit. On second incident, students will be referred to Student Conduct Programs and will fail the course.__
