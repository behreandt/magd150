// Different graphic rendering modes are available in Processing,
// including P3D which allows for 3-dimensional graphics. using
// this lets you create gradients. (On Windows systems, using these
// different rendering modes can lead to lag.)
size(420, 420, P2D);

// For high density displays, this line may be necessary to get a
// smooth image.
pixelDensity(displayDensity());
background(32);
noStroke();

beginShape();
fill(255, 0, 0);
vertex(0, 0);
fill(0, 255, 0);
vertex(420, 0);
fill(0, 0, 255);
vertex(210, 210);
endShape();

beginShape();
fill(255, 255, 0);
vertex(420, 420);
fill(0, 255, 255);
vertex(420, 0);
fill(255, 0, 255);
vertex(210, 210);
endShape();

beginShape();
fill(255, 127, 0);
vertex(0, 420);
fill(0, 127, 255);
vertex(420, 420);
fill(127, 255, 0);
vertex(210, 210);
endShape();

beginShape();
fill(255, 54, 127);
vertex(0, 0);
fill(127, 255, 54);
vertex(0, 420);
fill(54, 127, 255);
vertex(210, 210);
endShape();