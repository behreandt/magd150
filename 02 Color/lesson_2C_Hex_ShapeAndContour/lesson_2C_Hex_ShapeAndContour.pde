size(420, 420);
background(32);

// When we count, we use the decimal system, where we
// go from 0 to 9 in a single digit before we add another
// digit. for the sake of efficiency, colors can be
// represented in a hexadecimal (base 16) system. After 0-9
// this system counts A, B, C, D, E, F before it goes to
// another digit. FF = 255, 127 = 7F, and so on. Thus,
// (255, 255, 255) in RGB mode become #FFFFFF in hex mode,
// where the first two digits are for red, the next two
// for green, the last for blue. An alternate notation lets
// us include alpha: 0xAARRGGBB. On Processing's menu bar,
// go to Tools > Color Selector to better see this.

noStroke();
fill(0xCC7F0000);
ellipse(150, 150, 200, 200);
fill(0xCC400000);
ellipse(150, 150, 160, 160);
fill(0xCCE50000);
ellipse(150, 150, 80, 80);

fill(0xCC007F00);
ellipse(270, 150, 200, 200);
fill(0xCC004000);
ellipse(270, 150, 160, 160);
fill(0xCC00E500);
ellipse(270, 150, 80, 80);

fill(0xCC00007F);
ellipse(150, 270, 200, 200);
fill(0xCC000040);
ellipse(150, 270, 160, 160);
fill(0xCC0000E5);
ellipse(150, 270, 80, 80);

fill(0xCC7F7F00);
ellipse(270, 270, 200, 200);
fill(0xCC404000);
ellipse(270, 270, 160, 160);
fill(0xCCE5E500);
ellipse(270, 270, 80, 80);

// Between beginShape(); and endShape(CLOSE); specify
// each vertex (point) which will act as a fence post
// to enclose an area. Vertices proceed in clockwise
// order.
fill(0x8890B2C1);
beginShape();
  vertex(144, 10);
  vertex(276, 10);
  vertex(276, 144);
  vertex(409, 144);
  vertex(409, 276);
  vertex(276, 276);
  vertex(276, 409);
  vertex(144, 409);
  vertex(144, 276);
  vertex(10, 276);
  vertex(10, 144);
  vertex(144, 144);
  
  // Between beginContour(); and endContour(); specify
  // each vertex (point) to cut-out from the shape you
  // created. Vertices proceed in counter-clockwise order.
  beginContour();
    vertex(187, 221);
    vertex(174, 257);
    vertex(207, 239);
    vertex(240, 261);
    vertex(235, 220);
    vertex(263, 195);
    vertex(228, 194);
    vertex(213, 157);
    vertex(196, 194);
    vertex(160, 190);
  endContour();

// Close specifies that the gap between any initial and
// concluding line will be crossed by a final line.
endShape(CLOSE);