size(420, 420);
background(255);
noFill();

// colorMode(HSB, hueMax, satMax, brightMax, alphaMax);
// colorMode(RGB, redMax, greenMax, blueMax, alphaMax);
// colorMode switches between Red-Green-Blue and
// Hue-Saturation-Brightness. Hue is the place
// on the color wheel, usually from 0 to 360.
// Saturation at 0 is grayscale. Brightness at 0
// is black. HSB mode is very helpful when you want
// to vary colors semi-randomly but also keep them
// in the same color range.
colorMode(HSB, 360, 100, 100, 1.0);

// Straight line distance.
strokeWeight(1);
stroke(300, 100, 100);
line(102, 27, 210, 210);

// Pull of control points on anchor points.
stroke(240, 100, 100);
line(102, 27, 54, 400);
line(325, 305, 210, 210);

strokeWeight(7.5);

// Anchor points
stroke(30, 100, 100);
point(102, 27);
point(210, 210);

// Control points
stroke(330, 100, 100);
point(54, 400);
point(325, 305);

//bezier(anchorPoint1X, anchorPoint1Y,
//  controlPoint1X, controlPoint1Y,
//  controlPoint2X, controlPoint2Y,
//  anchorPoint2X, anchorPoint2Y);
strokeWeight(3);
stroke(0, 0, 0);

bezier(102, 27, // Anchor point 1
  54, 400, // Control point 1
  325, 305, // Control point 2
  210, 210); // Anchor point 2

strokeWeight(100);
stroke(180, 100, 100, 0.125);
bezier(0, 0, 
  210, 375, 
  375, 210, 
  420, 0);

stroke(270, 100, 100, 0.125);
bezier(0, 0, 
  105, 375, 
  210, 105, 
  420, 420);

stroke(0, 100, 100, 0.125);
bezier(0, 210, 
  105, 105, 
  375, 375, 
  210, 0);