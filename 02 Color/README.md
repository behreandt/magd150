# Assignment 2
## Color
### 100 points

![Color](preview.png)

### References

* Daniel Shiffman, [_Learning Processing_](http://learningprocessing.com/examples/) Chapters 1 - 2
* Shiffman, [Video: Pixels](https://youtu.be/a562vsSI2Po?list=PLRqwX-V7Uu6bsRnSEJ9tRn4V_XCGXovs4)
* Shiffman, [Video: Processing Environment](https://youtu.be/5N31KNgOO0g?list=PLRqwX-V7Uu6Yo4VdQ4ZTtqRQ1AE4t_Ep9)
* [Tutorial: Color](https://processing.org/tutorials/color)
* [Tutorial: Curves](https://processing.org/tutorials/curves/)
* [Adobe Color](https://color.adobe.com/create/color-wheel/)

### Tools

* [Coordinates Tool](../Utilities/coordinatesTool/coordinatesTool.pde)
* [Polygon Tool](../Utilities/polygonTool/polygonTool.pde)
* [Bezier Tool](../Utilities/bezierTool/bezierTool.pde)

### Instructions

1. Adhere to the theme to be determined in lab. (10 pts.)
    * Wednesday: Planets / Space
    * Monday: Balloons

2. Give the sketch a [background](https://processing.org/reference/background_.html). (10 pts.)

3. Use at least 4 different colors. (10 pts.)

4. Use [color mode](https://processing.org/reference/colorMode_.html) to work with all 3 different ways of representing color: (10 pts.)
    * RGBA (Red, Blue, Green, Alpha)
    * HSBA (Hue, Saturation, Brightness, Alpha)
    * Hexadecimal (0xAARRGGBB or #RRGGBB)

5. Use at least two compositional elements introduced in this unit: (20 pts.)
    * [Triangle](https://processing.org/reference/triangle_.html)
    * [Bezier Curve](https://processing.org/reference/bezier_.html)
    * [Arc](https://processing.org/reference/arc_.html)
    * [Shape](https://processing.org/reference/beginShape_.html)
    * [Contour](https://processing.org/reference/beginContour_.html)

6. Compose with the following in mind: (20 pts.)
    * Symmetry: If the sketch were folded, would it look the same on both sides of the fold?
    * Contrast: How much do the elements of the sketch stand out or blend together? Do they use analogous, monochromatic, triadic, complementary or compound color?
    * Grouping: Are like or unlike elements grouped together, repeated at regular or irregular intervals? Are they combined to form a symbol or representational image?
    * Scale: How do the relative sizes of the elements influence our perception of them?

7. Ensure that the sketch runs without errors. The main tab of the sketch should be the only tab with global-scope setup and draw functions. (10 pts.)

8. Ensure that the main tab of the sketch shares the same name as the sketch folder. Name the sketch folder and main tab according to the following convention: s17_magd150_lab02_yourlastname. Compress the sketch folder into a .zip file and upload to D2L. (10 pts.)

__Any code or media within a submission are presumed to be the work of the student(s) unless otherwise stated. When media or code from outside sources are used, a link to the source and notice of attribution at the top of the main sketch in a comment should be provided. Media and code used are expected to be from the intellectual commons (i.e., not copyrighted or registered as a trademark) and to be altered substantially by the student(s). On first incident, students whose work does not meet this standard will have one week to resubmit the assignment for half credit. On second incident, students will be referred to Student Conduct Programs and will fail the course.__
