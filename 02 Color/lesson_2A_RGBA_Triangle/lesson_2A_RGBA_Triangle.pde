size(280, 220);
background(32);
noStroke();

// Red is 255, 0, 0.
// Opacity increases from left (32) to right (255).
fill(255, 0, 0, 32);
// triangle(x1, y1, x2, y2, x3, y3);
triangle(10, 10, 60, 10, 35, 60);
fill(255, 0, 0, 64);
triangle(40, 10, 90, 10, 65, 60);
fill(255, 0, 0, 96);
triangle(70, 10, 120, 10, 95, 60);
fill(255, 0, 0, 128);
triangle(100, 10, 150, 10, 125, 60);
fill(255, 0, 0, 159);
triangle(130, 10, 180, 10, 155, 60);
fill(255, 0, 0, 191);
triangle(160, 10, 210, 10, 185, 60);
fill(255, 0, 0, 223);
triangle(190, 10, 240, 10, 215, 60);
fill(255, 0, 0, 255);
triangle(220, 10, 270, 10, 245, 60);

// Green is 0, 255, 0.
fill(0, 255, 0, 32);
triangle(10, 40, 60, 40, 35, 90);
fill(0, 255, 0, 64);
triangle(40, 40, 90, 40, 65, 90);
fill(0, 255, 0, 96);
triangle(70, 40, 120, 40, 95, 90);
fill(0, 255, 0, 128);
triangle(100, 40, 150, 40, 125, 90);
fill(0, 255, 0, 159);
triangle(130, 40, 180, 40, 155, 90);
fill(0, 255, 0, 191);
triangle(160, 40, 210, 40, 185, 90);
fill(0, 255, 0, 223);
triangle(190, 40, 240, 40, 215, 90);
fill(0, 255, 0, 255);
triangle(220, 40, 270, 40, 245, 90);

// Blue is 0, 0, 255.
fill(0, 0, 255, 32);
triangle(10, 70, 60, 70, 35, 120);
fill(0, 0, 255, 64);
triangle(40, 70, 90, 70, 65, 120);
fill(0, 0, 255, 96);
triangle(70, 70, 120, 70, 95, 120);
fill(0, 0, 255, 128);
triangle(100, 70, 150, 70, 125, 120);
fill(0, 0, 255, 159);
triangle(130, 70, 180, 70, 155, 120);
fill(0, 0, 255, 191);
triangle(160, 70, 210, 70, 185, 120);
fill(0, 0, 255, 223);
triangle(190, 70, 240, 70, 215, 120);
fill(0, 0, 255, 255);
triangle(220, 70, 270, 70, 245, 120);

// Cyan is 0, 255, 255.
fill(0, 255, 255, 32);
triangle(10, 100, 60, 100, 35, 150);
fill(0, 255, 255, 64);
triangle(40, 100, 90, 100, 65, 150);
fill(0, 255, 255, 96);
triangle(70, 100, 120, 100, 95, 150);
fill(0, 255, 255, 128);
triangle(100, 100, 150, 100, 125, 150);
fill(0, 255, 255, 159);
triangle(130, 100, 180, 100, 155, 150);
fill(0, 255, 255, 191);
triangle(160, 100, 210, 100, 185, 150);
fill(0, 255, 255, 223);
triangle(190, 100, 240, 100, 215, 150);
fill(0, 255, 255, 255);
triangle(220, 100, 270, 100, 245, 150);

// Magenta is 255, 0, 255.
fill(255, 0, 255, 32);
triangle(10, 130, 60, 130, 35, 180);
fill(255, 0, 255, 64);
triangle(40, 130, 90, 130, 65, 180);
fill(255, 0, 255, 96);
triangle(70, 130, 120, 130, 95, 180);
fill(255, 0, 255, 128);
triangle(100, 130, 150, 130, 125, 180);
fill(255, 0, 255, 159);
triangle(130, 130, 180, 130, 155, 180);
fill(255, 0, 255, 191);
triangle(160, 130, 210, 130, 185, 180);
fill(255, 0, 255, 223);
triangle(190, 130, 240, 130, 215, 180);
fill(255, 0, 255, 255);
triangle(220, 130, 270, 130, 245, 180);

// Yellow is 255, 255, 0.
fill(255, 255, 0, 32);
triangle(10, 160, 60, 160, 35, 210);
fill(255, 255, 0, 64);
triangle(40, 160, 90, 160, 65, 210);
fill(255, 255, 0, 96);
triangle(70, 160, 120, 160, 95, 210);
fill(255, 255, 0, 128);
triangle(100, 160, 150, 160, 125, 210);
fill(255, 255, 0, 159);
triangle(130, 160, 180, 160, 155, 210);
fill(255, 255, 0, 191);
triangle(160, 160, 210, 160, 185, 210);
fill(255, 255, 0, 223);
triangle(190, 160, 240, 160, 215, 210);
fill(255, 255, 0, 255);
triangle(220, 160, 270, 160, 245, 210);