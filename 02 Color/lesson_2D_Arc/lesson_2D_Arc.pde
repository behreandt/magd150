size(680, 420);
background(32);

strokeWeight(1.5);
fill(127, 255, 127);
stroke(127, 127, 255);

arc(340, 210, 200, 200, 0, QUARTER_PI, PIE);

fill(255, 127, 127);
stroke(127, 255, 127);
arc(340, 210, 200, 200, QUARTER_PI, HALF_PI + QUARTER_PI, PIE);

fill(127, 127, 255);
stroke(255, 127, 127);
arc(340, 210, 200, 200, HALF_PI + QUARTER_PI, HALF_PI + QUARTER_PI + PI, PIE);

stroke(0, 127, 255);
fill(255, 127, 0);
arc(370, 195, 200, 200, HALF_PI + QUARTER_PI + PI, TAU, PIE);

// Processing works in radians by default, while you
// may find degrees to be more convenient. There are
// functions called degrees(n) and radians(n) which
// will let you convert back and forth.
stroke(127, 0, 255);
fill(255, 0, 127);
arc(50, 40, 75, 75, 0, radians(30));
arc(50, 90, 75, 75, 0, radians(45));
arc(50, 140, 75, 75, 0, radians(60));
arc(50, 190, 75, 75, 0, radians(90));
arc(50, 240, 75, 75, 0, radians(135));
arc(50, 290, 75, 75, 0, radians(180));
arc(50, 375, 75, 75, 0, radians(270));