// When you go to look up an entry in a dictionary, you
// look up a value (the definition) with a key (the word).
// In Java, the collection which simulates this is, confusingly,
// called a Map.

import java.util.Map;
import java.util.List;

// Like Lists, Maps require that you specify what type
// of variable you're using as a key, and what type you'll
// be using as a value: Map<Key, Value> mapName;
// In this case, I want to create a weird shape alphabet,
// so I will use 'a', 'b', 'c' as a key and a Pshape object
// as a value.

Map<Character, PShape> alphabet;
List<PShape> typed;
int textSize;
float x, y, radius, minSize, maxSize;
char resetKey = '/';

void setup() {
  surface.setResizable(true);
  surface.setTitle("Dictionaries");
  pixelDensity(displayDensity());
  size(420, 420);
  background(32);
  textAlign(CENTER, BOTTOM);

  // Processing has a built-in dictionary, but it trades
  // flexibility for accessibility.

  StringDict inv = new StringDict();
  inv.set("chess", "played by pharoahs.");
  inv.set("tic tac toe", "also called crosses and nauts.");
  inv.set("connect 4", "fondly remembered");
  inv.sortKeys();
  println(inv.get("chess"));
  println(inv.get("tic tac toe"));
  println(inv.get("connect 4"));

  alphabet = new HashMap<Character, PShape>();

  // In ASCII code, uppercase 'A' begins at 65,
  // lowercase 'a' begins at 97. The randomShape
  // function creates and returns a PShape.

  for (int i = 0; i < 26; ++i) {
    alphabet.put(char(i + 97), randomShape());
  }

  // The list typed will store the keys the user
  // has pressed.

  typed = new ArrayList<PShape>();
  x = width * 0.5;
  y = height * 0.5;
  radius = min(width, height) * 0.4;
  minSize = 10;
  maxSize = 40;
}

void draw() {
  background(32);

  x = width * 0.5;
  y = height * 0.5;
  radius = min(width, height) * 0.4;
  minSize = min(width, height) * 0.05;
  maxSize = min(width, height) * 0.1;

  float theta, scl;

  // The more 'characters' appear on screen, the
  // smaller each character will be.
  
  int size = typed.size();
  scl = map(size, 52, 0, minSize, maxSize);
  for (int i = 0; i < size; ++i) {
    theta = map(i, 0, size, 0, TWO_PI);
    shape(typed.get(i), 
      x + cos(theta) * radius, 
      y + sin(theta) * radius, 
      scl, scl);
  }
}

void keyTyped() {
  if (key == resetKey) {
    typed.clear();
    for (int i = 0; i < 26; ++i) {
      alphabet.put(char(i + 97), randomShape());
    }
  } else if (key == BACKSPACE && typed.size() > 0) {
    typed.remove(typed.size() - 1);
  } else if (alphabet.containsKey(key)) {
    typed.add(alphabet.get(key));
  }
}

PShape randomShape() {
  PShape s = createShape();
  s.beginShape();
  s.colorMode(HSB, 360, 100, 100);
  //s.noFill();
  //s.strokeWeight(0.02);
  //s.stroke(random(0, 360), 100, 100);
  s.noStroke();
  s.fill(random(0, 360), 100, 100);
  int count = int(random(12, 24));
  float t1 = 0;
  for (int j = 0; j < count; ++j) {
    t1 = map(j, 0, count, 0, TWO_PI);
    s.vertex(sin(t1) * random(0.25, 1), 
      cos(t1) * random(0.25, 1));
  }
  s.endShape(CLOSE);
  return s;
}