// While not true of C#, enums can hold values and use
// constructors in Java.

enum Occupation {
  Arbolist('a'),
  Berserker('s'),
  Knight('d'),
  Thief('f');
  
  char value;
  
  private Occupation(char v) {
    value = v;
  }
}