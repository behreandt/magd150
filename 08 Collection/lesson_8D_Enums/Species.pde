// While not true of C#, enums can hold values and use
// constructors in Java.

enum Species {
  Dwarf('h'),
  Elf('j'),
  Human('k'),
  Lizard('l');
  
  char value;
  
  private Species(char v) {
    value = v;
  }
}