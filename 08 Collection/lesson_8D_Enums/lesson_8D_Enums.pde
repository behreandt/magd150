// Because enumerations can contain a greater range
// of discrete values than true or false, as with a
// boolean, and a smaller range of discrete values
// than a string ("Playing", "PLAYING", "playing", etc.),
// they are ideal for storing game states.

GameState current;
Avatar player;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Enumerations");
  pixelDensity(displayDensity());
  size(420, 420);
  background(64);
  noStroke();
  rectMode(RADIUS);
  ellipseMode(RADIUS);
  current = GameState.Title;
}

void draw() {
  background(32);

  if (current == GameState.Title) {
    background(255, 127, 0);
    textAlign(CENTER, CENTER);
    textSize(24);
    text("Welcome to Generic Game.\nPress any key.", 
      width / 2.0, height / 2.0);
  } else if (current == GameState.CharacterSelect) {
    background(0, 127, 255);
    textAlign(CENTER, TOP);
    textSize(24);
    text("Select your occupation.", width / 2.0, 0);

    Occupation[] occs = Occupation.values();
    textAlign(LEFT, TOP);
    textSize(14);
    for (int i = 0; i < occs.length; ++i) {
      text("Press " + occs[i].value + " for " + occs[i], 
        width * 0.125, 32 + i * 16);
    }
  } else if (current == GameState.Playing) {
    background(32);
    player.draw();
  }
}

void keyPressed() {
  if (current == GameState.Title) {
    current = GameState.CharacterSelect;
  } else if (current == GameState.CharacterSelect) {

    // You can get an array containing all values of
    // an enumeration using the .values() function. This
    // is handy when assigning a random element from
    // an enumeration, as with species below.

    Occupation[] occs = Occupation.values();
    Species[] species = Species.values();
    Species rnd = species[int(random(0, species.length))];
    for (int i = 0; i < occs.length; ++i) {
      if (key == occs[i].value) {
        player = new Avatar(occs[i], rnd);
        current = GameState.Playing;
        return;
      }
    }
  } else if (current == GameState.Playing) {
    current = GameState.Title;
  }
}