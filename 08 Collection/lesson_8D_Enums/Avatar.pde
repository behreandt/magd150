class Avatar {
  Occupation occupation;
  Species species;

  Avatar(Occupation occ, Species spc) {
    occupation = occ;
    species = spc;
  }

  void draw() {
    if (occupation == Occupation.Arbolist) {
      fill(255, 127, 0);
    } else if (occupation == Occupation.Berserker) {
      fill(0, 127, 255);
    } else if (occupation == Occupation.Knight) {
      fill(127, 0, 255);
    } else if (occupation == Occupation.Thief) {
      fill(0, 127, 255);
    }

    if (species == Species.Dwarf) {
      quad(width / 2.0 + 50, height / 2.0, 
        width / 2.0, height / 2.0 + 50, 
        width / 2.0 - 50, height / 2.0, 
        width / 2.0, height / 2.0 - 50);
    } else if (species == Species.Elf) {
      triangle(width / 2.0 + 50, height / 2.0, 
        width / 2.0 - 50, height / 2.0 + 50, 
        width / 2.0 - 50, height / 2.0 - 50);
    } else if (species == Species.Human) {
      ellipse(width / 2.0, height / 2.0, 50, 50);
    } else if (species == Species.Lizard) {
      rect(width / 2.0, height / 2.0, 50, 50);
    }

    textAlign(CENTER, BOTTOM);
    textSize(20);
    fill(255);
    text(species + " " + occupation, width / 2.0, height);
  }
}