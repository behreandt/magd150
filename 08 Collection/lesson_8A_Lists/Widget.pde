class Widget {

  color fillColor;
  float diameter;
  boolean isRetired;
  float retiredX;
  float retiredY;

  Widget(float diameter) {
    fillColor = color(random(0, 40), 
      random(204, 255), 
      random(50, 255));
    this.diameter = random(diameter, diameter * 3);
  }

  void retire(float x, float y) {
    isRetired = true;
    retiredX = x;
    retiredY = y;
  }

  void display(float x, float y) {
    fill(fillColor);
    if (isRetired) {
      ellipse(retiredX + random(-3, 3), 
        retiredY + random(-3, 3), 
        diameter, diameter);
      fill(0);
      ellipse(retiredX, retiredY, 
        diameter - 5, diameter - 5);
    } else {
      ellipse(x + random(-10, 10), 
        y + random(-10, 10), 
        diameter, diameter);
      fill(16);
      ellipse(x, y, 
        diameter - 15, diameter - 15);
    }
  }
}