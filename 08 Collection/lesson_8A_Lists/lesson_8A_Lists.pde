// Unlike arrays, lists can grow in size dynamically.
// Processing has built in lists for integers, floats
// and strings, but you're better off learning the Java
// list. The first step to doing this is to import the
// library below.

import java.util.List;

// Clicker is an object which uses lists. Widgets are
// objects added to those lists.

Clicker c;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Lists");
  pixelDensity(displayDensity());
  size(420, 420);
  background(32);
  colorMode(HSB, 359, 99, 99);
  noStroke();

  c = new Clicker();

  // Built-in Processing lists.
  IntList pIntList = new IntList();
  StringList pStringList = new StringList();
  //FloatList pFloatList = new FloatList();
  
  // Integer list functionality.
  pIntList.append(25);
  pIntList.append(12);
  pIntList.append(17);
  pIntList.append(-6);
  pIntList.appendUnique(12);
  println("After appending values: " + pIntList);
  pIntList.div(0, 5); /* 25 / 5 = 5 */
  pIntList.add(1, 3); /* 12 + 3 = 15 */
  pIntList.sub(2, 4); /* 17 - 4 = 13 */
  pIntList.mult(3, 2); /* -6 * 2 = -12 */
  println("After math operations: " + pIntList);
  pIntList.sort();
  println("After sorting: " + pIntList);
  pIntList.shuffle();
  println("After shuffling: " + pIntList);
  println("Minimum value: " + pIntList.min());
  println("Maximum value: " + pIntList.max());

  // String list functionality.
  pStringList.append("Happy");
  pStringList.append("Sad");
  pStringList.append("Tired");
  pStringList.append("Focused");
  println("After appending values: " + pStringList);
  pStringList.sort();
  println("After sorting: " + pStringList);
  pStringList.shuffle();
  println("After shuffling: " + pStringList);
  pStringList.upper();
  println("To Upper Case: " + pStringList);
}

void draw() {
  fill(0, 0, 12, 32);
  rect(0, 0, width, height);
  c.drawWidgets(pmouseX, pmouseY, 25);
  c.showInstructions();
}

void keyPressed() {
  if (key == ' ') {
    c.removeWidget();
  }
}

void mouseReleased() {
  if (mouseButton == LEFT) {
    c.addWidget(25);
  } else if (mouseButton == RIGHT) {
    c.retireWidget();
  } else if (mouseButton == CENTER) {
    c.removeWidget();
  }
}