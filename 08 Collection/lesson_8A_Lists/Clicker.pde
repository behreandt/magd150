class Clicker {

  // Since a list can be a list of any type of object,
  // such as a Float or, in our case, a widget, we need
  // to tell the list what kind of objects we're putting
  // inside it. We use the < > angle brackets to specify
  // we want a list of widgets.

  List<Widget> widgets = new ArrayList<Widget>();
  List<Widget> retired = new ArrayList<Widget>();

  void addWidget(int diameter) {
    Widget widget = new Widget(diameter);

    // The add function adds a new object to the list.

    widgets.add(widget);
  }

  void drawWidgets(int x, int y, int diameter) {

    // The size function tells the number of items in
    // the list. Unlike arrays, we retrieve an item
    // from the list not with [ ] square brackets, but
    // with the get function.

    for (int i = 0, size = retired.size(); i < size; ++i) {
      retired.get(i).display(0, 0);
    }

    for (int i = 0, size = widgets.size(); i < size; ++i) {
      widgets.get(i).display(x
        + random(-diameter * i  * 0.5, diameter * i * 0.5), 
        y
        + random(-diameter * i  * 0.5, diameter * i  * 0.5));
    }
  }

  void removeWidget() {
    int size = widgets.size();
    if (size > 0) {

      // The remove function removes a widget from the list.

      widgets.remove(size - 1);
    } else {
      size = retired.size();
      if (size > 0) {
        retired.remove(size - 1);
      }
    }
  }

  void retireWidget() {
    int size = widgets.size();
    if (size > 0) {
      Widget removed = widgets.get(size -1);
      removed.retire(mouseX, mouseY);
      widgets.remove(size - 1);
      this.retired.add(removed);
    }
  }

  void showInstructions() {
    fill(255);
    text("Number of active widgets: " + widgets.size()
      + "\r\nNumber of retired widgets: " + retired.size()
      + "\r\n\r\nLeft click to add a widget."
      + "\r\nRight click to retire a widget."
      + "\r\nSpace to remove a widget.", 15, 15);
  }
}