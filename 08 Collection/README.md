# Assignment 8
## Collection
### 100 points

![Collection](preview.png)

### References

* Daniel Shiffman, [Video: ArrayLists in Processing](https://youtu.be/HnSJZ4qTcwY)
* Shiffman, [Video: Deleting objects from ArrayList](https://youtu.be/IsdZKG9wyBc)
* Shiffman, [_The Nature of Code_](http://natureofcode.com/book/chapter-4-particle-systems/index.html#43-the-arraylist) Section 4.3 The ArrayList
* [Introduction to Collections](https://docs.oracle.com/javase/tutorial/collections/intro/index.html)

### Instructions

1. Adhere to the theme to be determined in lab. (10 pts.)
    * Wednesday: Virus
    * Monday: Dreams

2. Use at least one [enumeration](https://docs.oracle.com/javase/tutorial/java/javaOO/enum.html). (10 pts.)

3. Use at least one of the following collections: (20 pts.)
    * [List](https://docs.oracle.com/javase/tutorial/collections/interfaces/list.html)
    * [Dictionary / Map](https://docs.oracle.com/javase/tutorial/collections/interfaces/map.html)
    * [Queue](https://docs.oracle.com/javase/tutorial/collections/interfaces/queue.html)

4. Define and construct an instance of one class. (10 pts.)

5. Add multiple instances of that class to a collection. (10 pts.)

6. Compose with the following in mind: (10 pts.)
    * Color: Are colors chosen with [intentionality](https://www.youtube.com/watch?v=_2LLXnUdUIc), according to a design pattern (monochromatic, analogous, complementary, triadic)?
    * Grouping: Are like or unlike elements grouped together, repeated at regular or irregular intervals? Are they combined to form a symbol or representational image?
    * Scale: How do the relative sizes of the elements influence our perception of them?

7. Ensure that the code contains at least 4 single- or multi-line comments, written in complete sentences, that explain what the code is doing at key steps. (10 pts.)

8. Ensure that the sketch runs without errors. The main tab of the sketch should be the only tab with global-scope setup and draw functions. (10 pts.)

9. Ensure that the main tab of the sketch shares the same name as the sketch folder. Name the sketch folder and main tab according to the following convention: s17_magd150_lab08_yourlastname. Compress the sketch folder into a .zip file and upload to D2L. (10 pts.)

__Any code or media within a submission are presumed to be the work of the student(s) unless otherwise stated. When media or code from outside sources are used, a link to the source and notice of attribution at the top of the main sketch in a comment should be provided. Media and code used are expected to be from the intellectual commons (i.e., not copyrighted or registered as a trademark) and to be altered substantially by the student(s). On first incident, students whose work does not meet this standard will have one week to resubmit the assignment for half credit. On second incident, students will be referred to Student Conduct Programs and will fail the course.__
