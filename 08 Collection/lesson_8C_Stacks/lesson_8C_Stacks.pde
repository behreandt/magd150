import java.util.Stack;
import java.util.Queue;
import java.util.LinkedList;

// Suppose we have a series of locations which can be
// visited only once by an agent, and then that we have
// a line of agents waiting to pick a location.

Stack<PVector> locations;
Queue<Widget> widgets;
int lCount, wCount;
float xCenter, yCenter;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Stacks");
  pixelDensity(displayDensity());
  size(640, 420);
  background(64);
  colorMode(HSB, 359, 99, 99);

  xCenter = width * 0.5;
  yCenter = height * 0.5;

  lCount = 128;
  wCount = 5;

  locations = new Stack<PVector>();
  float t = 0, 
    radius = 2.5;
  for (int i = 0; i < lCount; ++i) {
    t = map(i, 0, lCount, 0, TWO_PI);
    locations.push(new PVector(cos(t) * i * radius, 
      sin(t) * i * radius));
  }

  widgets = new LinkedList<Widget>();
  for (int i = 0; i < wCount; ++i) {
    widgets.add(new Widget(i));
  }
}

void draw() {
  background(32);
  translate(xCenter, yCenter);

  // Draw all the locations.
  for (PVector l : locations) {
    stroke(209, 49, 99, 204);
    strokeWeight(4);
    point(l.x, l.y);
  }

  // A foreach loop allows you to iterate over
  // all the elements of a collection without having
  // to declare an index. In this case, each
  // successive element of the collection takes on
  // the name 'w' within the loop.

  for (Widget w : widgets) {
    w.draw();
  }
}

void mousePressed() {

  // Adding to a queue what was just removed simulates
  // a leapfrogging of elements in the queue.

  widgets.add(widgets.remove());
  for (Widget w : widgets) {
    if (locations.size() > 0) {

      // Pop removes an item from a stack and returns
      // the element that was just removed.

      w.dest = locations.pop();
    } else {

      // If there are no more points on the spiral, then
      // refill locations with random points.

      float x, y;
      for (int i = 0; i < lCount; ++i) {
        x = random(-xCenter, xCenter);
        y = random(-yCenter, yCenter);
        locations.push(new PVector(x, y));
      }
    }
  }
}