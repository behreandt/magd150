class Widget {

  PVector pos, scl, dest;
  color stroke;
  float damping;

  Widget(int i) {
    pos = new PVector(random(-xCenter, xCenter), 
      random(-yCenter, yCenter));
    dest = pos.copy();
    scl = new PVector(24, 24);
    stroke = color(map(i, 0, wCount, 0, 360), 100, 100);
    damping = map(i, 0, wCount, 0.045, 0.05);
    ;
  }

  void draw() {
    pushStyle();
    noFill();
    stroke(stroke);
    strokeWeight(4);
    ellipse(pos.x, pos.y, scl.x, scl.y);
    pos.lerp(dest, damping);
    popStyle();
  }
}