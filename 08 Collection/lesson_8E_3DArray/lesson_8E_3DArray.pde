int rows, cols, layers;
PVector pos, scl, rot;
PVector[][][] points;

void setup() {
  surface.setResizable(true);
  surface.setTitle("3D Arrays");
  pixelDensity(displayDensity());
  size(420, 420, P3D);
  background(64);

  // 4 * 6 * 5 = 120
  rows = 4;
  cols = 6;
  layers = 5;

  // Three key transformations of the cube / matrix.
  pos = new PVector(width / 2.0, height / 2.0, 0);
  scl = new PVector(300, 300, 300);
  rot = new PVector();

  // Fills array with vectors recording x, y, z position.
  points = new PVector[rows][cols][layers];
  for (int x = 0, i = 0; x < rows; ++x) {
    for (int y = 0; y < cols; ++y) {
      for (int z = 0; z < layers; ++z, ++i) {
        // For convenience, the cube here is a unit in
        // scale (where unit = 0 to 1 when drawing from
        // the corner or -0.5 to 0.5 when drawing from
        // the center.
        points[x][y][z] = new PVector(
          map(x, -1, rows, -0.5, 0.5), 
          map(y, -1, cols, -0.5, 0.5), 
          map(z, -1, layers, -0.5, 0.5));
        println(nfs(i, 3) + " " + points[x][y][z]);
      }
    }
  }
}

void draw() {
  background(32);

  pos.x = width / 2.0;
  pos.y = height / 2.0;
  scl= new PVector(width * 0.75, height * 0.75, height * 0.75);
  rot.y = map(mouseX, 0, width, 0, TWO_PI);
  rot.x = map(mouseY, 0, height, 0, TWO_PI);

  pushMatrix();
  translate(pos);
  rotateZ(rot.z);
  rotateY(rot.y);
  rotateX(rot.x);
  scale(scl);
  strokeWeight(0.075);
  for (int x = 0; x < rows; ++x) {
    for (int y = 0; y < cols; ++y) {
      for (int z = 0; z < layers; ++z) {
        stroke(127 + x / (float)rows * 255, 
          54 + y / (float)cols * 255, 
          127 + z / (float)layers * 255, 
          127);
        point(points[x][y][z]);
      }
    }
  }
  popMatrix();
}

// On one hand, it is easier to work with
// x, y and z bundled together in a vector
// when working with 3D; on the other,
// Processing's many functions treat them
// as separate. These three functions are
// 'wrappers' intended to reduce the amount
// of typing needed.

void translate(PVector v) {
  translate(v.x, v.y, v.z);
}

void scale(PVector v) {
  scale(v.x, v.y, v.z);
}

void point(PVector v) {
  point(v.x, v.y, v.z);
}