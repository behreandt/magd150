Tile[][] tiles;
int rows, cols;

void setup() {
  surface.setTitle("Composition");
  pixelDensity(displayDensity());
  size(420, 420);
  background(64);
  noStroke();
  rows = 4;
  cols = 4;
  tiles = new Tile[rows][cols];
  for (int y = 0; y < rows; ++y) {
    for (int x = 0; x < cols; ++x) {
      tiles[y][x] = new Tile(x, y, 
        width / float(cols), height / float(rows), 
        color(map(x, 0, cols, 54, 255), 
        map(y, 0, rows, 54, 204), 127), 
        color(255, 180));
    }
  }
}

void draw() {
  background(32);
  for (int y = 0; y < rows; ++y) {
    for (int x = 0; x < cols; ++x) {
      tiles[y][x].draw();
      tiles[y][x].scalar = map(mouseX, 0, width, 0.25, 1.0);
    }
  }
}