class Tile {
  int x, y, step;
  float w, h, a, speed, scalar, radius;
  color fill1, fill2;

  Tile(int x, int y, float w, float h, 
    color fill1, color fill2) {
    this.x = x;
    this.y = y;
    this.step = this.x + this.y * cols;
    this.w = w;
    this.h = h;
    this.radius = min(w, h) / 2.0;
    this.fill1 = fill1;
    this.fill2 = fill2;
    this.a = map(step, 0, rows * cols, 0, TWO_PI);
    this.speed = map(step, 0, rows * cols, -0.05, 0.05);
    this.scalar = 0.5;
  }

  public String toString() {
    return "[ " + x + ", " + y + " ]";
  }

  public void draw() {
    a += speed;
    
    // Draw background.
    fill(fill1);
    rect(x * w, y * h, w, h);
    
    pushMatrix();
    pushStyle();
    translate(x * w + (w / 2.0), y * h + (h / 2.0));
    rotate(a);
    scale(radius, radius);
    fill(fill2);
    float a1 = 0, a2 = 0;
    
    // Draw star shape based on two angles.
    beginShape();
    for (int i = 0; i < step + 3; ++i) {
      a1 = map(i, 0, step + 3, 0, TWO_PI);
      a2 = map(i + 1, 0, step + 3, 0, TWO_PI);
      vertex(cos(a1), sin(a1));
      vertex(cos(a2) * scalar, sin(a2) * scalar);
    }
    
    // Draw cut-out inside star shape.
    beginContour();
    for (int i = 0; i < step + 3; ++i) {
      a1 = map(i, 0, step + 3, TWO_PI, 0);
      vertex(cos(a1) * scalar * .85, sin(a1) * scalar * .85);
    }
    endContour();
    endShape(CLOSE);
    popStyle();
    popMatrix();
  }
}