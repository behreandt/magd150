PVector pos, scale;
float a, speed;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Atan2");
  pixelDensity(displayDensity());
  size(420, 420);
  background(32);
  noStroke();
  pos = new PVector(width / 2.0, height / 2.0);
  scale  = new PVector(width /15.0, height / 15.0);
  a = 0;
  speed = 2;
}

void draw() {
  //background(32);

  // Atan2 can be used to convert two positions to
  // an angle of rotation. This is helpful for creating
  // projectiles, enemies and heroes which orient
  // toward the mouse or toward each other.
  a = atan2(mouseY - pos.y, mouseX - pos.x);

  pushMatrix();
  translate(pos.x, pos.y);
  rotate(a);
  scale(scale.x, scale.y);
  beginShape();
  fill(random(25, 127), 25, random(25, 255));
  vertex(1, 0);
  vertex(-0.5, -0.75);
  vertex(-0.25, 0);
  vertex(-0.5, 0.75);
  endShape();
  popMatrix();

  if (keyPressed) {
    if (key == CODED) {
      if (keyCode == UP) {
        pos.add(new PVector(cos(a) * speed, sin(a) * speed));
      }
      if (keyCode == DOWN) {
        pos.sub(new PVector(cos(a) * speed, sin(a) * speed));
      }
    }
  }
}