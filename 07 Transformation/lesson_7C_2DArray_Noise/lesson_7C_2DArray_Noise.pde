// Randomness is boring, because in a world where anything can happen,
// and do so entirely without cause, whatever does happen says nothing about
// what causal actions preceded it and gives us nothing to look forward to.
// Compared to randomness, noise is more interesting, in that it at least is
// smooth; it has a relationship to the random values neighboring it either
// spatially or temporally. Perlin noise is frequently used in procedural
// generation of landscapes in video games because of its smoothness.

int twoDArraySize = 42;

// A two-dimensional array is a more complex form of an array;
// it's commonly used to work directly with pixels, since the
// two dimensions of the array correspond to the x and y axis.
float[][] randoms = new float[twoDArraySize][twoDArraySize];
float[][] noise = new float[twoDArraySize][twoDArraySize];
color[][] randomColors = new color[twoDArraySize][twoDArraySize];
color[][] noiseyColors = new color[twoDArraySize][twoDArraySize];

float noiseIncr = 0.02;
float xOff = 0.0;
float yOff = 0.0;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Noise vs. Randomness");
  pixelDensity(displayDensity());
  size(630, 630);
  background(32);
  noStroke();
  colorMode(HSB, 360, 255, 255);
  noiseDetail(8);
  for (int y = 0; y < twoDArraySize; ++y) {
    yOff += noiseIncr;
    xOff = 0.0;
    for (int x = 0; x < twoDArraySize; ++x) {
      xOff += noiseIncr;
      randoms[x][y] = random(0, 255);
      noise[x][y] = noise(xOff, yOff) * 255;
      randomColors[x][y] = color(random(0, 360), random(127, 255), random(127, 255));
      noiseyColors[x][y] = color(noise(xOff, yOff) * 360, 127 + noise(xOff, yOff) * 255, 127 + noise(xOff, yOff) * 255);
    }
  }
}

void draw() {
  background(32);
  for (int y = 0; y < twoDArraySize; ++y) {
    for (int x = 0; x < twoDArraySize; ++x) {
      fill(randoms[x][y]);
      ellipse(x * 7.5 + 3.75, y * 7.5 + 3.75, 7.5, 7.5);
      fill(noise[x][y]);
      ellipse(width / 2.0 + x * 7.5 + 3.75, y * 7.5 + 3.75, 7.5, 7.5);
      fill(noiseyColors[x][y]);
      ellipse(width / 2.0 + x * 7.5 + 3.75, height / 2.0 + y * 7.5 + 3.75, 7.5, 7.5);
      fill(randomColors[x][y]);
      ellipse(x * 7.5 + 3.75, height / 2.0 + y + y * 7.5 + 3.75, 7.5, 7.5);
    }
  }
}