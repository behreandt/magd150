# Assignment 7
## Transformation
### 100 points

![Transformation](preview.png)

### References

* Daniel Shiffman, [_Learning Processing_](http://learningprocessing.com/) Chapters 13 - 14
* Shiffman, [Video: What is an Array?](https://youtu.be/NptnmWvkbTw)
* Shiffman, [Video: Vectors](https://youtu.be/mWJkvxQXIa8)
* Shiffman, [Video: Angles and Angular Motion](https://youtu.be/qMq-zd6hguc)
* [Tutorial: Trigonometry Primer](https://processing.org/tutorials/trig/)
* [Tutorial: 2D Transformations](https://processing.org/tutorials/transform2d/)

### Instructions

1. Adhere to the theme to be determined in lab. (10 pts.)
    * Wednesday: Carnival
    * Monday: Nature

2. Create and use at least 3 PVectors. (10 pts.)

3. Use at least 3 [PVector functions](https://processing.org/reference/PVector.html). (10 pts.)

4. Perform all three basic transformations on a shape. (20 pts.)
    * [Translate](https://processing.org/reference/translate_.html)
    * [Rotate](https://processing.org/reference/rotate_.html)
    * [Scale](https://processing.org/reference/scale_.html)

5. Create and use a one- or two-dimensional array. (20 pts.)

6. Ensure that the code contains at least 4 single- or multi-line comments, written in complete sentences, that explain what the code is doing at key steps. (10 pts.)

7. Ensure that the sketch runs without errors. The main tab of the sketch should be the only tab with global-scope setup and draw functions. (10 pts.)

8. Ensure that the main tab of the sketch shares the same name as the sketch folder. Name the sketch folder and main tab according to the following convention: s17_magd150_lab07_yourlastname. Compress the sketch folder into a .zip file and upload to D2L. (10 pts.)

__Any code or media within a submission are presumed to be the work of the student(s) unless otherwise stated. When media or code from outside sources are used, a link to the source and notice of attribution at the top of the main sketch in a comment should be provided. Media and code used are expected to be from the intellectual commons (i.e., not copyrighted or registered as a trademark) and to be altered substantially by the student(s). On first incident, students whose work does not meet this standard will have one week to resubmit the assignment for half credit. On second incident, students will be referred to Student Conduct Programs and will fail the course.__
