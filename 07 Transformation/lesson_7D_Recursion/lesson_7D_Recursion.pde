// A recursive function calls itself. Like using for-,
// while-, and do-while-loops, this can be problematic,
// because you do not want a function to call itself
// infinitely, causing your computer to freeze up. Therefore,
// all recursive functions should have a base case, in which
// they do not call themselves, but perform a very simple
// operation. Classic recursive functions calculate factorials,
// Fibonacci sequences, walk trees and merge-sort.

float theta, scalar = 15;
int maxRecursion = 18;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Recursion");
  pixelDensity(displayDensity());
  fullScreen();
  background(32);
  ellipseMode(RADIUS);
  rectMode(RADIUS);
  noStroke();

  // Print factorial 5.
  println("5! = 5 * 4 * 3 * 2 * 1 = " + factorial(5) + "\r\n");

  // Print the first 10 numbers in the Fibonacci sequence.
  for (int i = 0; i < 10; ++i) {
    print(fibonacci(i));
    if (i < 9) {
      print(", ");
    }
  }
  println("\r\n");

  // Test whether or not a phrase is a palindrome.
  println("sator arepo tenet opera rotas: " + isAPalindrome("sator arepo tenet opera rotas"));
  println("abcdef: " + isAPalindrome("abcdef") + "\r\n");
}

void draw() {
  background(32);
  theta = map(mouseX, 0, width, 0, TWO_PI);
  visualRecursion(width * 0.25, height - 100, 100, theta, maxRecursion);
  visualRecursion(width * 0.75, height - 100, 100, TWO_PI - theta, maxRecursion);
}

void visualRecursion(float x, float y, float r, float theta, int itr) {
  fill(map(itr, 0, maxRecursion, 0, 255), 
    127, 
    map(itr, 0, maxRecursion, 255, 0), 
    127);
  if (itr % 2 == 0) {
    ellipse(x + cos(theta) * scalar * itr, 
      y + sin(theta) * scalar * itr, r, r);
  } else {
    quad(x + r + cos(theta) * scalar * itr, y + sin(theta) * scalar * itr, 
      x + cos(theta) * scalar * itr, y + r + sin(theta) * scalar * itr, 
      x - r + cos(theta) * scalar * itr, y + sin(theta) * scalar * itr, 
      x  + cos(theta) * scalar * itr, y - r + sin(theta) * scalar * itr);
  }

  if (itr > 1) {
    visualRecursion(x, y - r, r * 0.825, theta, --itr);
  }
}

int factorial(int n) {
  // n == 0 is the base case
  if (n == 0) {
    return 1;
  } else {
    return (n * factorial(n - 1));
  }
}

int fibonacci(int n) {
  // n <= 1 is the base case
  if (n <= 1) {
    return n;
  } else {
    return fibonacci(n - 1) + fibonacci(n - 2);
  }
}

boolean isAPalindrome(String s) {

  // If the string is of length 0 or 1, then no more comparisons are possible.
  if (s.length() <= 1) {
    return true;

    // If the string is 2 or more characters long, it fails the first if check.
    // The next check is to see if the first and last character in the string are
    // equal. If so, then a substring which chops off those first and last
    // characters of the string are passed into a recursive call.
  } else if (s.charAt(0) == s.charAt(s.length() - 1)) {
    return isAPalindrome(s.substring(1, s.length() - 1));

    // If the first and last characters in the String are not equal, then
    // the String is not a palindrome.
  } else {
    return false;
  }
}