PVector center1, center2, center3, center4, 
  sample1, sample2, sample3, sample4, 
  scale;
float theta;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Transformations");
  pixelDensity(displayDensity());
  size(420, 420);
  background(64);
  textAlign(LEFT, CENTER);

  // The three basic transformations are to translate
  // (change the position), rotate (change the angle of
  // orientation, and scale (change the size) of a shape.
  center1 = new PVector(width * 0.25, height * 0.25);
  scale = new PVector(width * 0.2, height * 0.125);
  theta = 0;

  center2 = new PVector(width * 0.75, height * 0.25);
  center3 = new PVector(width * 0.25, height * 0.75);
  center4 = new PVector(width * 0.75, height * 0.75);
}

void draw() {
  background(32);

  theta = map(mouseX, 0, width, 0, TWO_PI);
  scale.y = map(mouseY, 0, height, height * 0.075, width * 0.2);

  pushStyle();
  fill(255, 127, 0);
  noStroke();
  rectMode(RADIUS);

  // Functions like translation, rotation and scale need
  // to be encapsulated with pushMatrix and popMatrix to
  // make them influence only the desired shape.
  pushMatrix();

  // The default origin for a Processing sketch is (0, 0),
  // but translate changes that origin to a spot of your
  // choosing.
  translate(center1.x, center1.y);

  // Rotate changes the angle of the whole world or sketch,
  // so if you don't translate, or set your shape to an offset
  // after you pushMatrix, then your rotation will be lop-sided.
  // Note that 0 degrees of rotation is (1, 0) on the X-axis, and
  // that because (0, 1) is down, positive rotation is clockwise.
  rotate(theta);

  // If you scale before you rotate, then the shape will 
  // become distorted as its angle changes. Scale can be used
  // with just one input if you want to scale all dimensions
  // uniformly.
  scale(scale.x, scale.y);
  rect(0, 0, 1, 1);

  // These three transformations introduce the problem of a
  // coordinate being different in local space than in world
  // space. For example, the coordinate (1, 1), the bottom-right
  // corner of the rectangle, is fixed in local space, but
  // changes rapidly in world space. Using the screenX and
  // screenY positions will let you map the two coordinates.
  // Notice this is done before popMatrix.
  sample1 = new PVector(screenX(1, 0), screenY(1, 0));
  popMatrix();
  popStyle();

  // Ellipse
  pushStyle();
  noStroke();
  ellipseMode(RADIUS);
  fill(0, 255, 127);
  pushMatrix();
  translate(center2.x, center2.y);
  rotate(-theta);
  scale(scale.x, scale.y);
  ellipse(0, 0, 1, 1);
  sample2 = new PVector(screenX(1, 0), screenY(1, 0));
  popMatrix();
  popStyle();

  // Triangle
  pushStyle();
  noStroke();
  fill(0, 127, 255);
  pushMatrix();
  translate(center3.x, center3.y);
  rotate(-theta);
  scale(scale.x, scale.y);
  triangle(1, 0, -.5, .87, -.5, -.87);
  sample3 = new PVector(screenX(1, 0), screenY(1, 0));
  popMatrix();
  popStyle();

  // Hexagon
  pushStyle();
  noStroke();
  fill(255, 0, 127);
  pushMatrix();
  translate(center4.x, center4.y);
  rotate(theta);
  scale(scale.x, scale.y);
  beginShape();
  vertex(1, 0);
  vertex(.5, .87);
  vertex(-.5, .87);
  vertex(-1, 0);
  vertex(-.5, -.87);
  vertex(.5, -.87);
  endShape();
  sample4 = new PVector(screenX(1, 0), screenY(1, 0));
  popMatrix();
  popStyle();

  // Note that screenX and screenY don't account for
  // high density display.
  if(displayDensity() == 2) {
    sample1.mult(0.5);
    sample2.mult(0.5);
    sample3.mult(0.5);
    sample4.mult(0.5);
  }

  // Samples of global coordinate space.
  strokeWeight(6);
  stroke(255);
  point(sample1.x, sample1.y);
  point(sample2.x, sample2.y);
  point(sample3.x, sample3.y);
  point(sample4.x, sample4.y);
  text("(" + nf(sample1.x, 1, 1) +", "
    + nf(sample1.y, 1, 1) + ")", 
    sample1.x + 4, sample1.y);
  text("(" + nf(sample2.x, 1, 1) +", "
    + nf(sample2.y, 1, 1) + ")", 
    sample2.x + 4, sample2.y);
  text("(" + nf(sample3.x, 1, 1) +", "
    + nf(sample3.y, 1, 1) + ")", 
    sample3.x + 4, sample3.y);
  text("(" + nf(sample4.x, 1, 1) +", "
    + nf(sample4.y, 1, 1) + ")", 
    sample4.x + 4, sample4.y);
}