/* SOURCES AND REFERENCE:
 * https://youtu.be/A86COO8KC58 ,
 * https://youtu.be/4bIsntTiKfM ,
 * http://processingjs.org/learning/custom/intersect/
 */

// Line 1 variables.
float ax1, ay1, bx1, by1, rise1, run1, c1, 
  // Line 2 variables.
  ax2, ay2, bx2, by2, rise2, run2, c2, 
  // Point intersecting both lines.
  denominator, xIntersect, yIntersect, 
  // Variables to deal with line segments rather than infinite lines.
  segmentX1, segmentY1, segmentX2, segmentY2;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Line-Line Intersection");
  pixelDensity(displayDensity());
  size(420, 420);
  background(32);
  strokeWeight(1.5);
  stroke(255);
  textAlign(LEFT, CENTER);

  // Line1.
  ax1 = random(5, width - 5);
  bx1 = random(5, width - 5);
  ay1 = random(5, height - 5);
  by1 = random(5, height - 5);

  // Line2.
  ax2 = random(5, width - 5);
  bx2 = random(5, width - 5);
  ay2 = random(5, height - 5);
  by2 = random(5, height - 5);
}

void draw() {
  background(32);

  // Change the location of a line's end points
  // when the user clicks the mouse.
  if (mousePressed) {
    if (mouseButton == LEFT) {
      ax1 = mouseX;
      ay1 = mouseY;
    } else if (mouseButton == RIGHT) {
      ax2 = mouseX;
      ay2 = mouseY;
    }
  }

  // General equation of a line: ax + by = c.

  // Line1.
  rise1 = by1 - ay1;
  run1 = ax1 - bx1;
  c1 = rise1 * ax1 + run1 * ay1;

  // Show rise and run for Line1.
  strokeWeight(1);
  stroke(64);
  fill(64);
  line(ax1, ay1, ax1, ay1 + rise1); /* Rise. */
  line(ax1, by1, ax1 - run1, by1); /* Run. */

  // Line2.
  rise2 = by2 - ay2;
  run2 = ax2 - bx2;
  c2 = rise2 * ax2 + run2 * ay2;

  // Show rise and run for line 2.
  line(ax2, ay2, ax2, ay2 + rise2); /* Rise. */
  line(ax2, by2, ax2 - run2, by2); /* Run. */

  denominator = rise1 * run2 - rise2 * run1;

  // We don't want a divide by 0 exception if the lines
  // are parallel or colinear.
  if (denominator != 0) {
    xIntersect = (run2 * c1 - run1 * c2) / denominator;
    yIntersect = (rise1 * c2 - rise2 * c1) / denominator;
  }

  // Calculation for line segments.
  segmentX1 = (xIntersect - ax1) / (bx1 - ax1);
  segmentY1 = (yIntersect - ay1) / (by1 - ay1);
  segmentX2 = (xIntersect - ax2) / (bx2 - ax2);
  segmentY2 = (yIntersect - ay2) / (by2 - ay2);

  // Line 1.
  strokeWeight(1.5);
  stroke(255);
  line(ax1, ay1, bx1, by1);

  // Line 2.
  line(ax2, ay2, bx2, by2);

  // Origin points (moveable).
  strokeWeight(6);
  stroke(255, 54, 0);
  point(ax1, ay1);
  stroke(127, 204, 0);
  point(ax2, ay2);

  // Destination points (unmoveable).
  stroke(0, 127, 255);
  point(bx1, by1);
  stroke(255, 0, 127);
  point(bx2, by2);

  // Intersection point, if any.
  if (denominator != 0
    && ((segmentX1 >= 0 && segmentX1 <= 1) ||
    (segmentY1 >= 0 && segmentY1 <= 1))
    && ((segmentX2 >= 0 && segmentX2 <= 1) ||
    (segmentY2 >= 0 && segmentY2 <= 1))) {
    stroke(255, 255, 0);
    point(xIntersect, yIntersect);
  }
}