// Circle 1
float x1, y1, r1;
// Circle 2
float x2, y2, r2;
float circleDist;
color fill1, fill2;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Circle-Circle Intersection");
  pixelDensity(displayDensity());
  size(420, 420);
  background(32);
  strokeWeight(1.5);
  stroke(255);

  // The intersection between two circles is
  // easiest to calculate using the circles' radii, so
  // we will switch the way Processing interprets the
  // information given to an ellipse to radius.
  ellipseMode(RADIUS);

  // Circle 1
  r1 = random(width / 8.0, width / 5.0);
  x1 = random(r1, width - r1);
  y1 = random(r1, height - r1);
  fill1 = color(255, 0, 0, 127);

  // Circle 2
  r2 = random(width / 10.0, width / 8.0);
  x2 = random(r2, width - r2);
  y2 = random(r2, height - r2);
  fill2 = color(0, 0, 255, 127);
}

void draw() {
  background(32);

  x2 = mouseX;
  y2 = mouseY;

  // Circle 1
  fill(fill2);
  ellipse(x1, y1, r1, r1);
  point(x1, y1);

  // Circle 2
  fill(fill1);
  ellipse(x2, y2, r2, r2);
  point(x2, y2);

  // Formula for finding the intersection of two circles:
  // if the distance between two center points of a circle
  // is less than the sum of the circles' radii, then they
  // intersect.
  circleDist = dist(x1, y1, x2, y2);

  if (circleDist < r1 + r2) {
    // Turn the circles teal and orange if they intersect
    // without one being entirely within another.
    fill1 = color(255, 127, 0, 127);
    fill2 = color(0, 127, 255, 127);
    line(x1, y1, x2, y2);
    line(x1, y1, x1 + r1, y1);
    line(x2, y2, x2 + r2, y2);

    // Formula for finding if one circle is inside the other:
    // if the distance between two circles is less than the
    // absolute value of the difference between their radii,
    // then one is within the other.
    if (circleDist < abs(r1 - r2)) {
      // Turn the circles gray if one is within the other.
      fill1 = color(255, 127);
      fill2 = color(127, 64);
    }
  } else {
    // Turn the circles red and blue with no intersection.
    fill1 = color(255, 0, 0, 127);
    fill2 = color(0, 0, 255, 127);
  }
}