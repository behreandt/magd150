// The point which will be intersecting with a rectangle.
float pointX, pointY;
// The rectangle's edges and center point.
float left, top, right, bottom, centerX, centerY;
// The fill color of the rectangle, which will change based
// on intersection.
color rectFill;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Point-Rectangle Intersection");
  pixelDensity(displayDensity());
  size(420, 420);
  background(32);
  strokeWeight(1.5);
  stroke(255);

  // The intersection between a rectangle and a point is
  // easiest to calculate using the rectangle's edges, so
  // we will switch the way Processing interprets the
  // information given to a a rectangle to corners.
  rectMode(CORNERS);

  rectFill = color(255, 0, 0, 127);
  left = random(width / 3.0 - width / 15.0, width / 3.0 + width / 15.0);
  top = random(height / 3.0 - height / 15.0, height / 3.0 + height / 15.0);
  right = random(width * 2 / 3.0 - width / 15.0, width * 2 / 3.0 + width / 15.0);
  bottom = random(height * 2 / 3.0 - height / 15.0, height * 2 / 3.0 + height / 15.0); 

  // With rectMode set to CORNERS above, we can calculate
  // the center of an axis-aligned rectangle by finding
  // the width of the rectangle by subtracting the left
  // edge from the right. We then divide that by half to 
  // find the halfway point. We then add that to the left
  // edge. We then do the same for the vertical center:
  // subtract the top from the bottom, divide by two, and
  // add it to the top edge.
  centerX = left + (right - left) / 2.0;
  centerY = top + (bottom - top) / 2.0;
}

void draw() {
  background(32);

  pointX = mouseX;
  pointY = mouseY;

  // Draw the rectangle.
  strokeWeight(1.5);
  fill(rectFill);
  rect(left, top, right, bottom);
  point(centerX, centerY);

  // Draw the point.
  strokeWeight(3);
  point(pointX, pointY);

  // If a point is within the four edges
  // of the rectangle, then there's an intersection.
  if (pointX > left
    && pointX < right
    && pointY > top
    && pointY < bottom) {
    strokeWeight(1);
    line(centerX, centerY, pointX, pointY);
    // The rectangle is teal when there is no intersection.
    rectFill = color(0, 204, 255, 127);
  } else {
    // The rectangle is red when there is no intersection.
    rectFill = color(255, 0, 0, 127);
  }
}