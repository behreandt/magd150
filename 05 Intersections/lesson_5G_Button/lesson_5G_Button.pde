// These five variables govern the shape of the button.
float buttonX, buttonY, w, h, rounding, 
  // If we want the button to grow larger and smaller in
  // response to the mouse, we can use variables to store
  // the larger and smaller size.
  dilatedW, dilatedH, contractedW, contractedH, 
  // This governs how quickly or smoothly the rectangle
  // will shrink or enlarge (from 0 to 1).
  fadeTime, 
  // Text display options.
  textSize, dilatedTextSize, contractedTextSize, 
  smileyX, smileyY, smileyRadius;
// These colors will allow the button to respond to the
// mouse in color as well as size.
color hoverFill, inactiveFill, activeFill, fill;
boolean toggle;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Button");
  pixelDensity(displayDensity());
  size(420, 420);
  background(64);

  // Variables for button shape.
  rectMode(RADIUS);
  buttonX = width * 0.25;
  buttonY = height * 0.5;
  contractedW = 80;
  contractedH = 15;
  dilatedW = contractedW * 1.125;
  dilatedH = contractedH * 1.125;
  w = contractedW;
  h = contractedH;
  rounding = 5;

  // Variables for button color.
  colorMode(HSB, 360, 100, 100);
  hoverFill = color(210, 100, 100);
  inactiveFill = color(210, 100, 70);
  activeFill = color(270, 100, 100);
  fill = inactiveFill;
  fadeTime = 0.05;

  // Changes the alignment and
  // size of the text which appears
  // in the button.
  textAlign(CENTER, CENTER);
  contractedTextSize = 16;
  dilatedTextSize = 20;
  textSize = contractedTextSize;

  // Variables for smiley face.
  ellipseMode(RADIUS);
  smileyX = width * 0.75;
  smileyY = height * 0.5;
  smileyRadius = 75;
}

void draw() {
  background(32);

  // Calculate the button's size and color based on whether
  // or not the mouse is hovering over it.
  if (mouseX > buttonX - w
    && mouseX < buttonX + w
    && mouseY > buttonY - h
    && mouseY < buttonY + h) {
    // cursor(KEYWORD) allows you to change the cursor's
    // appearance based on a capitalized keyword.
    cursor(HAND);

    // lerpColor(fromColor, toColor, fadeTime);
    // Allows you to smoothly transition from one
    // color to another.
    fill = lerpColor(fill, hoverFill, fadeTime);

    w = lerp(w, dilatedW, fadeTime);
    h = lerp(h, dilatedH, fadeTime);
    textSize = lerp(textSize, dilatedTextSize, fadeTime);

    if (mousePressed) {
      fill = activeFill;
    }
  } else {
    cursor(ARROW);

    fill = lerpColor(fill, inactiveFill, fadeTime);

    w = lerp(w, contractedW, fadeTime);
    h = lerp(h, contractedH, fadeTime);
    textSize = lerp(textSize, contractedTextSize, fadeTime);
  }

  // Draw the button.
  noStroke();
  fill(fill);
  rect(buttonX, buttonY, w, h, rounding);
  fill(0, 0, 100);
  textSize(textSize);
  text("CLICK ME!", buttonX, buttonY);

  // If the button is clicked, then draw a smiley face.
  if (toggle) {
    noStroke();
    fill(60, 100, 100);
    ellipse(smileyX, smileyY, smileyRadius, smileyRadius);
    fill(60, 40, 100);
    ellipse(smileyX * 1.05, smileyY * 0.925, 
      smileyRadius / 2.0, smileyRadius / 2.0);
    fill(0, 0, 0);
    ellipse(smileyX * 0.915, smileyY * 0.925, 
      smileyRadius / 5.0, smileyRadius / 5.0);
    ellipse(smileyX * 1.085, smileyY * 0.925, 
      smileyRadius / 5.0, smileyRadius / 5.0);
    fill(0, 0, 0);
    arc(smileyX, smileyY * 1.05, 
      smileyRadius * 2 / 3.0, smileyRadius * 2 / 3.0, 
      0, PI);
  }
}

void mousePressed() {
  if (mouseX > buttonX - w
    && mouseX < buttonX + w
    && mouseY > buttonY - h
    && mouseY < buttonY + h) {
    toggle = !toggle;
  }
}