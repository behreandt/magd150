# Assignment 5
## Intersections
### 100 points

![Intersections](preview.png)

### References

* Daniel Shiffman, [_Learning Processing_](http://learningprocessing.com/examples/) Chapter 5
* Shiffman, [Video: Conditionals](https://youtu.be/wsI6N9hfW7E?list=PLRqwX-V7Uu6YqykuLs00261JCqnL_NNZ_)
* Jeff Thompson, [Collision Detection](http://jeffreythompson.org/collision-detection/)

### Instructions

1. Adhere to the theme to be determined in lab. (10 pts.)
    * Wednesday: Menus to An Alternate Dimension
    * Monday: Television

2. Create a circular button which responds to the mouse. (20 pts.)

3. Create a rectangular button which responds to the mouse. (20 pts.)

4. Make an event happen when a button is clicked. (10 pts.)

5. Give each button a text label. (10 pts.)

6. Compose with the following in mind: (10 pts.)
    * Color: Are colors chosen with [intentionality](https://www.youtube.com/watch?v=_2LLXnUdUIc), according to a design pattern (monochromatic, analogous, complementary, triadic)?
    * Grouping: Are like or unlike elements grouped together, repeated at regular or irregular intervals? Are they combined to form a symbol or representational image?
    * Scale: How do the relative sizes of the elements influence our perception of them?

7. Ensure that the sketch runs without errors. The main tab of the sketch should be the only tab with global-scope setup and draw functions. (10 pts.)

8. Ensure that the main tab of the sketch shares the same name as the sketch folder. Name the sketch folder and main tab according to the following convention: s17_magd150_lab05_yourlastname. Compress the sketch folder into a .zip file and upload to D2L. (10 pts.)

__Any code or media within a submission are presumed to be the work of the student(s) unless otherwise stated. When media or code from outside sources are used, a link to the source and notice of attribution at the top of the main sketch in a comment should be provided. Media and code used are expected to be from the intellectual commons (i.e., not copyrighted or registered as a trademark) and to be altered substantially by the student(s). On first incident, students whose work does not meet this standard will have one week to resubmit the assignment for half credit. On second incident, students will be referred to Student Conduct Programs and will fail the course.__
