// The background will change to the 'on' color
// when the mouse pointer is over the triangle
// color. When the mouse pointer is not, then
// the background will change to the 'off' color.
color triangle, bkgOn, bkgOff;

// The triangle's three corners.
float v1x, v1y, v2x, v2y, v3x, v3y, 
  // Three new corners for the triangle to travel to.
  v1xd, v1yd, v2xd, v2yd, v3xd, v3yd, 
  // Speed of travel from vnx to vnxd (between 0 and 1).
  // This number will be plugged into the lerp function.
  travel = 0.05;
  
void setup() {

  // Setting the pixel density to a high density display
  // is not recommended here, since it messes with Processing's
  // ability to acquire the accurate color at a coordinate.
  // See https://github.com/processing/processing/issues/3364 .
  //pixelDensity(displayDensity());

  surface.setTitle("Pixel Color Intersection");
  surface.setResizable(true);
  size(420, 420);
  background(32);
  triangle = color(#981DBF);
  bkgOn = color(#A5F58D);
  bkgOff = color(#F5AE8D);

  // give the triangle a random starting point.
  v1x = random(width);
  v2x = random(width);
  v3x = random(width);

  v1y = random(height);
  v2y = random(height);
  v3y = random(height);

  // give the triangle a random destination point.
  v1xd = random(width);
  v2xd = random(width);
  v3xd = random(width);

  v1yd = random(height);
  v2yd = random(height);
  v3yd = random(height);
}

void draw() {  
  // Each pixel contains a color at a location on the
  // screen. This loads the color informaiton of all
  // the pixels.
  loadPixels();

  // get(x, y) gets the color at an (x, y) coordinate.
  // This checks to see if the color under the mouse
  // is equal to the fill of the triangle, if so, then
  // the mouse is over the triangle.
  if (get(mouseX, mouseY) == triangle) {
    fill(bkgOn);
  } else {
    fill(bkgOff);
  }
  rect(0, 0, width, height);

  // Draw the triangle.
  noStroke();
  fill(triangle);
  triangle(v1x, v1y, v2x, v2y, v3x, v3y);

  // Every 200 frames, change the triangle's destination
  // vertices to random locations.
  if (frameCount % 200 == 0) {
    v1xd = random(width * 0.05, width * 0.95);
    v2xd = random(width * 0.05, width * 0.95);
    v3xd = random(width * 0.05, width * 0.95);

    v1yd = random(height * 0.05, height * 0.95);
    v2yd = random(height * 0.05, height * 0.95);
    v3yd = random(height * 0.05, height * 0.95);
  }

  // Linear interpolation between the triangle's
  // present location and its destination.
  v1x = lerp(v1x, v1xd, travel);
  v1y = lerp(v1y, v1yd, travel);

  v2x = lerp(v2x, v2xd, travel);
  v2y = lerp(v2y, v2yd, travel);

  v3x = lerp(v3x, v3xd, travel);
  v3y = lerp(v3y, v3yd, travel);
}