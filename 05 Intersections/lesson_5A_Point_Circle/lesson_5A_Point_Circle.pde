// The point which will be intersecting with a circle.
float pointX, pointY;
// The circle's center coordinate pair and radius.
float circleX, circleY, radius;
// The fill color of the circle, which will change based
// on intersection.
color circleFill;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Point-Circle Intersection");
  pixelDensity(displayDensity());
  size(420, 420);
  background(32);
  strokeWeight(1.5);
  stroke(255);

  // The intersection between a circle and a point is
  // easiest to calculate using the circle's radius, so
  // we will switch the way Processing interprets the
  // information given to an ellipse to radius.
  ellipseMode(RADIUS);

  radius = random(60, 90);

  // If we gave our circles a random position from 0 to
  // width and from 0 to height, then it would be possible
  // for half the circle to go off-screen, since circleX
  // and circleY are at the center of the circle, not the edge.
  circleX = random(radius, width - radius);
  circleY = random(radius, height - radius);
  circleFill = color(255, 0, 0, 127);
}

void draw() {
  background(32);

  pointX = mouseX;
  pointY = mouseY;

  // Draw the circle.
  strokeWeight(1.5);
  fill(circleFill);
  ellipse(circleX, circleY, radius, radius);
  point(circleX, circleY);

  // Draw the point.
  strokeWeight(3);
  point(pointX, pointY);

  // Formula for finding the intersection of a point
  // and circle: if the distance between the point and
  // the center of the circle is less than circle's
  // radius, then the point and circle intersect.
  if (dist(pointX, pointY, circleX, circleY) < radius) {
    // The circle is teal when there is no intersection.
    circleFill = color(0, 204, 255, 127);
    strokeWeight(1);
    line(pointX, pointY, circleX, circleY);
    line(circleX, circleY, circleX + radius, circleY);
  } else {
    // The circle is red when there is no intersection.
    circleFill = color(255, 0, 0, 127);
  }
}