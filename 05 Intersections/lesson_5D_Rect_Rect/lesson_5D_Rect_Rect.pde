float left1, top1, right1, bottom1, centerX1, centerY1;
float left2, top2, right2, bottom2, centerX2, centerY2;
float halfWidth, halfHeight;

color fill1, fill2;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Rectangle-Rectangle Intersection");
  pixelDensity(displayDensity());
  size(420, 420);
  noCursor();
  background(32);
  strokeWeight(1.5);
  stroke(255);
  rectMode(CORNERS);

  // Rectangle1, whose edges are set to random places
  fill1 = color(0, 255, 0, 127);
  left1 = random(20, width / 3.0);
  top1 = random(20, height / 3.0);
  right1 = random(width * 2 / 3.0, width - 20);
  bottom1 = random(height * 2 / 3.0, height - 20);

  centerX1 = left1 + (right1 - left1) / 2.0;
  centerY1 = top1 + (bottom1 - top1) / 2.0;

  // Since rectangle2's center stays with the mouse, the calculations
  // of its four edges will be done differently than rectangle 1.
  fill2 = color(255, 0, 255, 127);
  halfWidth = width / 13.0;
  halfHeight = height / 13.0;
}

void draw() {
  background(32);

  // Update Rectangle 2
  // based on the mouse's position.
  centerX2 = mouseX;
  centerY2 = mouseY;
  left2 = mouseX - halfWidth;
  right2 = mouseX + halfWidth;
  top2 = mouseY - halfHeight;
  bottom2 = mouseY + halfHeight;

  // Rectangle1
  fill(fill1);
  rect(left1, top1, right1, bottom1);
  point(centerX1, centerY1);

  // Rectangle2
  fill(fill2);
  rect(left2, top2, right2, bottom2);
  point(centerX2, centerY2);

  // Formula for finding the intersection of two rectangles.
  // All four of these greater than / less than comparisons
  // must be true for the color to change.
  if (left1 < right2
    && left2 < right1
    && top1 < bottom2
    && top2 < bottom1) {
    // Turn the rectangles teal and orange if they intersect
    // without one being entirely within another.
    fill1 = color(255, 127, 0, 127);
    fill2 = color(0, 127, 255, 127);

    // Formula for finding if one rect is inside the other.
    // There's no point in doing these calculations unless
    // the rectangles already overlap.
    if (left1 < left2
      && right1 > right2
      && top1 < top2
      && bottom1 > bottom2) {
      // Turn the rectangles gray if one is within the other.
      fill2 = color(255, 127);
      fill1 = color(127, 64);
    }
  } else {
    // Turn the rectangles red and blue with no intersection.
    fill1 = color(255, 0, 0, 127);
    fill2 = color(0, 0, 255, 127);
  }
}