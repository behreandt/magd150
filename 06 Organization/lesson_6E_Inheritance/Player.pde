class Player extends Actor {

  float damping = 0.05;

  Player(String name, int hpMax) {
    super(name, hpMax);
    radius  = height / 12.0;
    fill = color(242, 204, 47);
  }

  public String toString() {
    return "Player: " + name;
  }

  @Override
    void move() {
    x = lerp(x, mouseX, damping);
    y = lerp(y, mouseY, damping);
  }

  void collide(Actor[] actors) {
    for (int i = 0, size = actors.length; i < size; ++i) {

      // The instanceof keyword can be used to check if an instance
      // of an object is of a certain data type. We don't care about
      // the player instance colliding with itself.

      if (actors[i] instanceof Player == false) {
        float dist = dist(x, y, actors[i].x, actors[i].y);

        // If the distance between two circles is less than the sum
        // of their diameter, then the circles are colliding.

        if (dist < radius + actors[i].radius) {
          if (actors[i] instanceof Opponent) {
            
            // If the colliding actor is an opponent, then decrease
            // the player's HP.
            
            this.hpCurrent = constrain(hpCurrent - 1, 0, hpMax);
          } else {
            
            // If the colliding actor is not an opponent, then
            // bounce it off of the player.
            
            actors[i].xDir = -actors[i].xDir;
            actors[i].yDir = -actors[i].yDir;
          }
        }
      }
    }
  }
}