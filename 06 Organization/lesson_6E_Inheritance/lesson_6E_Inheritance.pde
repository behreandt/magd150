// The classes Opponent and Player inherit from, or extend,
// the parent or super class Actor.

//      Actor
//      /  \
// Player  Opponent

// A common phrase is to say that classes have IS A and HAS A
// relationships: a player IS A(n) actor, and HAS A name.
// the inheritance relationship goes only one direction. The child
// or sub-classes Opponent and Player can use and refine the
// variables and functions of the parent or super-class Actor
// but Actor cannot use methods and variables of its children.

// A one-dimensional array of objects. We could initialize this
// with curly braces like in the commented out code below. However
// the contructors of these objects determines the radii of the
// ellipses based on the dimensions of the screen, which hasn't
// been determined by the setup() function yet.

// Actor[] actors = {
//    new Player("Alicia"), 
//    new Actor("Patricia"), 
//    new Opponent("Samuel"), 
//    new Actor("Hank")
//  };

Actor[] actors;
int actorSize = 4;

PFont times;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Inheritance");
  pixelDensity(displayDensity());
  background(32);
  size(420, 420);
  ellipseMode(RADIUS);
  noStroke();

  times = createFont("Times-Roman", 36);
  textFont(times, min(width, height) / 20);
  textAlign(CENTER, CENTER);

  actors = new Actor[actorSize];
  actors[0] = new Player("Alicia", 125);
  actors[1] = new Actor("Patricia");
  actors[2] = new Opponent("Samuel", 50);
  actors[3] = new Actor("Hank");

  for (int i = 0; i < actorSize; ++i) {
    actors[i].x = random(actors[i].radius, width - actors[i].radius);
    actors[i].y = random(actors[i].radius, height - actors[i].radius);
  }
}

void draw() {
  fill(32, 32, 32, 54);
  rect(0, 0, width, height);


  // When a child or sub-class's functionality overrides
  // the parent's functionality, this is called polymorphism.
  // move() is polymorphic, but show() is not.
  for (int i = 0; i < actorSize; ++i) {
    actors[i].move();
    actors[i].show();
    actors[i].collide(actors);
  }
}

void mousePressed() {
  exit();
}