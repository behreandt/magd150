class Actor {

  String name;
  int hpCurrent, hpMax;
  float x, y, radius, 
    xSpeed, ySpeed, 
    xDir, yDir;
  color fill;

  Actor() {
    this("Anonymous", 100);
  }

  Actor(String name) {
    this(name, 100);
  }

  Actor(String name, int hpMax) {
    this.name = name;
    this.hpMax = hpMax;
    hpCurrent = hpMax;
    radius = min(height, width) * 0.0714;
    x = random(radius, width - radius);
    y = random(radius, height - radius);
    fill = color(174, 221, 60);
    xSpeed = random(1, 8);
    ySpeed = random(1, 8);
    float theta = random(TWO_PI);
    xDir = cos(theta);
    yDir = sin(theta);
  }

  public String toString() {
    return "Actor: " + name;
  }

  float hp() {
    return hpCurrent / float(hpMax);
  }

  void show() {
    fill(color(242, 60, 47));
    ellipse(x, y, radius, radius);
    fill(fill);
    ellipse(x, y, radius - 10, radius - 10);

    fill(0, 0, 0);
    text(nf(hpCurrent, 3, 0), x - 1, y + 1);
    fill(255, 255, 255);
    text(nf(hpCurrent, 3, 0), x, y);
  }

  void collide(Actor[] actors) {
  }

  void move() {
    move(xSpeed, ySpeed);
  }

  // Since this version of move is specific to just the actor
  // class, and we don't want children or sub classes to
  // override it, we add the final keyword.
  
  final void move(float xSpeed, float ySpeed) {
    x += xSpeed * xDir;
    y += ySpeed * yDir;

    if (x >= width - radius || x <= radius) {
      xDir = -xDir;
    }

    if (y >= height - radius || y <= radius) {
      yDir = -yDir;
    }
  }
}