// When you type in size(420, 420); you are
// calling a function, then providing arguments
// to give it the information it needs to do work.

// What if you want to make your own function?
// you need to define it. You've actually been
// calling and defining functions the entire time,
// even before knowing these terms.

void setup() {
  surface.setResizable(true);
  surface.setTitle("Functions");
  pixelDensity(displayDensity());
  size(420, 420);
  background(32);
}

void draw() {
  myFirstFunction();
  parameterizedFunction(
    (mouseY + 120) % width, 
    (mouseX + 120) % height, 
    width / 6.0, 
    height / 6.0);
}

// This is a definition for a function we make.
// void means that it returns no information back
// to the caller when it's finished doing its job.

void myFirstFunction() {

  // pushStyle() and popStyle() are two functions
  // that are grouped together. they isolate changes
  // to fill, stroke and strokeWeight which take
  // place between the push and pop, preventing style
  // commands from bleeding over into other shapes.

  pushStyle();
  noStroke();
  if (mousePressed) {
    fill(127, map(mouseX, 0, width, 0, 255), 
      map(mouseY, 0, height, 0, 255), 204);
    ellipse(mouseX, mouseY, width / 4.0, height / 4.0);
    fill(map(mouseX, 0, width, 0, 255), 
      map(mouseY, 0, height, 0, 255), 
      127, 204);
    ellipse(mouseX, mouseY, width / 5.0, height / 5.0);
  } else {
    fill(32, 32, 32, 8);
    rect(0, 0, width, height);
  }
  popStyle();
}

// Parameters are the slots into which arguments are
// placed when they function is called. Parameters
// are for vital information needed to do the task.

// Parameters go between the parentheses of the function
// and are separated by commas. This comma separated
// list is sometimes called the function's Signature.

// The variables x, y, w and h exist only between the
// curly braces which serve to define parameterizedFunction.
// The outside program doesn't know about them and may
// even have other variables named x, y, etc.

void parameterizedFunction(float x, float y, float w, float h) {
  if (!mousePressed) {
    pushStyle();
    noFill();
    strokeWeight(1.5);
    colorMode(HSB, 360, 100, 100, 50);
    stroke(random(0, 120), 50, 100);
    triangle(x, y - h / 2.0, 
      x + w / 2.0, y + h / 2.0, 
      x - w / 2.0, y + h / 2.0);
    popStyle();
  }
}