// An array is an efficient way of storing variables of
// the same data type together. It is declared by
// placing square brackets [] after the data type key word.
// Arrays can be initialized by using curly braces { } with
// a list of values in-between or by using the new keyword
// followed by the datatype with the size of the array
// in square brackets.
float[] floatArray = new float[7];

// Each element in an array has an index through which it
// can be accessed. Arrays begin counting these indices at
// 0 and count up to the length of the array - 1. This array's
// length is 5, so its elements are counted from 0 to 4.
int[] intArray = { 17, 30, 25, 10, 13 };
//                  0   1   2   3   4

boolean[] booleanArray = new boolean[4];
char[] charArray = { 'a', 'b', 'c', 'z', 'w', 'x', 'v' };
String[] stringArray = { "Aardvark", "Beaver", "Cat", "Yak", "Zebra" };

void setup() {
  surface.setResizable(true);
  surface.setTitle("Arrays");
  pixelDensity(displayDensity());
  size(420, 420);
  background(32);
  noStroke();
  rectMode(CORNERS);

  // An individual element of an array can be accessed by
  // placing the element's index in square brackets at the
  // end of the array variable name. The 2nd element should be 25.
  println("intArray value of element 2 = " + intArray[2]);

  // If you accidentally request an element index that is
  // less than 0 or greater than or equal to the array's
  // length you'll get an ArrayIndexOutOfBoundsException
  // when you press run.
  // println(intArray[-1]);
  // println(intArray[7]);

  // You can find out the length or size of an array by
  // accessing the 'length' member variable (more on what
  // this is later).
  int size = floatArray.length;
  
  // You can use a for-loop to quickly run through and
  // access all the elements of an array.
  for (int i = 0; i < size; ++i) {
    // This will add a random number to the array, and then
    // draw a red rectangle to represent that number.
    floatArray[i] = random(0, height);
    float x = map(i, 0, size, 0, width);
    fill(255, 0, 0, 204);
    rect(x, height-floatArray[i], x + 30, height, 5, 5, 0, 0);
  }
  println("\r\nrandom decimal numbers: ");
  printArray(floatArray);

  // What if you want to randomly choose an element from an
  // array? The random() function works in terms of floating
  // point decimals but the index of an array is an int, so
  // we need to convert from float to int.
  int choice = int(random(0, stringArray.length));
  String chosenString = stringArray[choice];
  println("\r\nrandom element: " + choice + " " + chosenString);

  // What if you want to swap the elements in an array? You
  // will need a temporary container.
  char temp = charArray[2];
  charArray[2] = charArray[0];
  charArray[0] = temp;
  println("\r\nswapping elements:");
  printArray(charArray);

  // What if you want to sort an array? the simplest, but not the best
  // way to sort an array is called a bubble sort.
  size = floatArray.length;
  for (int i = 0; i < size; ++i) {
    for (int j = 0; j < size; ++j) {
      if (floatArray[i] < floatArray[j]) {
        // We create a variable to temporarily hold a value. this is
        // so we can swap two elements of an array around.
        float t = floatArray[i];
        floatArray[i] = floatArray[j];
        floatArray[j] = t;
      }
    }
  }
  println("\r\nsorted floatArray:");
  printArray(floatArray);

  // As you can see in the window, working with arrays is helpful
  // for people who want to visualize statistics.
  for (int i = 0; i < size; ++i) {
    // This will draw a yellow bar for the array after it has been sorted.
    float x = map(i, 0, size, 0, width);
    fill(255, 255, 0, 255);
    rect(x, height - floatArray[i], x + 10, height, 5, 5, 0, 0);
  }
}