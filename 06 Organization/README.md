# Assignment 6
## Organization
### 100 points

![Organization](preview.png)

### References

* Daniel Shiffman, [_Learning Processing_]((http://learningprocessing.com/examples/)) Chapters 7 - 9
* Shiffman, [Video: Functions](https://youtu.be/XCu7JSkgl04?list=PLRqwX-V7Uu6ajGB2OI3hl5DZsD1Fw1WzR)
* Shiffman, [Video: Object-Oriented Programming](https://youtu.be/YcbcfkLzgvs?list=PLRqwX-V7Uu6bb7z2IJaTlzwzIg_5yvL4i)
* Shiffman, [Video: Arrays](https://youtu.be/NptnmWvkbTw?list=PLRqwX-V7Uu6bO9RKxHObluh-aPgrrvb4a)
* [Tutorial: Objects](https://processing.org/tutorials/objects/)

### Instructions

1. Adhere to the theme to be determined in lab. (10 pts.)
    * Wednesday: Weather
    * Monday: Screensavers

2. Define and call three of your own functions. (25 pts.)
    * A function definition begins with the type of information the function will `return` (or output) when it's done.
        * `void myFunctionName ( ) { }`
        * `int sureThing ( ) { return -1; }`
        * `boolean trueOrFalse ( ) { return false; }`
    * Next is the function's name.
        * `void eyeball ( ) { }`
        * `void socket ( ) { }`
        * `void foo ( ) { }`
    * The next is the function's signature, a comma-separated list of parameters between parentheses. Parameters are the information the function needs as inputs in order to do its job. Each parameter consists of the data type and name of the parameter.
        * `void eyeball (float centerX, float centerY, color iris) { }`
        * `float average (float a, float b, float c) { return (a + b + c) / 3.0; }`
    * The last is the function definition's body in-between curly braces, the list of instructions to be executed when the function is called. If you promised that the definition would output information, be sure to include a `return` statement followed by information of that type.
        * `void drawNose () { ellipse(30, 40, 20, 50); ellipse(30, 65, 35, 35); }`
        * `PVector newLocation() { return new PVector(width / 2.0, height / 2.0, 0); }`

3. Define and construct an instance of one class. (25 pts.)
    * Create a new tab by clicking on the white arrow underneath the Run and Stop buttons.
    * Give the tab the same name as the class you intend to create. Enter `class ClassName { }`
    * Define a constructor for the class in the new tab. `class ClassName { ClassName() { } }`
    * In the main tab above setup, declare a variable of the class's data type. `ClassName myInstanceVar;`
    * Initialize the variable using the class's constructor. `void setup() { myInstanceVar = new ClassName(); }`
    * Access the instance's (object's) variables and methods using dot syntax.
        * `myInstanceVar.radius = 50;`
        * `myInstanceVar.pingPong();`
    * The first tab that opens when you create a new Processing sketch is your default, main sketch .pde. __This is the only tab and the only .pde file that can contain void setup and void draw at global scope.__

4. Ensure that the code contains at least 4 single- or multi-line comments, written in complete sentences, that explain what the code is doing at key steps. (20 pts.)

5. Ensure that the sketch runs without errors. The main tab of the sketch should be the only tab with global-scope setup and draw functions. (10 pts.)

6. Ensure that the main tab of the sketch shares the same name as the sketch folder. Name the sketch folder and main tab according to the following convention: s17_magd150_lab06_yourlastname. Compress the sketch folder into a .zip file and upload to D2L. (10 pts.)

__Any code or media within a submission are presumed to be the work of the student(s) unless otherwise stated. When media or code from outside sources are used, a link to the source and notice of attribution at the top of the main sketch in a comment should be provided. Media and code used are expected to be from the intellectual commons (i.e., not copyrighted or registered as a trademark) and to be altered substantially by the student(s). On first incident, students whose work does not meet this standard will have one week to resubmit the assignment for half credit. On second incident, students will be referred to Student Conduct Programs and will fail the course.__
