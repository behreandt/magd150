float x, y, r;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Return");
  pixelDensity(displayDensity());
  size(420, 420);
  background(32);

  // These functions not only take information, they return info
  // of a certain type, which can then be assigned to and stored
  // in a variable.
  int a = sum(1, 2);
  float b = sum(1.0, 2.0);
  float c = sum(4, 5, 7, 9);
  float d = avg(4, 5, 7, 9);
  float e = avg();
  println("a: " + a);
  println("b: " + b);
  println("c: " + c);
  println("d: " + d);
  println("e: " + e);

  noStroke();
  ellipseMode(RADIUS);
  r = random(min(width, height) / 15.0, min(width, height) / 2.0);
  x = random(r, width - r);
  y = random(r, height - r);
}

void draw() {
  background(32);
  // intersection is a custom function which can take any (x, y)
  // coordinate and returns true or false based on the circle
  // intersection formula.
  if (intersection(mouseX, mouseY)) {
    fill(255, 127, 0);
  } else {
    fill(0, 127, 255);
  }
  ellipse(x, y, r, r);
}

// The first keyword specifies the datatype of information
// that the function returns.
// Those variables, a and b in this case, have scope, or exist
// in the space between those curly braces.
int sum(int a, int b) {
  println("sum(int a, int b) called");
  return a + b;
}

// You can have two functions with the same name
// so long as they take different arguments (different
// data types and/or a different number of arguments).
float sum(float a, float b) {
  println("sum(float a, float b) called");
  return a + b;
}

// Sometimes it's useful to write a function which
// takes an unknown amount of arguments. This is called
// using 'varargs'. the ... means that values is an
// array passed into the function.
float sum(float... values) {
  println("sum(float... values) called");
  float s = 0.0;
  for (int i = 0; i < values.length; ++i) {
    s += values[i];
  }
  return s;
}

// With varargs, be sure to account for the possibility
// that the array contains no elements.
float avg(float... values) {
  println("avg(float... values) called");
  int size = values.length;
  if (size > 0) {
    float s = 0.0;
    for (int i = 0; i < size; ++i) {
      s += values[i];
    }
    return s / size;
  } else {
    return 0;
  }
}

// You can create a function to make intersection functions.
boolean intersection(float px, float py) {
  return dist(x, y, px, py) < r;
}