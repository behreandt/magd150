# Extra Credit
## Maximum of 100 points
## Deadline: 12:30 Friday, May 5, 2017

### Possibilities

* Respond to material from the lecture section of class.

* Attend and respond to an event significant to new media art practice or video game development. This might include an art exhibit or talk, theatrical or musical performance, video game tournament or con.

* For either of the above, the response could be emphasize creativity or analysis or both.
    * Analytical responses should relate to concepts from the lecture portion of the class (e.g., diachronic vs. synchronic approaches to historical media, Roger Caillois' four categories of game). Concision and clarity of thought are more important than page count.
        * Cite any sources used.
    * Creative response might include the below. Submit both a working file (.ai or .psd) and a fixed file (.svg, .png, .pdf). Working files should include color swatches; layers should be organized and named.
        * Make a vector image using software such as Adobe Illustrator, Inkscape, Figma or other software.
        * Make a raster image using software such as Adobe Photoshop, GIMP, Aseprite or other software.

* Create a Processing sketch. Some possibilities include:
    * Use a library, for example, [sound](https://processing.org/reference/libraries/sound) or [video](https://processing.org/reference/libraries/video), to make something you couldn't with the Processing core alone.
    * Follow a coding challenge on Dan Shiffman's [Youtube channel](https://www.youtube.com/user/shiffman).
        * Note in a comment at the top of the sketch which video you are referencing. Include the URL.
        * Alter the code in a substantive way, both in terms of behavior and of aesthetics.
        * Provide your own comments in the code demonstrating your understanding of how it works. Comments should be written in complete sentences.
    * Migrate a Processing sketch you've made for an assignment to a JavaScript foundation using [p5.js](https://p5js.org).

### Logistics

* Name the submission according to the following convention: s17_magd150_extraCredit_yourlastname.ext, where '.ext' is the file extension, depending on the format.
    * If necessary, compress the submission into a .zip file and upload to D2L.
    * If submitting a Processing sketch, remember that all additional code and media files need to be included in the folder which is to be compressed into a zip file.

__Any code or media within a submission are presumed to be the work of the student(s) unless otherwise stated. When media or code from outside sources are used, a link to the source and notice of attribution at the top of the main sketch in a comment should be provided. Media and code used are expected to be from the intellectual commons (i.e., not copyrighted or registered as a trademark) and to be altered substantially by the student(s). Work which falls into this category will not be accepted for extra credit.__
