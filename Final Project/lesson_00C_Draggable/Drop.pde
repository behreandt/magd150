class Drop {
  float x, y, w, h, 
    cornerRounding = 5, 
    assistRate = 0.025, threshold = 50;

  Drop(Draggable d) {
    h = d.h * 1.02;
    w = d.w * 1.02;
    x = random(w, width - w);
    y = random(h, height - h);
  }

  void draw() {
    noFill();
    strokeWeight(1.25);
    stroke(0x88ffffff);
    rect(x, y, w * 2, h * 2, cornerRounding);
  }
  
  void placeAssist(Draggable d) {
    if (!d.dragging && dist(x, y, d.x, d.y) < threshold) {
      d.x = lerp(d.x, x, assistRate);
      d.y = lerp(d.y, y, assistRate);
    }
  }
}