class Draggable {
  float x, y, w, h, 
    offsetX, offsetY, 
    colorTransTime = 0.025;
  int defaultFill, 
    hoverFill, 
    activeFill, 
    fill;
  boolean dragging;
  PImage image;

  // If a draggable object is created with a string argument,
  // then load the image and call the fully developed
  // constructor below.
  Draggable(String img) {
    this(loadImage(img));
  }

  Draggable(PImage img) {
    // Size the draggable according to the
    // screen's shorter edge, place it randomly.
    w = h = min(width, height) * 0.15;
    x = random(w, width - w);
    y = random(h, height - h);

    // Construct a color from hue, saturation
    // and brightness.
    float hue = random(0, 300);
    float sat = random(75, 100);
    float bri = random(75, 100);
    
    // The draggable item will change color
    // based on whether the mouse is hovering
    // over it or dragging it.
    defaultFill = color(hue, sat, bri);
    hoverFill = color(hue + 30, sat, bri);
    activeFill = color(hue + 60, sat, bri);
    fill = defaultFill;
    image = img;
  }

  void draw() {
    // If the mouse is hovering over the item...
    if (intersection(mouseX, mouseY)) {
      // If the mouse is dragging the item...
      if (dragging) {
        // Fade the color to a brighter color.
        fill = lerpColor(fill, activeFill, colorTransTime);
      } else {
        // Fade the color to a bright color.
        fill = lerpColor(fill, hoverFill, colorTransTime);
      }
    } else {
      // Return the color to its usual state.
      fill = lerpColor(fill, defaultFill, colorTransTime);
    }

    // If the object is being dragged, then set it to the
    // mouse's position minus the offset of the mouse from
    // the center/pivot of the object.
    if (dragging
      && mouseX > 0 && mouseX < width
      && mouseY > 0 && mouseY < height) {
      x = mouseX - offsetX;
      y = mouseY - offsetY;
    }

    // Tint the image by the fill color.
    tint(fill);
    // Display the image.
    image(image, x, y, w * 2, h * 2);
  }

  // Test a coordinate against the left, right,
  // top and bottom edge.
  boolean intersection(float px, float py) {
    return px > x - w
      && px < x + w
      && py > y - h
      && py < y + h;
  }

  // If the mouse is pressed and the mouse is over
  // the object, then set dragging to true.
  void mousePressed() {
    if (intersection(mouseX, mouseY)) {
      offsetX = mouseX - x;
      offsetY = mouseY - y;
      dragging = true;
    }
  }

  void mouseReleased() {
    dragging = false;
  }
}