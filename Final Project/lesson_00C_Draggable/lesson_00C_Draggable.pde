Draggable drag;
Drop drop;

void setup() {
  surface.setTitle("Advanced Draggable Items.");
  pixelDensity(displayDensity());
  size(630, 420);
  colorMode(HSB, 359, 99, 99);
  imageMode(CENTER);
  rectMode(CENTER);
  drag = new Draggable("back.png");
  drop = new Drop(drag);
}

void draw() {
  background(#202020);
  drop.placeAssist(drag);
  drop.draw();
  drag.draw();
}

void mousePressed() {
  drag.mousePressed();
}

void mouseReleased() {
  drag.mouseReleased();
}