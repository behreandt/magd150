/* SOURCE & REFERENCE:
 * https://forum.processing.org/two/discussion/16227/
 * how-to-export-to-svg-using-library-processing-svg
 */

// Step 1. Import processing.svg library.
import processing.svg.*;

// Step 2. Declare a graphics object from which the
// .svg will be recorded.
PGraphics drawing;

// Step 3. Decide on a file name.
String fileName = "curves.svg";

// Number of curves to draw. Hue
// and its offset when creating
// a gradient.
int count = 20, hue, hOff = 30;

// Start and stop color of the curves.
color start, stop;

// Changes the extremity of the curves.
float xDamping, yDamping;

// Thickness of the curves.
PVector off = new PVector(-20, 20);

void setup() {
  surface.setTitle("Exporting SVG From Processing");
  size(512, 512);
  colorMode(HSB, 359, 99, 99);

  int h = (int)random(360);
  start = color(h, 99, 99);
  stop = color((h + hOff) % 360, 59, 89);
  saveSVG(fileName, 512, 512, count, start, stop, off, 
    random(2.0), random(2.0));
}

void draw() {
  background(#202020);
  noStroke();

  if (frameCount % 3 == 0) { 
    hue = (hue + 1) % 360;
  }
  start = color(hue, 99, 99);
  stop = color((hue + hOff) % 360, 59, 89);
  xDamping = map(mouseX, 0, width, 0.1, 2.0);
  yDamping = map(mouseY, 0, height, 0.1, 2.0);

  for (int i = 0; i < count; ++i) {
    float n = i / (float)count;
    fill(lerpColor(start, stop, n));
    beginShape();
    vertex(0, 0);
    bezierVertex(width * xDamping * n, 
      height * yDamping * (1 - n), 
      width * yDamping * (1 - n), 
      height * xDamping * n, 
      width, height);
    vertex(off.x + width, off.y + height);
    bezierVertex(off.x + width * yDamping * (1 - n), 
      off.y + height * xDamping * n, 
      off.x + width * xDamping * n, 
      off.y + height * yDamping * (1 - n), 
      off.x, off.y);
    vertex(off.x, off.y);
    endShape(CLOSE);
  }
}

void mousePressed() {
  saveSVG(fileName, 512, 512, 
    20, #0088ff, #ffff00, new PVector(-20, 20), 
    xDamping, yDamping);
}

void saveSVG(String fileName, int w, int h, 
  int count, color start, color stop, PVector off, 
  float xDamping, float yDamping) {

  // Step 4. Create graphics with the .svg file as the target.
  // A PGraphics object is like a microcosm of Processing. You
  // use the dot-syntax to recreate everything that will go on
  // within that shape.
  PGraphics drawing = createGraphics(w, h, SVG, fileName); 

  // Step 5. Begin drawing.
  drawing.beginDraw();

  // Step 6. Issue commands to draw just like in draw above.
  drawing.noStroke();
  drawing.colorMode(HSB, 359, 99, 99);

  // For each curve...
  float n = 0;
  for (int i = 0; i < count; ++i) {

    // Normalized value of each curve from 0 to 1 (0% to 100%).
    // Later, 1 - n will be used to stand for the complementary
    // value; this is so the control points of the bezier vertices
    // will change proportionally as we go through the for-loop.
    n = i / (float)count;

    // Use lerpColor to create a gradient from start to stop.
    drawing.fill(lerpColor(start, stop, n));
    drawing.beginShape();
    drawing.vertex(0, 0);
    drawing.bezierVertex(w * xDamping * n, 
      h * yDamping * (1 - n), 
      w * yDamping * (1 - n), 
      h * xDamping * n, 
      w, h);

    // Anchor Point 1.
    drawing.vertex(off.x + w, 
      off.y + h);

    // Control Point 1, Control Point 2, Anchor Point 2.
    drawing.bezierVertex(off.x + w * yDamping * (1 - n), 
      off.y + h * xDamping * n, 
      off.x + w * xDamping * n, 
      off.y + h * yDamping * (1 - n), 
      off.x, off.y);
    drawing.vertex(off.x, off.y);
    drawing.endShape(CLOSE);
  }

  // Step 7. Conclude the drawing.
  drawing.dispose();
  drawing.endDraw();
}