# Final Project

* Design Document: 100 points
* Project: 200 points
    * File: 125 points
    * Presentation: 75 points

### References

* Daniel Shiffman, [_Learning Processing_](http://learningprocessing.com/examples/)
* Shiffman, [The Coding Train](https://www.youtube.com/user/shiffman/videos)
* [Processing Exhibition](https://processing.org/exhibition/)
* [OpenProcessing](https://www.openprocessing.org/browse/?viewBy=most&filter=favorited#)
* [Generative Design](http://www.generative-gestaltung.de/code)
* [Form+Code](http://formandcode.com/code-examples/)
* Libraries
    * [Sound](https://processing.org/reference/libraries/sound/index.html)
    * [Minim](http://code.compartmental.net/tools/minim/)
    * [Game Control Plus](http://lagers.org.uk/gamecontrol/)

### Instructions

1. Work either individually or in groups of up to 4 people.

2. Author a project design document. This will be due 1 week after work on the final project commences. (100 pts.)
    1. Give the project a name. Include the project name and group member names at the top of the document. (10 pts.)
    2. List 7 ideas for a project that the group has brainstormed. (10 pts.)
    3. Write a 1- to 2- paragraph [executive summary](https://en.wikipedia.org/wiki/Executive_summary) of the project. (10 pts.)
    4. Write an ordered list of at least 7 features in ascending order from most essential, 1, to least essential, 7. (20 pts.)
        * Emphasize concreteness and specificity over abstraction and generality.
    5. For each group member, specify the role he or she will play in the group. (20 pts.)
        * List 3 skills that the group member already possesses which qualify him or her to play the above role.
        * List 2 skills that the group member knows he or she will have to research, acquire and/or improve during the course of development.
    6. Include at least 1 visual with the document (e.g., concept art, diagram). (10 pts.)
    7. Write in complete sentences using standard English grammar, with emphasis on specificity and concision over generalities. (10 pts.)
    8. Name the document according to the following convention: s17_magd150_projectName_lastNames . Format the document as a .rtf, .doc or .pdf and submit to the D2L dropbox. (10 pts.)

3. Submit the project to D2L by the deadline. (125 pts.)
    1. The project consolidates the 10 lessons practiced through the previous labs. It demonstrates sustained aesthetic intent and organization of code. (50 pts.)
    2. The project has implemented at least four key features outlined in the design document. (25 pts.)
    3. The project demonstrates that its group members have acquired the skills they have set out to learn. (25 pts.)
    4. The project has been [exported](https://github.com/processing/processing/wiki/Export-Info-and-Tips) as a standalone application for both Windows and Mac (File > Export Application). This folder, which contains both the standalone and the source code, has been compressed into a .zip file and uploaded to D2L. The project is named according to the following convention s17_magd150_projectName_lastNames. (15 pts.)
    5. The project runs without errors. (10 pts.)

4. Present the project to the class during the final lecture period of the semester. (75 pts.)
    * The amount of time per presentation will be the total time of a lecture divided by the number of groups in both labs.
        *   75 minutes / (14 + 15) = 2 min. 30 sec.

__Any code or media within a submission are presumed to be the work of the student(s) unless otherwise stated. When media or code from outside sources are used, a link to the source and notice of attribution at the top of the main sketch in a comment should be provided. Media and code used are expected to be from the intellectual commons (i.e., not copyrighted or registered as a trademark) and to be altered substantially by the student(s). For this assignment there will be no time remaining to resubmit should students fail to meet this standard.__
