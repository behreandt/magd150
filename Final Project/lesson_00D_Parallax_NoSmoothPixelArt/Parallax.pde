class Parallax {
  
  // Number of layers.
  private int size;
  
  // Lower and upper bound which will contain each
  // hill layer on the Z-axis.
  private float minDepth, 
    maxDepth = -1000;
  private PImage[] layers;
  private PVector[] centers;
  private PVector[] scales;

  Parallax(String pathBase, String ext, int max) {
    size = max;
    layers = new PImage[size];
    centers = new PVector[size];
    scales = new PVector[size];
    float aspect, shortEdge, longEdge, w, h, cx, cy;
    for (int i = 0; i < size; ++i) {

      // Load the image.
      layers[i] = loadImage(pathBase + i + "." + ext);

      // Determine the scale of the image by finding the
      // short edge.
      w = layers[i].width;
      h = layers[i].height;
      shortEdge = min(w, h);
      longEdge = max(w, h);
      
      // Calculate an aspect ratio by which to up-scale the
      // display of the original image while maintaining
      // its proportions.
      aspect = longEdge / shortEdge;
      scales[i] = new PVector(width * aspect, height);

      // Determine the position of the image.
      cx = width * 0.5;
      cy = height - h * 0.5;
      
      // Translate the hill's order in the layers to a
      // depth along the Z-axis using the map function.
      centers[i] = new PVector(cx, cy, 
        map(i, 0, size, maxDepth, minDepth));
    }
  }

  void draw() {
    for (int i = 0; i < size; ++i) {
      pushMatrix();
      translate(centers[i].x, centers[i].y, centers[i].z);
      image(layers[i], 0, 0, scales[i].x, scales[i].y);
      popMatrix();
    }
  }
}