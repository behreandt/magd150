class StateMachine<E extends State> {
  public E current;
  public E previous;

  private HashMap<String, E> states =
    new HashMap<String, E>();

  StateMachine(E... sts) {
    int size = sts.length;
    for (int i = 0; i < size; ++i) {
      sts[i].machine = this;
      states.put(sts[i].toString(), sts[i]);
    }
    if (size > 0) {
      set(sts[0]);
    }
  }

  public String toString() {
    return states.values().toString();
  }

  public void add(E s) {
    s.machine = this;
    states.put(s.toString(), s);
  }

  public E remove(String request) {
    E removed = states.remove(request);
    if (removed != null) {
      removed.machine = null;
    }
    return removed;
  }

  public E get(String request) {
    return states.get(request);
  }

  public void set(String request) {
    E s = states.get(request);
    if (s != null) {
      previous = current;
      current = s;
      if (previous != null) {
        previous.exit();
      }
      current.enter();
    } else {
      System.err.println("The state " + request
        + " could not be found.");
    }
  }

  public void set(E s) {
    previous = current;
    current = s;
    if (previous != null) {
      previous.exit();
    }
    current.enter();
  }
}