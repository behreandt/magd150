class Actor extends Sprite {
  KeyListener kl;

  int facing = 1, 
    walkLeft = 65, 
    walkRight = 68;

  float walkSpeed = 2.5;

  Actor(KeyListener _kl, float _x, float _y, 
    float _w, float _h, String... paths) {

    // Call the actor's parent constructor, Sprite.
    super(new PVector(_x, _y), new PVector(_w, _h), 
      loadImages(paths));
      
    // Assign the actor's key listener.
    kl = _kl;
    
    // Add the actor's actions to the key listener.
    kl.add(walkLeft);
    kl.add(walkRight);
  }

  void update() {
    if (kl.get(walkRight)) {
      facing = 1;
      animator.set("WalkRight");
      pos.x += walkSpeed;
    } else if (kl.get(walkLeft)) {
      facing = -1;
      animator.set("WalkLeft");
      pos.x -= walkSpeed;
    } else {

      // If the actor is facing left (-1), set
      // the idle animation to the left. Otherwise
      // use the default.
      if (facing == -1) {
        animator.set("IdleLeft");
      } else {
        animator.set("Default");
      }
    }
  }
}