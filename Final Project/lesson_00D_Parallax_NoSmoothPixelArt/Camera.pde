class Camera {
  PVector pos, 
    lookAt, 
    up = new PVector(0, 1, 0),
    offset;
  float fov = PI / 3.0, 
    aspect, 
    panSpeed = 0.025, 
    zoomSpeed = 0.0125, 
    defaultZPos, 
    overviewZPos, 
    inspectZPos, 
    nearClip, 
    farClip, 
    freeCamPan = 4;
  int panUp = UP, 
    panLeft = LEFT, 
    panDown = DOWN, 
    panRight = RIGHT, 
    zoomOutKey = 46, 
    zoomInKey = 47;
  boolean isOrthographic;
  KeyListener kl;

  Camera() {
    this(null, 
      new PVector(width * 0.5, height * 0.5, height / 1.155), 
      new PVector(width * 0.5, height * 0.5, 0),
      new PVector(0, -height * 0.4125));
  }

  Camera(KeyListener _kl) {
    this(_kl, 
      new PVector(width * 0.5, height * 0.5, height / 1.155), 
      new PVector(width * 0.5, height * 0.5, 0),
      new PVector(0, -height * 0.4125));
  }

  Camera(KeyListener _kl, PVector _pos, PVector _lookAt, PVector _offset) {
    pos = _pos;
    lookAt = _lookAt;
    offset = _offset;
    defaultZPos = pos.z;
    overviewZPos = defaultZPos * 7.5;
    inspectZPos = defaultZPos * 0.133;
    aspect = width / float(height);
    nearClip = pos.z / 10.0;
    farClip = pos.z * 10.0;
    kl = _kl;
    if (kl != null) { 
      kl.add(panUp, panLeft, panDown, panRight, zoomOutKey);
    }
  }

  public String toString() {
    return "position: " + pos.toString()
      + "\nlooking at: " + lookAt.toString();
  }

  void draw() {
    if (kl != null) { 
      zoom();
    }
    if (isOrthographic) {
      ortho();
    } else {
      perspective(fov, 
        aspect, 
        nearClip, 
        farClip);
    }
    camera(pos.x, pos.y, pos.z, 
      lookAt.x, lookAt.y, lookAt.z, 
      up.x, up.y, up.z);
  }

  void pan(float x, float y) {
    pan(new PVector(x, y));
  }

  void pan(PVector target) {
    if (kl != null
      && (kl.get(panUp) || kl.get(panDown)
      || kl.get(panLeft) || kl.get(panRight))) {
      if (kl.get(panUp)) {
        pos.y -= this.freeCamPan;
      }

      if (kl.get(panLeft)) {
        pos.x -= this.freeCamPan;
      }

      if (kl.get(panDown)) {
        pos.y += this.freeCamPan;
      }
      if (kl.get(panRight)) {
        pos.x += this.freeCamPan;
      }
    } else {
      pos.x = lerp(pos.x, target.x + offset.x, panSpeed);
      pos.y = lerp(pos.y , target.y + offset.y, panSpeed);
    }
    lookAt.x = pos.x;
    lookAt.y = pos.y;
  }

  void zoom() {
    if (kl.get(zoomOutKey)) {
      pos.z = lerp(pos.z, overviewZPos, zoomSpeed);
    } else if (kl.get(zoomInKey)) {
      pos.z = lerp(pos.z, inspectZPos, zoomSpeed);
    } else {
      pos.z = lerp(pos.z, defaultZPos, zoomSpeed);
    }
  }
}