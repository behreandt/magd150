class Animation extends State {
  public String name = "Animation";
  private PImage[] frames;
  private int size = 0, 
    osc = 1;
  public int current = 0, 
    interval = 12, 
    pivot = CENTER;
  public PVector offset = new PVector(), 
    scl = new PVector(64, 64);
  public boolean oscillate = true;

  Animation(String _name, int _interval, 
    PVector _offset, PVector _scl, 
    String... _frames) {
    this(_name, _interval, 
      _offset, _scl, 
      loadImages(_frames));
  }

  Animation(String _name, int _interval, 
    PVector _offset, PVector _scl, 
    PImage... _frames) {
    name = _name;
    interval = _interval <= 0 ? 1 : _interval;
    offset = _offset;
    scl = _scl;
    frames = _frames;
    size = frames.length;
  }

  public String toString() {
    return name;
  }

  void enter() {
  }

  void draw() {
    advance();
    image(frames[current], 
      offset.x, offset.y, 
      scl.x, scl.y);
  }

  void exit() {
  }

  void advance() {
    if (frameCount % interval == 0) {
      if (!oscillate) {
        current = (current + 1) % size;
      } else {
        current += osc;
        if (current == 0
          || current >= size - 1) {
          osc *= -1;
        }
      }
    }
  }
}

// On one hand, it is cumbersome to try to load multiple frames at
// once with loadImage. On the other, a lot can go wrong, such as
// a typo in the file name or the file not being in the data folder.
PImage[] loadImages(String... paths) {
  int size = paths.length;
  
  // If no file names were specified, don't bother.
  if (size <= 0) {
    return null;
  }
  
  // Create an expandable list.
  java.util.List<PImage> result = new java.util.ArrayList<PImage>();
  PImage attempt;
  for (int i = 0; i < size; ++i) {
    
    // Attempt to load the file.
    attempt = loadImage(paths[i]);
    
    // If the file is not null, add to the list.
    if (attempt != null) { 
      result.add(attempt);
    }
  }
  
  // Convert the expandable list to a fixed-size array.
  return result.toArray(new PImage[result.size()]);
}