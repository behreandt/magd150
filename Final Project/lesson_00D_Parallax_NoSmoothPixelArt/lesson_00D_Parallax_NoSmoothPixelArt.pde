/* The P3D renderer is used to allow the camera to pan and zoom, and to
 * sort the depth of each background layer. However, that means we can't
 * use noSmooth(); to get the crisp look of pixel art with up-scaled
 * images. The workaround below comes from the following references:
 * https://github.com/processing/processing/wiki/Advanced-OpenGL
 * https://forum.processing.org/one/topic/
 * p3d-disable-texture-smoothing-antialiasing-when-upscaling.html
 */

KeyListener kl;
Camera cam;
Actor hero;
Parallax hills;

void setup() {

  int hillCount = 8;

  // Generate the background hills with Perlin noise. The hills are
  // saved as .png files at the size specified then imported.
  createHills(256, /* Image width. */
    64, /* Image height. */
    hillCount, /* Number of hills to make */
    #0b263f, /* Starting color of rear-most hill. */
    #b8cbdd, /* Ending color for front-most hill. */
    8, /* Noise detail; should be a power of two, e.g., 2, 4, 8. */
    0.6); /* Noise falloff. */

  // Create the parallax object which will manage the images.
  // Since the images are ordered sequentially - "hills0.png",
  // "hills1.png", etc. - the parallax object constructor can
  // loop through and grab all of them from the data folder.
  hills = new Parallax("hills", "png", hillCount);

  pixelDensity(displayDensity());
  surface.setTitle("Parallax");
  size(1024, 512, P3D);
  imageMode(CENTER);

  // Workaround for noSmooth. See note above.
  hint(DISABLE_TEXTURE_MIPMAPS);
  PGraphicsOpenGL _g = (PGraphicsOpenGL)g;
  _g.textureSampling(3);

  kl = new KeyListener();
  cam = new Camera(kl);

  hero = new Actor(kl, /* Controls for the actor */
    width * 0.5, /* Horizontal position */
    height * 0.95, /* Vertical position */
    64, /* Width */
    64, /* Height */
    "idleright0.png", "idleright1.png", 
    "idleright2.png", "idleright3.png");

  // Add extra animation states to the actor. Each receives a name,
  // a frame delay, and a list of the file names of images to load
  // in as frames of animation.
  hero.addAnimation("IdleLeft", 12, 
    "idleleft0.png", "idleleft1.png", 
    "idleleft2.png", "idleleft3.png");
  hero.addAnimation("WalkLeft", 8, 
    "walkleft0.png", "walkleft1.png", 
    "walkleft2.png", "walkleft4.png", 
    "walkleft5.png", "walkleft6.png");
  hero.addAnimation("WalkRight", 8, 
    "walkright0.png", "walkright1.png", 
    "walkright2.png", "walkright4.png", 
    "walkright5.png", "walkright6.png");
}

void draw() {
  background(#f2f6f9);

  // Draw the background.
  hills.draw();

  // Follow the hero.
  cam.pan(hero.pos);

  // Activate camera controls like pan and zoom.
  cam.draw();

  // Change animation states according to key inputs.
  hero.update();

  // Draw the hero.
  hero.draw();
}

void keyPressed() {
  kl.keyPressed(keyCode);
}

void keyReleased() {
  kl.keyReleased(keyCode);
}

void createHills(int w, int h, int itr, color start, color stop, 
  int ndetail, float falloff) {

  // Create a miniature world in which to draw.
  PGraphics hillGen;

  for (int i = 0; i < itr; ++i) {

    // Provide a seed to the noise function.
    noiseSeed((long)random(0, 10000));

    // The greater the detail (2, 4, 8) and falloff (0 to 1),
    // the craggier the resulting hills will be.
    noiseDetail(ndetail, falloff);

    // Size the miniature.
    hillGen = createGraphics(w, h);

    // Ensures the resulting images will have a crisp,
    // pixel art feel.
    hillGen.noSmooth();

    // Equivalent to void draw() {}
    hillGen.beginDraw();
    hillGen.noStroke();
    hillGen.fill(lerpColor(start, stop, i / (float)itr));
    hillGen.beginShape();

    // Set an anchor in the bottom-left corner. The shape
    // will then be drawn clockwise until it reaches the
    // bottom-right corner.
    hillGen.vertex(0, hillGen.height);
    PVector[] p = new PVector[w];
    for (int j = 0; j < w; ++j) {
      p[j] = new PVector(map(j, 0, w - 1, 0, hillGen.width), 
        map(noise(j / (float)w), 0, 1, 0, hillGen.height * 0.95));
      hillGen.vertex(p[j].x, p[j].y);
    }
    hillGen.vertex(hillGen.width, hillGen.height);
    hillGen.endShape(CLOSE);

    // Save the created image as a .png file. Specify the data
    // folder in the path so that these images can in turn be
    // imported later in setup.
    hillGen.save("data/hills" + i + ".png");

    // Conclude drawing the miniature world.
    hillGen.dispose();
    hillGen.endDraw();
  }
}