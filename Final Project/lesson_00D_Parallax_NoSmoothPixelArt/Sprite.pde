class Sprite {
  protected StateMachine<Animation> animator;
  public PVector pos, scl;

  Sprite(PVector _pos, PVector _scl, PImage... _frames) {
    this(_pos, _scl, 
      new Animation("Default", 12, new PVector(), _scl, _frames));
  }

  Sprite(PVector _pos, PVector _scl, Animation... _anims) {
    pos = _pos;
    scl = _scl;
    animator = new StateMachine<Animation>(_anims);
  }

  public void draw() {
    pushMatrix();
    translate(pos.x, pos.y);
    this.animator.current.draw();
    popMatrix();
  }

  public void addAnimation(Animation anim) {
    animator.add(anim);
  }

  public void addAnimation(String name, int interval, PImage... frames) {
    animator.add(new Animation(name, interval, 
      new PVector(), scl, frames));
  }

  public void addAnimation(String name, int interval, String... paths) {
    animator.add(new Animation(name, interval, 
      new PVector(), scl, paths));
  }

  public Animation removeAnimation(String request) {
    return animator.remove(request);
  }

  public Animation getAnimation(String request) {
    return animator.get(request);
  }

  public void setAnimation(String request) {
    animator.set(request);
  }

  public void setAnimation(Animation anim) {
    animator.set(anim);
  }
}