// Variables to govern the red-black ellipses in the background.
float bkgScale, bkgIncr, bkgRadius;

float sunX, sunY, sunRadius;

float planetRadius, planetDistance, 
  planetX, planetY, 
  planetAngle, planetSpeed;

float moonRadius, moonDistance, 
  moonX, moonY, 
  moonAngle, moonSpeed;

float mouseAngle;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Solar System");
  pixelDensity(displayDensity());
  size(420, 420);
  //fullScreen();
  background(32);
  ellipseMode(CENTER);  
  noStroke();

  bkgScale = min(height, width) / 30.0;
  bkgIncr = 0.25;
  bkgRadius = 50;

  sunX = width / 2.0;
  sunY = height / 2.0;
  sunRadius = min(height, width) / 7.5;

  planetRadius = min(height, width) / 10.0;
  planetDistance = planetRadius * 3;
  planetX = sunX + planetDistance;
  planetY = sunY;
  planetSpeed = 0.02;

  moonRadius = min(height, width) / 15.0;
  moonDistance = moonRadius * 1.5;
  moonX = planetX + moonDistance;
  moonY = planetY;
  moonSpeed = -0.01;
}

void draw() {
  //background(32);
  fill(32, 32, 32, 24);
  rect(0, 0, width, height);

  // Creates the background spiral of ellipses.

  // As bkgRadius increases, the base radius of each spiral
  // increases, leading to the illusion that the ellipses expand
  // as the mouse moves from the left to the right.
  bkgRadius = map(mouseX, 0, width, 20, 100);

  // As bkgIncr decreases, the number of ellipses in each spiral
  // will increase, leading to an increasingly smoother appearance
  // as the mouse moves to the top of the screen.
  bkgIncr = map(mouseY, 0, height, 0.0125, 0.5);

  // The x-coordinate uses the result of the cosine function,
  // The y-coordinate uses the result of the sine function.
  for (float i = 0; i < TWO_PI; i+= bkgIncr) {
    float sz = bkgScale * i;
    // Circles become increasingly red.
    fill(map(i, 0, TWO_PI, 255, 0), /* Red */
      0, /* Green */
      0, /* Blue */
      map(i, 0, TWO_PI, 0, 255)); /* Transparency */

    // To make a spiral rather than a circular motion, 
    // the radius is multiplied by i, which increases
    // every time through the loop. The same holds true
    // for the scale of the ellipses.
    ellipse(sunX + cos(i) * bkgRadius * i, 
      sunY + sin(i) * bkgRadius * i, 
      sz, sz);

    ellipse(sunX + cos(i + PI) * bkgRadius * i + PI, 
      sunY + sin(i + PI) * bkgRadius * i + PI, 
      sz, sz);
  }

  // Sun.
  sunX = width / 2.0;
  sunY = height / 2.0;
  fill(255, 204, 0);
  ellipse(sunX, sunY, sunRadius, sunRadius);

  // Planet.
  planetAngle += planetSpeed;
  // General formula: offset + function * scalar.
  planetX = sunX + cos(planetAngle) * planetDistance;
  planetY = sunY + sin(planetAngle) * planetDistance;
  fill(0, 75, 255);
  ellipse(planetX, planetY, planetRadius, planetRadius);

  // Moon.
  moonAngle += moonSpeed;
  moonX = planetX + cos(moonAngle) * moonDistance;
  moonY = planetY + sin(moonAngle) * moonDistance;
  fill(127);
  ellipse(moonX, moonY, moonRadius, moonRadius);
}

// Slow down the revolutions of the planets
// if the mouse is pressed.
void mousePressed() {
  planetSpeed /= 2.0;
  moonSpeed /= 2.0;
}

void mouseReleased() {
  planetSpeed *= 2.0;
  moonSpeed *= 2.0;
}