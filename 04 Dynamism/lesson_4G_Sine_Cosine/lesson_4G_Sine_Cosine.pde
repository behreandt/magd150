float theta, cosTheta, sinTheta, 
  rotationSpeed;
float x1, y1, w, h, 
  x2, y2, 
  x3, y3;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Sine and Cosine");
  pixelDensity(displayDensity());
  size(420, 420);
  background(32);
  noStroke();

  // The size of the circles is 1/4th the shorter edge.
  w = h = min(width, height) * 0.25;

  // Circle 1.
  x1 = width * 0.25;
  y1 = height * 0.5;

  // Circle 2.
  x2 = width * 0.5;
  y2 = height * 0.5;

  // Circle 3.
  x3 = width * 0.5;
  y3 = height * 0.5;

  // Based on the mouse's position, theta will
  // increase based on these minimum and maximum
  // rotation speeds.
  rotationSpeed = 0.1;
}

void draw() {
  //background(32);
  fill(32, 32);
  rect(0, 0, width, height);

  // Increase the angle every frame.
  theta = theta + map(mouseX, 0, width, -rotationSpeed, rotationSpeed);
  // Store the sine and cosine in the floats.
  cosTheta = cos(theta);
  sinTheta = sin(theta);

  // The cos circle transforms the cosine of theta
  // into horizontal movement.
  x1 = map(cosTheta, -1, 1, w, width - w);
  y1 = height * 0.5;
  fill(255, map(sinTheta, -1, 1, 0, 255), 0, 204);
  ellipse(x1, y1, w * 0.5, h * 0.5);

  // The sin circle transforms the sine of theta
  // into vertical movement.
  x2 = width * 0.5;
  y2 = map(sinTheta, -1, 1, h, height - h);
  fill(0, map(cosTheta, -1, 1, 0, 255), 255, 204);
  ellipse(x2, y2, w * 0.5, h * 0.5);

  // The sin & cos circle takes both oscillations
  // into consideration.
  x3 = map(cosTheta, -1, 1, w, width - w);
  y3 = map(sinTheta, -1, 1, h, height - h);
  fill(154, 0, 154, 127);
  ellipse(x3, y3, w, h);
}