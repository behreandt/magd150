boolean dragging;
float x, y, 
  offsetX, offsetY, 
  radius, speed, 
  hue, sat, bri;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Mouse Draggable Objects");
  pixelDensity(displayDensity());
  size(680, 420);
  background(32);
  ellipseMode(RADIUS);
  colorMode(HSB, 359, 99, 99);
  noStroke();

  // Give the circle a starting position
  // at the center of the screen and a random
  // color.
  dragging = false;
  x = width * 0.5;
  y = height * 0.5;
  radius = 50;
  speed = 5;

  hue = random(360);
  sat = random(50, 100);
  bri = random(75, 100);
}

void draw() {
  fill(0x12202020);
  rect(0, 0, width, height);

  // If the mouse is dragging the circle, then it matches
  // the mouse's position. If not, then the circle takes a
  // random walk by moving +/- 5 pixels on the x and y axis.
  if (dragging) {
    x = mouseX - offsetX;
    y = mouseY - offsetY;
  } else {
    x = constrain(x + random(-speed, speed), 
      radius, width - radius);
    y = constrain(y + random(-speed, speed), 
      radius, height - radius);
  }

  // Show the circle.
  fill(hue, sat, bri);
  ellipse(x, y, radius, radius);
}

void mouseDragged() {
  if (dragging) {
    hue = (hue + 1) % 360;
  }
}

void mousePressed() {
  // Formula for finding the intersection of a point
  // and circle: if the distance between the point and
  // the center of the circle is less than the radius,
  // then the two intersect.
  if (dist(mouseX, mouseY, x, y) < radius) {

    // The mouse may not be pressed down on exactly the
    // center of the circle, and we don't want the circle
    // to 'jump' to the mouse's position when it's moved.
    // Therefore, we record the offset of the mouse's
    // position on first click from the center.
    offsetX = mouseX - x;
    offsetY = mouseY - y;
    dragging = true;
  }
}

void mouseReleased() {
  dragging = false;
}