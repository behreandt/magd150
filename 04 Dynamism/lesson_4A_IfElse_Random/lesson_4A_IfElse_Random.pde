// Decimal numbers.
float xPosition, yPosition;
float xSpeed, ySpeed;
float w, h;
float xMinBoundary, xMaxBoundary, 
  yMinBoundary, yMaxBoundary;

// Whole numbers.
int xDirection = 1, yDirection = 1;
int choice = 0;

void setup() {
  surface.setResizable(true);
  surface.setTitle("If-Else and Random");
  pixelDensity(displayDensity());
  size(420, 420);
  //fullScreen();
  background(32);
  noStroke();

  xPosition = width / 2.0;
  yPosition = height / 2.0;

  // random(minimum, maximum);
  xSpeed = random(-3.0, 3.0);
  ySpeed = random(-3.0, 3.0);

  xMinBoundary = random(width * 0.125, width * 0.25);
  yMinBoundary = random(height * 0.125, height * 0.25);
  xMaxBoundary = random(width * 0.875, width * 0.75);
  yMaxBoundary = random(height * 0.875, height * 0.75);

  // choice is an integer, meaning it can only hold
  // whole numbers; random returns a decimal number.
  // So the information random returns must be simplified
  // down into a whole number. This is done with int().
  choice = int(random(0, 4));
  println("The random number is " + choice + "!");

  w = height / 10.0;
  h = height / 10.0;
}

void draw() {
  // Translucent background (16 / 255 opaque).
  fill(32, 16);
  rect(0, 0, width, height);

  // Draw boundary lines that were randomly chosen.
  strokeWeight(1);
  stroke(255);
  // Horizontal bounds.
  line(xMinBoundary, yMinBoundary, xMaxBoundary, yMinBoundary);
  line(xMinBoundary, yMaxBoundary, xMaxBoundary, yMaxBoundary);

  // Vertical bounds.
  line(xMinBoundary, yMinBoundary, xMinBoundary, yMaxBoundary);
  line(xMaxBoundary, yMinBoundary, xMaxBoundary, yMaxBoundary);

  noStroke();

  // An if-statement executes the code between the
  // curly braces if the statement between the parentheses
  // is true.

  // If it is true that the random number is equal to 0...
  if (choice == 0) {
    // ...then execute the following code.
    fill(map(xPosition, 0, width, 0, 255), 
      map(yPosition, 0, height, 0, 255), 
      random(127, 255));
    ellipse(xPosition, yPosition, w, h);

    // If it wasn't true that choice is equal to 0,
    // instead check to see if choice is equal to 1...
  } else if (choice == 1) {
    w = h = 0;
    stroke(random(127, 255), 
      random(127, 255), 
      random(127, 255));
    strokeWeight(2);
    line(xPosition, 
      yPosition, 
      map(mouseX, 0, width, xMinBoundary, xMaxBoundary), 
      map(mouseY, 0, height, yMinBoundary, yMaxBoundary));
    noStroke();

    // If it wasn't true that choice is equal to 1,
    // instead check to see if choice is equal to 2...
  } else if (choice == 2) {
    w = h = 0;
    stroke(random(54, 204), random(54, 255), random(0, 204));
    strokeWeight(2);
    bezier(map(mouseX, 0, width, xMinBoundary, xMaxBoundary), 
      map(mouseY, 0, height, yMinBoundary, yMaxBoundary), 
      mouseX, 
      mouseY, 
      random(0, width), 
      random(0, height), 
      xPosition, 
      yPosition
      );
    noStroke();

    // If the choice wasn't equal to any of the above,
    // then execute this code as a default.
  } else {
    w = h = 0;
    fill(random(0, 255), 
      map(xPosition, 0, width, 0, 255), 
      map(yPosition, 0, height, 0, 255), 100);
    beginShape();
    vertex(xPosition, yPosition);
    vertex(map(mouseX, 0, width, xMinBoundary, xMaxBoundary), 
      map(mouseY, 0, height, yMinBoundary, yMaxBoundary));
    vertex(mouseX, mouseY);
    vertex((xMaxBoundary - xMinBoundary) / 2.0, 
      (yMaxBoundary - yMinBoundary) / 2.0);
    endShape();
  }

  // Movement is a position + speed * direction.
  xPosition = xPosition + xSpeed * xDirection;
  yPosition = yPosition + ySpeed * yDirection;

  // The || stands for 'or'. So if either x < 0 or x > width
  // is true, then the whole conjunction is true, meaning
  // the code in the if-statement will be executed.

  // if (xPosition <= 0
  // || xPosition >= width) {
  // /* ... */ }
  // if (xPosition <= w / 2.0
  // || xPosition >= width - w / 2.0) {
  // /* ... */ }

  // Because a circle is drawn from the center by default, and
  // we want the circle to bounce when its edge hits the wall, we
  // have to take its radius into account.
  if (xPosition <= xMinBoundary + w / 2.0
    || xPosition >= xMaxBoundary - w / 2.0) {
    xDirection = -xDirection;
  }

  if (yPosition <= yMinBoundary + h / 2.0
    || yPosition >= yMaxBoundary - h / 2.0) {
    yDirection = -yDirection;
  }
}