// char is short for 'character'. It stores individual
// keys like 'a' or 'A'.
char upKey, leftKey, downKey, rightKey;
float x, y, radius, speed;
boolean up, left, down, right;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Responding to Specific Keys");
  pixelDensity(displayDensity());
  size(680, 420);
  background(32);
  ellipseMode(RADIUS);

  // Set characters to initial value.
  upKey = 'w';
  leftKey = 'a';
  downKey = 's';
  rightKey = 'd';

  // Set float variables to initial value.
  x = width / 2.0;
  y = height / 2.0;
  radius = min(width, height) / 7.5;
  speed = 3;

  // Set boolean variables to initial value.
  up = left = right = down = false;
}

void draw() {
  background(32);
  strokeWeight(2);
  stroke(255, 255, 0);

  fill(
    map(x, 0, width, 54, 255), 
    27, 
    map(y, 0, height, 54, 204)
    );
  ellipse(x, y, radius, radius);

  // There's a limitation to working this way: only one
  // key is being registered at a time, so our 'character'
  // can't move diagonally.
  if (keyPressed) {
    if (key == upKey) {
      y -= speed;

      // The triangle indicates the direction the circle
      // is heading.
      fill(0, 100, 255);
      triangle(x, y - radius / 2.0, 
        x - radius / 2.0, y, 
        x + radius / 2.0, y);
    }

    if (key == leftKey) {
      x -= speed;

      fill(0, 100, 255);
      triangle(x - radius / 2.0, y, 
        x, y - radius / 2.0, 
        x, y + radius / 2.0);
    }

    if (key == downKey) {
      y += speed;

      fill(0, 100, 255);
      triangle(x, y + radius / 2.0, 
        x - radius / 2.0, y, 
        x + radius / 2.0, y);
    }

    if (key == rightKey) {
      x += speed;

      fill(0, 100, 255);
      triangle(x + radius / 2.0, y, 
        x, y - radius / 2.0, 
        x, y + radius / 2.0);
    }
  }
}


// Controlling motion this way is considerably
// less smooth than controlling motion with
// the if(keyPressed) {} in draw() {} above.
// There is also a discrepancy between how this
// behaves on Macs vs. on PCs.
/* void keyPressed() {
  if (key == upKey) {
    y -= speed;
    fill(0, 100, 255);
    triangle(x, y - radius / 2.0, 
      x - radius / 2.0, y, 
      x + radius / 2.0, y);
  }
  if (key == leftKey) {
    x -= speed;
    fill(0, 100, 255);
    triangle(x - radius / 2.0, y, 
      x, y - radius / 2.0, 
      x, y + radius / 2.0);
  }
  if (key == downKey) {
    y += speed;
    fill(0, 100, 255);
    triangle(x, y + radius / 2.0, 
      x - radius / 2.0, y, 
      x + radius / 2.0, y);
  }
  if (key == rightKey) {
    x += speed;
    fill(0, 100, 255);
    triangle(x + radius / 2.0, y, 
      x, y - radius / 2.0, 
      x, y + radius / 2.0);
  }
} */