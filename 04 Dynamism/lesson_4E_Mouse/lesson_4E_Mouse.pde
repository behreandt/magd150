int mouseHeartBeat = 0;
int mouseWheel = 0;

float scrollSpeed = 3.0;
float wheelY;
float deltaMouse;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Mouse Interaction");
  pixelDensity(displayDensity());
  size(680, 420);
  background(32);
  noCursor();
  rectMode(CENTER);
  wheelY = height * 0.5;
}

void draw() {
  // Translucent background which allows for what is
  // called onion-skinning in animation.
  noStroke();
  fill(32, 32, 32, 20);
  rect(width * 0.5, height * 0.5, width, height);

  // pmouseX and pmouseY record the mouse's position at
  // the previous frame. When pmouseX and pmouseY are
  // subtracted from mouseX and mouseY, you can get a
  // the speed of the mouse.
  deltaMouse = dist(mouseX, mouseY, pmouseX, pmouseY);

  // Assumes that the max distance between the current
  // and previous mouse will be 420 (an arbitrary number).
  // The range from 0 to 420 is then remapped from 0 to height.
  float h = map(deltaMouse, 0, 420, 0, height);

  // Keeps moving the bar which represents the mouse speed
  // from the left to the right of the screen until it reaches
  // the right boundary, then it is reset to 0.
  mouseHeartBeat = (mouseHeartBeat + 1) % width;

  strokeWeight(1);
  stroke(0, 127, 255);
  line(mouseHeartBeat, height, mouseHeartBeat, height - h);

  // We can express this same information by giving a 'trail'
  // to the mouse position itself. Instead of remapping mouse speed
  // to a line height above, we can instead remap it to color info.
  float heat = map(deltaMouse, 0, 200, 0, 255);
  strokeWeight(2);
  stroke(255, heat, 0, 127);
  line(pmouseX, pmouseY, mouseX, mouseY);

  // Within the mousePressed condition check, you can look
  // to see which mouseButton is being pressed and respond
  // accordingly, using right for alt functionality.
  if (mousePressed) {
    noStroke();
    // If the left mouse button is clicked, then a yellow
    // rectangle will show on the left side of the screen.
    // If the right mouse button is clicked, then a blue
    // rectangle will show on the right side. If the center
    // button is clicked, then a rectangle will appear in the
    // center of the screen.
    if (mouseButton == LEFT) {
      fill(64, 64, 0, 20);
      rect(width / 4.0, height / 2.0, width / 2.0, height);
    } else if (mouseButton == RIGHT) {
      fill(0, 64, 64, 20);
      rect(width * 3 / 4.0, height / 2.0, width / 2.0, height);
    } else if (mouseButton == CENTER) {
      fill(15, 32, 15, 20);
      rect(width / 2.0, height / 2.0, width / 2.0, height);
    }
  }

  // Draws the ball in the center which represents the scroll
  // wheel. This ball will move up or down depending on middle
  // button scrolling.
  noStroke();
  fill(255);
  ellipse(width / 2.0, wheelY, 15, 15);
}

// The mouseWheel() event function can receive a parameter
// which you use to get the direction of the mouse scroll.
// scrolling up = -1, scrolling down = 1.
void mouseWheel(MouseEvent me) {
  mouseWheel = me.getCount();
  if ((wheelY < height && mouseWheel == 1)
    || (wheelY > 0 && mouseWheel == -1)) {
    wheelY += mouseWheel * scrollSpeed;
  }
}

void mousePressed() {
  // LEFT = 37
  // RIGHT = 39
  // CENTER = 3
  println(mouseButton + " pressed.");

  noStroke();
  fill(255, 255, 0, 240);
  rect(mouseX, mouseY, 30, 30, 5);
}

void mouseDragged() {
  println(mouseButton + " dragged.");

  noStroke();
  fill(255, 204, 0, 240);
  ellipse(mouseX, mouseY, 5, 5);
}

void mouseReleased() {
  println(mouseButton + " released.");

  noStroke();
  fill(255, 127, 0, 240);
  rect(mouseX, mouseY, 30, 30, 5);
}