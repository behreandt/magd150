float diameter = 30;
float margin = 2.5;
float bezierCount = 20;

// Variables to track circle red, green, blue and transparency.
float r, g, b, a;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Nested Loops");
  pixelDensity(displayDensity());
  size(420, 420);
  //fullScreen();
  noStroke();
  diameter = height / 12.0;
}

void draw() {
  background(32);
  noStroke();

  // The major/outer/first loop draws the columns starting at 0 +
  // the circle's radius, stopping at the width of the screen,
  // and adding the diameter plus a margin between each circle
  // every time the loop repeats.
  for (float x = diameter / 2.0; x < width; x = x + diameter + margin) {
    
    // The minor/inner/second loop draws the rows starting at 0 +
    // the circle's radius, stopping at the height of the screen,
    // and adding the diameter plus a margin between each circle
    // every time the loop repeats.
    for (float y = diameter / 2.0; y < height; y = y + diameter + margin) {
      
      // The further right the circle is, the more red it contains.
      r = map(x, 0, width, 0, 255);
      // The further down the circle is, the more green it contains.
      g = map(y, 0, height, 0, 255);
      // The amount of blue in the circle is based on mouseX.
      b = map(mouseX, 0, width, 0, 255);
      // The transparency of the circle is based on mouseY.
      a = map(mouseY, 0, height, 0, 255);

      fill(r, g, b, a);
      ellipse(x, y, diameter, diameter);
      fill(g, b, a, r);
      ellipse(x, y, diameter * 0.75, diameter * 0.75);
      fill(b, a, r, g);
      ellipse(x, y, diameter * 0.5, diameter * 0.5);
      fill(a, r, g, b);
      ellipse(x, y, diameter * 0.33, diameter * 0.33);
    }
  }

  // Draw the bezier curves.
  int i = 0;
  noFill();
  strokeCap(SQUARE);
  
  // While loops provide less constraint. So long as a
  // condition is true, the while loop will repeat. Beware
  // of creating a loop with no exit condition.
  while (i < bezierCount) {
    strokeWeight(i / bezierCount * 2.0);
    a = map(i, 0, bezierCount, 54, 204);
    stroke(255, 255, 255, a);

    bezier(mouseX, mouseY, 
      i * bezierCount, i * bezierCount / 2.0, 
      i * bezierCount / 2.0, i * bezierCount, 
      width - mouseX, height - mouseY);

    // i increases every time through the loop, meaning
    // it will eventually be greater than the bezierCount.
    i++;
  }
}