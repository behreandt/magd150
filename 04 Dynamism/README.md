# Assignment 4
## Dynamism
### 100 points

![Dynamism](preview.png)

### References

* Daniel Shiffman, [_Learning Processing_](http://learningprocessing.com/examples/) Chapters 5 - 6
* Shiffman, [Video: Variables](https://youtu.be/B-ycSR3ntik?list=PLRqwX-V7Uu6aFNOgoIMSbSYOkKNTo89uf)
* Shiffman, [Video: Interaction](https://youtu.be/o8dffrZ86gs?list=PLRqwX-V7Uu6by61pbhdvyEpIeymlmnXzD)
* Shiffman, [Video: Conditionals](https://youtu.be/wsI6N9hfW7E?list=PLRqwX-V7Uu6YqykuLs00261JCqnL_NNZ_)
* Shiffman, [Video: Loops](https://youtu.be/RtAPBvz6k0Y?list=PLRqwX-V7Uu6bm-3M4Wntd4yYZGKwiKfrQ)
* [Tutorial: Interactivity](https://processing.org/tutorials/interactivity/)
* [Tutorial: Trigonometry](https://processing.org/tutorials/trig/)

### Tools

* [Key Listener](../Utilities/keyListenerDemo/KeyListener.pde)
* [Timer](../Utilities/timerDemo/Timer.pde)

### Instructions

1. Adhere to the theme to be determined in lab. (10 pts.)
    * Wednesday: Oceans (of Pizza?)
    * Monday: Bouncy Balls

2. Program movement or animation upon running the sketch. (10 pts.)

3. Program mouse-based user interaction. (20 pts.)
    * [Mouse Pressed](https://processing.org/reference/mousePressed.html)
    * [void mousePressed() {}](https://processing.org/reference/mousePressed_.html)

4. Program keyboard-based user interaction. (20 pts.)
    * [Key Pressed](https://processing.org/reference/keyPressed.html)
    * [void keyPressed() {}](https://processing.org/reference/keyPressed_.html)

5. Use at least 3 [if](https://processing.org/reference/if.html)-[else](https://processing.org/reference/else.html) conditional statements. (10 pts.)

6. Use at least one loop. (10 pts.)
    * [For-loop](https://processing.org/reference/for.html)
    * [While-loop](https://processing.org/reference/while.html)

7. Ensure that the sketch runs without errors. The main tab of the sketch should be the only tab with global-scope setup and draw functions. (10 pts.)

8. Ensure that the main tab of the sketch shares the same name as the sketch folder. Name the sketch folder and main tab according to the following convention: s17_magd150_lab04_yourlastname. Compress the sketch folder into a .zip file and upload to D2L. (10 pts.)

__Any code or media within a submission are presumed to be the work of the student(s) unless otherwise stated. When media or code from outside sources are used, a link to the source and notice of attribution at the top of the main sketch in a comment should be provided. Media and code used are expected to be from the intellectual commons (i.e., not copyrighted or registered as a trademark) and to be altered substantially by the student(s). On first incident, students whose work does not meet this standard will have one week to resubmit the assignment for half credit. On second incident, students will be referred to Student Conduct Programs and will fail the course.__
