void setup() {
  surface.setResizable(true);
  surface.setTitle("Keyboard Entry");
  pixelDensity(displayDensity());
  size(420, 420);
  background(32);
  // As with a word processor, Processing aligns the text
  // relative to the (x, y) coordinates you provide it based
  // on this function. The first parameter is for horizontal
  // alignment, the second is for vertical alignment. Other
  // potential combinations include (LEFT, TOP) and (RIGHT, BOTTOM).
  textAlign(CENTER, CENTER);
}

void draw() {
  fill(32, 64);
  rect(0, 0, width, height);

  // Processing is confusing in regards to the difference
  // between keyPressed and keyPressed(). The former is a
  // boolean (true or false) used with an if conditional,
  // the latter is a function that you define below which
  // is called during an event.
  if (keyPressed) {
    fill(255);

    // The size of the text can be set with this function.
    textSize(140);

    // text("message", xCoord, yCoord);
    // The pink key keyword will tell you the specific key pressed.
    text(key, width / 2.0, height / 4.0);
  }

  textSize(12);
  fill(255, 255, 0);
  text("PRESSED", width / 4.0, (height / 2.0) + 76);
  text("TYPED", width / 2.0, (height * 3 / 4.0) + 76);
  text("RELEASED", (width * 3 / 4.0), height / 2.0 + 76);
  text("IF(keyPressed) IN DRAW", width / 2.0, height / 4.0 + 76);
}

// The following are event functions. See the sketch for
// reference as to when they are fired.
void keyPressed() { /* RED */
  println(key + " " + keyCode + " pressed.");
  textSize(140);
  fill(255, 0, 0);
  text(key, width / 4.0, height / 2.0);
}

void keyTyped() { /* BLUE */
  textSize(140);
  fill(0, 0, 255);
  text(key, width / 2.0, height * 3/ 4.0);
}

void keyReleased() { /* GREEN */
  textSize(140);
  fill(0, 255, 0);
  text(key, width * 3 / 4.0, height / 2.0);
}