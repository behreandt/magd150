float paddle1Left, paddle1Top, paddle1Right, paddle1Bottom, 
  paddle1CenterX, paddle1CenterY, 

  paddle2Left, paddle2Top, paddle2Right, paddle2Bottom, 
  paddle2CenterX, paddle2CenterY, 

  screenPaddleMargin, 
  paddleHalfWidth, 
  paddleHalfHeight, 
  paddleSpeed, 

  ballCenterX, ballCenterY, ballRadius, 
  ballLeft, ballTop, ballRight, ballBottom, 
  ballXSpeed, ballYSpeed, ballXDirection = 1, ballYDirection = 1, 
  // Increases the speed of the ball slightly every time
  // it is hit by a paddle.
  challengeScalar = 1.05;

color ball, paddle1, paddle2;

char keyIgnoreCase, paddle1UpKey = 'w', paddle1DownKey = 's';

int paddle1Score, paddle2Score;

boolean paddle1Ball, paddle2Ball;

void setup() {
  surface.setTitle("Pong");
  pixelDensity(displayDensity());
  size(680, 420);
  background(32);
  strokeWeight(1.5);
  stroke(255);
  ellipseMode(RADIUS);
  rectMode(CORNERS);

  ballRadius = min(width, height) / 38.0;
  ballCenterX = random((width * 0.25) + ballRadius, 
    (width * 0.75) - ballRadius);
  ballCenterY = random((height * 0.25) + ballRadius, 
    (height * 0.75) - ballRadius);
  ball = color(230, 0, 0, 204);

  ballXSpeed = random(1) < 0.5 ? random(1, 4) : random(-4, -1);
  ballYSpeed = random(1) < 0.5 ? random(1, 4) : random(-4, -1);

  screenPaddleMargin = width / 65.0; 
  paddleHalfWidth = width / 100.0; 
  paddleHalfHeight = height / 12.0;
  paddleSpeed = 5.5;

  paddle1Left = screenPaddleMargin;
  paddle1Right = paddle1Left + paddleHalfWidth * 2.0;
  paddle1CenterX = paddle1Left + paddleHalfWidth;
  paddle1CenterY = height / 2.0;
  paddle1Top = paddle1CenterY - paddleHalfHeight;
  paddle1Bottom = paddle1CenterY + paddleHalfHeight;
  paddle1 = color(0, 200, 0, 204);

  paddle2Right = width - screenPaddleMargin;
  paddle2Left = paddle2Right - paddleHalfWidth * 2.0;
  paddle2CenterX = paddle2Left + paddleHalfWidth;
  paddle2CenterY = height / 2.0;
  paddle2Top = paddle2CenterY - paddleHalfHeight;
  paddle2Bottom = paddle2CenterY + paddleHalfHeight;
  paddle2 = color(0, 0, 230, 204);
}

void draw() {
  noStroke();
  fill(32, 32, 32, 52);
  rect(0, 0, width, height);

  ballCenterX += ballXSpeed * ballXDirection;
  ballCenterY += ballYSpeed * ballYDirection;

  if (ballCenterY <= ballRadius || ballCenterY >= height - ballRadius) {
    ballYDirection *= -1;
  }

  // If the ball reaches the left side of the screen and paddle2 was the
  // last to hit it, then paddle2 gains a point. The ball then resets to
  // a position in the center.
  if (ballCenterX <= 0) {
    if (paddle2Ball) {
      paddle2Score++;
    }
    ballCenterX = random((width * 0.25) + ballRadius, 
      (width * 0.75) - ballRadius);
    ballCenterY = random((height * 0.25) + ballRadius, 
      (height * 0.75) - ballRadius);
    ballXSpeed = random(1) < 0.5 ? random(1, 4) : random(-4, -1);
    ballYSpeed = random(1) < 0.5 ? random(1, 4) : random(-4, -1);
    paddle1Ball = false;
    paddle2Ball = false;
    ball = color(230, 0, 0, 204);
  }

  // If the ball reaches the right side of the screen and paddle1 was the
  // last to hit it, then paddle1 gains a point. The ball then resets to a
  // position at the center.
  if (ballCenterX >= width) {
    if (paddle1Ball) {
      paddle1Score++;
    }
    ballCenterX = random((width * 0.25) + ballRadius, 
      (width * 0.75) - ballRadius);
    ballCenterY = random((height * 0.25) + ballRadius, 
      (height * 0.75) - ballRadius);
    ballXSpeed = random(1) < 0.5 ? random(1, 4) : random(-4, -1);
    ballYSpeed = random(1) < 0.5 ? random(1, 4) : random(-4, -1);
    paddle1Ball = false;
    paddle2Ball = false;
    ball = color(230, 0, 0, 204);
  }

  stroke(127);
  ballLeft = ballCenterX - ballRadius;
  ballRight = ballCenterX + ballRadius;
  ballTop = ballCenterY - ballRadius;
  ballBottom = ballCenterY + ballRadius;

  stroke(255);
  fill(ball);
  ellipse(ballCenterX, ballCenterY, ballRadius, ballRadius);
  point(ballCenterX, ballCenterY);

  fill(paddle1);
  rect(paddle1Left, paddle1Top, paddle1Right, paddle1Bottom, 3);
  point(paddle1CenterX, paddle1CenterY);

  fill(paddle2);
  rect(paddle2Left, paddle2Top, paddle2Right, paddle2Bottom, 3);
  point(paddle2CenterX, paddle2CenterY);

  if (keyPressed) {
    if (key == CODED) {
      if (keyCode == UP && paddle2Top > 0) {
        paddle2Top -= paddleSpeed;
        paddle2Bottom -= paddleSpeed;
        paddle2CenterY -= paddleSpeed;
      }
      if (keyCode == DOWN && paddle2Bottom < height) {
        paddle2Top += paddleSpeed;
        paddle2Bottom += paddleSpeed;
        paddle2CenterY += paddleSpeed;
      }
    } else {
      keyIgnoreCase = Character.toLowerCase(key);
      if (keyIgnoreCase == paddle1UpKey && paddle1Top > 0) {
        paddle1Top -= paddleSpeed;
        paddle1Bottom -= paddleSpeed;
        paddle1CenterY -= paddleSpeed;
      }
      if (keyIgnoreCase == paddle1DownKey && paddle1Bottom < height) {
        paddle1Top += paddleSpeed;
        paddle1Bottom += paddleSpeed;
        paddle1CenterY += paddleSpeed;
      }
    }
  }

  if (ballLeft < paddle1Right
    && paddle1Left < ballRight
    && ballTop < paddle1Bottom
    && paddle1Top < ballBottom) {
    ballXDirection *= -1;
    paddle1Ball = true;
    paddle2Ball = false;
    ball = color(0, 255, 255, 204);
    ballXSpeed *= challengeScalar;
  }

  if (ballLeft < paddle2Right
    && paddle2Left < ballRight
    && ballTop < paddle2Bottom
    && paddle2Top < ballBottom) {
    ballXDirection *= -1;
    paddle1Ball = false;
    paddle2Ball = true;
    ball = color(255, 255, 0, 204);
    ballXSpeed *= challengeScalar;
  }

  textAlign(CENTER, TOP);
  textSize(24);
  fill(255);
  text(paddle1Score, 26, 16);
  text(paddle2Score, width - 26, 16);
}