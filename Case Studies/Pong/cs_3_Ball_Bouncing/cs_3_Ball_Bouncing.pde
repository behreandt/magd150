float paddle1Left, paddle1Top, paddle1Right, paddle1Bottom, 
  paddle1CenterX, paddle1CenterY, 

  paddle2Left, paddle2Top, paddle2Right, paddle2Bottom, 
  paddle2CenterX, paddle2CenterY, 

  screenPaddleMargin, 
  paddleHalfWidth, 
  paddleHalfHeight, 
  paddleSpeed, 

  ballCenterX, ballCenterY, ballRadius, 
  ballLeft, ballTop, ballRight, ballBottom, 
  // These new variables govern the motion of the ball.
  ballXSpeed, ballYSpeed, ballXDirection = 1, ballYDirection = 1;

color ball, paddle1, paddle2;

char keyIgnoreCase, paddle1UpKey = 'w', paddle1DownKey = 's';

void setup() {
  surface.setTitle("Pong");
  pixelDensity(displayDensity());
  size(680, 420);
  background(32);
  strokeWeight(1.5);
  stroke(255);
  ellipseMode(RADIUS);
  rectMode(CORNERS);

  ballRadius = min(width, height) / 40.0;
  ballCenterX = random((width * 0.25) + ballRadius, 
    (width * 0.75) - ballRadius);
  ballCenterY = random((height * 0.25) + ballRadius, 
    (height * 0.75) - ballRadius);
  ball = color(230, 0, 0, 204);

  // This ternary operator (conditional ? do if true : do if false)
  // is used to simulate a coin toss, where the initial speed of the
  // ball will be either 3 or -3 based on a 50-50 probability.
  ballXSpeed = random(1) < 0.5 ? 3 : -3;
  ballYSpeed = random(1) < 0.5 ? 3 : -3;

  screenPaddleMargin = width / 65.0; 
  paddleHalfWidth = width / 100.0; 
  paddleHalfHeight = height / 12.0;
  paddleSpeed = 5;

  paddle1Left = screenPaddleMargin;
  paddle1Right = paddle1Left + paddleHalfWidth * 2.0;
  paddle1CenterX = paddle1Left + paddleHalfWidth;
  paddle1CenterY = height / 2.0;
  paddle1Top = paddle1CenterY - paddleHalfHeight;
  paddle1Bottom = paddle1CenterY + paddleHalfHeight;
  paddle1 = color(0, 200, 0, 204);

  paddle2Right = width - screenPaddleMargin;
  paddle2Left = paddle2Right - paddleHalfWidth * 2.0;
  paddle2CenterX = paddle2Left + paddleHalfWidth;
  paddle2CenterY = height / 2.0;
  paddle2Top = paddle2CenterY - paddleHalfHeight;
  paddle2Bottom = paddle2CenterY + paddleHalfHeight;
  paddle2 = color(0, 0, 230, 204);
}

void draw() {
  background(32);

  fill(ball);
  ellipse(ballCenterX, ballCenterY, ballRadius, ballRadius);
  point(ballCenterX, ballCenterY);

  fill(paddle1);
  rect(paddle1Left, paddle1Top, paddle1Right, paddle1Bottom);
  point(paddle1CenterX, paddle1CenterY);

  fill(paddle2);
  rect(paddle2Left, paddle2Top, paddle2Right, paddle2Bottom);
  point(paddle2CenterX, paddle2CenterY);

  if (keyPressed) {
    if (key == CODED) {
      if (keyCode == UP && paddle2Top > 0) {
        paddle2Top -= paddleSpeed;
        paddle2Bottom -= paddleSpeed;
        paddle2CenterY -= paddleSpeed;
      }
      if (keyCode == DOWN && paddle2Bottom < height) {
        paddle2Top += paddleSpeed;
        paddle2Bottom += paddleSpeed;
        paddle2CenterY += paddleSpeed;
      }
    } else {
      keyIgnoreCase = Character.toLowerCase(key);
      if (keyIgnoreCase == paddle1UpKey && paddle1Top > 0) {
        paddle1Top -= paddleSpeed;
        paddle1Bottom -= paddleSpeed;
        paddle1CenterY -= paddleSpeed;
      }
      if (keyIgnoreCase == paddle1DownKey && paddle1Bottom < height) {
        paddle1Top += paddleSpeed;
        paddle1Bottom += paddleSpeed;
        paddle1CenterY += paddleSpeed;
      }
    }
  }

  // Add a speed multiplied by a direction to the ball's x and y position.
  ballCenterX += ballXSpeed * ballXDirection;
  ballCenterY += ballYSpeed * ballYDirection;

  // If the ball is less than the top of the screen plus its radius,
  // or if the ball is greater than the bottom of the screen minus its
  // radius, then reverse the ball's direction.
  if (ballCenterY <= ballRadius || ballCenterY >= height - ballRadius) {
    ballYDirection *= -1;
  }

  // If the ball is less than the left of the screen plus its radius,
  // or if the ball is greater than the right of the screen minus its radius,
  // then reverse the ball's direction.
  if (ballCenterX <= ballRadius || ballCenterX >= width - ballRadius) {
    ballXDirection *= -1;
  }
}