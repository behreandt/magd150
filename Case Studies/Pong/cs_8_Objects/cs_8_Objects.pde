// Reminder: in Processing's menu bar, go to File >
// Preferences and check the box that says Code completion
// with ctrl-space. Right click on a function or variable
// and use show usage to look at where else it is used in
// the program.

KeyListener kl;
Paddle p1, p2;
Ball ball;

void setup() {
  surface.setTitle("Pong");
  pixelDensity(displayDensity());
  size(680, 420);
  //fullScreen();
  background(32);

  ball = new Ball();

  // Since there is only ever one ball in Pong, a constructor
  // with no parameters will suffice. however, several key pieces
  // of information distinguish the right from the left paddle,
  // including horizontal position, color and which keys are used
  // to move them up and down.
  p1 = new Paddle(width / 65.0, 
    color(0, 200, 0, 204), 
    87, 83);

  p2 = new Paddle(width - width / 65.0, 
    color(0, 0, 230, 204), 
    UP, DOWN);

  kl = new KeyListener(p1.upKey, p1.downKey, p2.upKey, p2.downKey);

  // If there are properties that we want to set up later, such
  // as the color of the ball when a certain paddle has control
  // over it, we can access them with dot syntax.
  p1.possession = color(0, 255, 255, 204);
  p2.possession = color(255, 255, 0, 204);
}

void draw() {
  background(32);

  ball.update();
  p1.update();
  p2.update();

  if (p1.collision(ball)) {
    ball.rebound();
    ball.fill = p1.possession;
    p1.hasPossession = true;
    p2.hasPossession = false;
  }

  if (p2.collision(ball)) {
    ball.rebound();
    ball.fill = p2.possession;
    p2.hasPossession = true;
    p1.hasPossession = false;
  }

  if (ball.center.x < 0) {
    ball.start();
    if (p2.hasPossession) {
      p2.score++;
    }
    p1.hasPossession = false;
    p2.hasPossession = false;
  }

  if (ball.center.x > width) {
    ball.start();
    if (p1.hasPossession) {
      p1.score++;
    }
    p1.hasPossession = false;
    p2.hasPossession = false;
  }

  showScore();
}

void showScore() {
  textAlign(CENTER, TOP);
  textSize(24);
  fill(255);
  text(p1.score, 26, 16);
  text(p2.score, width - 26, 16);
}

void keyPressed() {
  kl.keyPressed(keyCode);
}

void keyReleased() {
  kl.keyReleased(keyCode);
}