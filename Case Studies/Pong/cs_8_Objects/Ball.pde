class Ball {

  int drawMode = RADIUS;
  // Scale will default to radius, not diameter.
  PVector center, scale, tl, br, speed;
  color fill;
  float challengeScalar = 1.05;

  // A default constructor which is quicker
  // to use and defaults to placing the ball
  // at the center of the screen.
  Ball() {
    start(width * 0.5, 
      height * 0.5, 
      min(width, height) * 0.053, 
      min(width, height) * 0.053);
  }

  // A constructor which allows the location
  // and size of the ball to be customized.
  Ball(float x, float y, float radius) {
    start(x, y, radius, radius);
  }

  public String toString() {
    return this.getClass().getSimpleName() +
      " (" + this.center.x + ", " + this.center.y + ")";
  }

  void start() {
    start(width * 0.5, 
      height * 0.5, 
      min(width, height) * 0.053, 
      min(width, height) * 0.053);
  }

  void start(float x, float y, float w, float h) {
    //TODO what if these rectangles are not drawn
    // using the assumed mode, but one of the other
    // 3 available? how would the calculations change?
    if (drawMode == RADIUS) {
      center = new PVector(x, y);
      scale = new PVector(w / 2.0, h / 2.0);
    } else if (drawMode == CENTER) {
    } else if (drawMode == CORNER) {
    } else if (drawMode == CORNERS) {
    } else {
    }
    setBounds();
    speed = new PVector(random(1.0, 4.0), 
      random(1.0, 4.0));
    // If a random number between 0 and 1 is less than
    // 0.5, then reverse the direction of the ball by
    // multiplying it by -1.
    if (random(1) < 0.5) {
      speed.mult(-1);
    }
    fill = color(230, 0, 0, 204);
  }

  void update() {
    pushStyle();

    //diagnosticEdges();

    ellipseMode(drawMode);
    strokeWeight(1);
    stroke(255);
    fill(fill);
    if (drawMode == RADIUS) {
      ellipse(center.x, center.y, scale.x, scale.y);
    } else if (drawMode == CENTER) {
    } else if (drawMode == CORNER) {
    } else if (drawMode == CORNERS) {
    } else {
    }
    stroke(255);
    point(center.x, center.y);
    move();
    setBounds();
    popStyle();
  }

  void move() {
    center.add(speed);
    if (center.y <= scale.y || center.y >= height - scale.y) {
      speed.y *= -1;
    }
  }

  void setBounds() {
    if (drawMode == RADIUS) {
      tl = PVector.sub(center, scale);
      br = PVector.add(center, scale);
    } else if (drawMode == CENTER) {
    } else if (drawMode == CORNER) {
    } else if (drawMode == CORNERS) {
    } else {
    }
  }

  void diagnosticEdges() {
    stroke(255, 54);
    strokeWeight(1);
    line(tl.x, tl.y, tl.x, br.y);
    line(br.x, tl.y, br.x, br.y);
    line(tl.x, tl.y, br.x, tl.y);
    line(tl.x, br.y, br.x, br.y);
  }

  // This is a fundamental dilemma of Object-Oriented Programming.
  // if a ball collides with a paddle, to which object does the
  // collision belong... or does each object take responsibility
  // for a 'part' of the collision?... or does a third object
  // adjudicate the collision between the two objects?
  boolean collision(Paddle p) {
    return tl.x < p.br.x
      && p.tl.x < br.x
      && tl.y < p.br.y
      && p.tl.y < br.y;
  }

  void rebound() {
    speed.x *= -1;
    speed.mult(challengeScalar);
  }
}