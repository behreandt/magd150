class Paddle {
  int drawMode = RADIUS, score;
  float speed = 5, cornerRounding = 3;
  PVector center, scale, tl, br, travel = new PVector(0, speed);
  color fill, possession;
  boolean hasPossession;
  int upKey, downKey;

  Paddle(float x, color fill, int upKey, int downKey) {
    //TODO what if these rectangles are not drawn
    // using the assumed mode, but one of the other
    // 3 available? how would the calculations change?
    if (drawMode == RADIUS) {
      center = new PVector(x, height / 2.0);
      scale = new PVector(width / 100.0, height / 12.0);
    } else if (drawMode == CENTER) {
    } else if (drawMode == CORNER) {
    } else if (drawMode == CORNERS) {
    } else {
    }
    setBounds();
    this.fill = fill;
    this.upKey = upKey;
    this.downKey = downKey;
  }

  public String toString() {
    return this.getClass().getSimpleName() +
      " (" + this.center.x + ", " + this.center.y + ")";
  }

  void update() {
    pushStyle();
    rectMode(drawMode);
    stroke(255);
    fill(fill);
    if (drawMode == RADIUS) {
      rect(center.x, center.y, scale.x, scale.y, cornerRounding);
    } else if (drawMode == CENTER) {
    } else if (drawMode == CORNER) {
    } else if (drawMode == CORNERS) {
    } else {
    }
    stroke(255);
    point(center.x, center.y);
    move();
    setBounds();
    popStyle();
  }

  void move() {
    if(kl.get(upKey) && tl.y > 0) {
      tl.sub(travel);
      br.sub(travel);
      center.sub(travel);
    }
    
    if(kl.get(downKey) && br.y < height) {
      tl.add(travel);
      br.add(travel);
      center.add(travel);
    }
  }
  
  boolean collision(Ball b) {
    return b.tl.x < br.x
    && tl.x < b.br.x
    && b.tl.y < br.y
    && tl.y < b.br.y;
  }

  void setBounds() {
    if (drawMode == RADIUS) {
      tl = PVector.sub(center, scale);
      br = PVector.add(center, scale);
    } else if (drawMode == CENTER) {
    } else if (drawMode == CORNER) {
    } else if (drawMode == CORNERS) {
    } else {
    }
  }
}