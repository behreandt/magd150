// The ability of one player to interrupt the movement of another
// by pressing a key is a huge problem for the game. So the first
// object to create is a key listener which makes simultaneous key
// presses possible.
KeyListener kl;

float paddle1Left, paddle1Top, paddle1Right, paddle1Bottom, 
  paddle1CenterX, paddle1CenterY, 

  paddle2Left, paddle2Top, paddle2Right, paddle2Bottom, 
  paddle2CenterX, paddle2CenterY, 

  screenPaddleMargin, 
  paddleHalfWidth, 
  paddleHalfHeight, 
  paddleSpeed, 

  ballCenterX, ballCenterY, ballRadius, 
  ballLeft, ballTop, ballRight, ballBottom, 
  ballXSpeed, ballYSpeed, ballXDirection = 1, ballYDirection = 1, 
  challengeScalar = 1.05;

color ball, paddle1, paddle2;

int paddle1Score, paddle2Score, paddle1UpKey = 87, paddle1DownKey = 83;

boolean paddle1Ball, paddle2Ball;

void setup() {
  surface.setTitle("Pong");
  pixelDensity(displayDensity());
  size(680, 420);
  //fullScreen();
  background(32);
  strokeWeight(1.5);
  stroke(255);
  ellipseMode(RADIUS);
  rectMode(CORNERS);

  kl = new KeyListener(UP, DOWN, paddle1UpKey, paddle1DownKey); 
  setPaddles();
  setBall();
}

void draw() {
  background(32);

  ballMove();
  ballCalculateBounds();
  ballShow();

  paddle1Show();
  paddle2Show();

  playerInput();

  paddle1Collision();
  paddle2Collision();

  showScore();
}

void showScore() {
  textAlign(CENTER, TOP);
  textSize(24);
  fill(255);
  text(paddle1Score, 26, 16);
  text(paddle2Score, width - 26, 16);
}

void setBall() {
  ballRadius = min(width, height) / 38.0;
  ballCenterX = random((width * 0.25) + ballRadius, 
    (width * 0.75) - ballRadius);
  ballCenterY = random((height * 0.25) + ballRadius, 
    (height * 0.75) - ballRadius);
  ballXSpeed = random(1) < 0.5 ? random(1, 4) : random(-4, -1);
  ballYSpeed = random(1) < 0.5 ? random(1, 4) : random(-4, -1);
  ball = color(230, 0, 0, 204);
}

void playerInput() {

  if (kl.get(paddle1UpKey) && paddle1Top > 0) {
    paddle1Top -= paddleSpeed;
    paddle1Bottom -= paddleSpeed;
    paddle1CenterY -= paddleSpeed;
  }

  if (kl.get(paddle1DownKey) && paddle1Bottom < height) {
    paddle1Top += paddleSpeed;
    paddle1Bottom += paddleSpeed;
    paddle1CenterY += paddleSpeed;
  }

  if (kl.get(UP) && paddle2Top > 0) {
    paddle2Top -= paddleSpeed;
    paddle2Bottom -= paddleSpeed;
    paddle2CenterY -= paddleSpeed;
  }

  if (kl.get(DOWN) && paddle2Bottom < height) {
    paddle2Top += paddleSpeed;
    paddle2Bottom += paddleSpeed;
    paddle2CenterY += paddleSpeed;
  }
}

void setPaddles() {
  screenPaddleMargin = width / 65.0; 
  paddleHalfWidth = width / 100.0; 
  paddleHalfHeight = height / 12.0;
  paddleSpeed = 5;
  setPaddle1();
  setPaddle2();
}

void setPaddle1() {
  paddle1Left = screenPaddleMargin;
  paddle1Right = paddle1Left + paddleHalfWidth * 2.0;
  paddle1CenterX = paddle1Left + paddleHalfWidth;
  paddle1CenterY = height / 2.0;
  paddle1Top = paddle1CenterY - paddleHalfHeight;
  paddle1Bottom = paddle1CenterY + paddleHalfHeight;
  paddle1 = color(0, 200, 0, 204);
}

void setPaddle2() {
  paddle2Right = width - screenPaddleMargin;
  paddle2Left = paddle2Right - paddleHalfWidth * 2.0;
  paddle2CenterX = paddle2Left + paddleHalfWidth;
  paddle2CenterY = height / 2.0;
  paddle2Top = paddle2CenterY - paddleHalfHeight;
  paddle2Bottom = paddle2CenterY + paddleHalfHeight;
  paddle2 = color(0, 0, 230, 204);
}

void paddle1Show() {
  fill(paddle1);
  rect(paddle1Left, paddle1Top, paddle1Right, paddle1Bottom, 3);
  point(paddle1CenterX, paddle1CenterY);
}

void paddle1Collision() {
  if (ballLeft < paddle1Right
    && paddle1Left < ballRight
    && ballTop < paddle1Bottom
    && paddle1Top < ballBottom) {
    ballXDirection *= -1;
    paddle1Ball = true;
    paddle2Ball = false;
    ball = color(0, 255, 255, 204);
    ballXSpeed *= challengeScalar;
  }
}

void paddle2Show() {
  fill(paddle2);
  rect(paddle2Left, paddle2Top, paddle2Right, paddle2Bottom, 3);
  point(paddle2CenterX, paddle2CenterY);
}

void paddle2Collision() {
  if (ballLeft < paddle2Right
    && paddle2Left < ballRight
    && ballTop < paddle2Bottom
    && paddle2Top < ballBottom) {
    ballXDirection *= -1;
    paddle1Ball = false;
    paddle2Ball = true;
    ball = color(255, 255, 0, 204);
    ballXSpeed *= challengeScalar;
  }
}

void ballShow() {
  stroke(255);
  fill(ball);
  ellipse(ballCenterX, ballCenterY, ballRadius, ballRadius);
  point(ballCenterX, ballCenterY);
}

void ballCalculateBounds() {
  ballLeft = ballCenterX - ballRadius;
  ballRight = ballCenterX + ballRadius;
  ballTop = ballCenterY - ballRadius;
  ballBottom = ballCenterY + ballRadius;
}

void ballMove() {
  ballCenterX += ballXSpeed * ballXDirection;
  ballCenterY += ballYSpeed * ballYDirection;

  if (ballCenterY <= ballRadius || ballCenterY >= height - ballRadius) {
    ballYDirection *= -1;
  }

  if (ballCenterX <= 0) {
    if (paddle2Ball) {
      paddle2Score++;
    }
    setBall();
    paddle1Ball = false;
    paddle2Ball = false;
  }

  if (ballCenterX >= width) {
    if (paddle1Ball) {
      paddle1Score++;
    }
    setBall();
    paddle1Ball = false;
    paddle2Ball = false;
  }
}

void keyPressed() {
  kl.keyPressed(keyCode);
}

void keyReleased() {
  kl.keyReleased(keyCode);
}