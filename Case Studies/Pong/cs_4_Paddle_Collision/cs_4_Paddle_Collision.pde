float paddle1Left, paddle1Top, paddle1Right, paddle1Bottom, 
  paddle1CenterX, paddle1CenterY, 

  paddle2Left, paddle2Top, paddle2Right, paddle2Bottom, 
  paddle2CenterX, paddle2CenterY, 

  screenPaddleMargin, 
  paddleHalfWidth, 
  paddleHalfHeight, 
  paddleSpeed, 

  ballCenterX, ballCenterY, ballRadius, 
  ballLeft, ballTop, ballRight, ballBottom, 
  ballXSpeed, ballYSpeed, ballXDirection = 1, ballYDirection = 1;

color ball, paddle1, paddle2;

char keyIgnoreCase, paddle1UpKey = 'w', paddle1DownKey = 's';

void setup() {
  surface.setTitle("Pong");
  pixelDensity(displayDensity());
  size(680, 420);
  background(32);
  strokeWeight(1.5);
  stroke(255);
  ellipseMode(RADIUS);
  rectMode(CORNERS);

  ballRadius = min(width, height) / 38.0;
  ballCenterX = random((width * 0.25) + ballRadius, 
    (width * 0.75) - ballRadius);
  ballCenterY = random((height * 0.25) + ballRadius, 
    (height * 0.75) - ballRadius);
  ball = color(230, 0, 0, 204);

  ballXSpeed = random(1) < 0.5 ? 3 : -3;
  ballYSpeed = random(1) < 0.5 ? 3 : -3;

  screenPaddleMargin = width / 65.0; 
  paddleHalfWidth = width / 100.0; 
  paddleHalfHeight = height / 12.0;
  paddleSpeed = 5;

  paddle1Left = screenPaddleMargin;
  paddle1Right = paddle1Left + paddleHalfWidth * 2.0;
  paddle1CenterX = paddle1Left + paddleHalfWidth;
  paddle1CenterY = height / 2.0;
  paddle1Top = paddle1CenterY - paddleHalfHeight;
  paddle1Bottom = paddle1CenterY + paddleHalfHeight;
  paddle1 = color(0, 200, 0, 204);

  paddle2Right = width - screenPaddleMargin;
  paddle2Left = paddle2Right - paddleHalfWidth * 2.0;
  paddle2CenterX = paddle2Left + paddleHalfWidth;
  paddle2CenterY = height / 2.0;
  paddle2Top = paddle2CenterY - paddleHalfHeight;
  paddle2Bottom = paddle2CenterY + paddleHalfHeight;
  paddle2 = color(0, 0, 230, 204);
}

void draw() {
  background(32);

  ballCenterX += ballXSpeed * ballXDirection;
  ballCenterY += ballYSpeed * ballYDirection;

  if (ballCenterY <= ballRadius || ballCenterY >= height - ballRadius) {
    ballYDirection *= -1;
  }

  stroke(127);
  ballLeft = ballCenterX - ballRadius;
  ballRight = ballCenterX + ballRadius;
  ballTop = ballCenterY - ballRadius;
  ballBottom = ballCenterY + ballRadius;
  line(ballLeft, ballTop, ballLeft, ballBottom);
  line(ballRight, ballTop, ballRight, ballBottom);
  line(ballLeft, ballTop, ballRight, ballTop);
  line(ballLeft, ballBottom, ballRight, ballBottom);

  stroke(255);
  fill(ball);
  ellipse(ballCenterX, ballCenterY, ballRadius, ballRadius);
  point(ballCenterX, ballCenterY);

  fill(paddle1);
  rect(paddle1Left, paddle1Top, paddle1Right, paddle1Bottom);
  point(paddle1CenterX, paddle1CenterY);

  fill(paddle2);
  rect(paddle2Left, paddle2Top, paddle2Right, paddle2Bottom);
  point(paddle2CenterX, paddle2CenterY);

  if (keyPressed) {
    if (key == CODED) {
      if (keyCode == UP && paddle2Top > 0) {
        paddle2Top -= paddleSpeed;
        paddle2Bottom -= paddleSpeed;
        paddle2CenterY -= paddleSpeed;
      }
      if (keyCode == DOWN && paddle2Bottom < height) {
        paddle2Top += paddleSpeed;
        paddle2Bottom += paddleSpeed;
        paddle2CenterY += paddleSpeed;
      }
    } else {
      keyIgnoreCase = Character.toLowerCase(key);
      if (keyIgnoreCase == paddle1UpKey && paddle1Top > 0) {
        paddle1Top -= paddleSpeed;
        paddle1Bottom -= paddleSpeed;
        paddle1CenterY -= paddleSpeed;
      }
      if (keyIgnoreCase == paddle1DownKey && paddle1Bottom < height) {
        paddle1Top += paddleSpeed;
        paddle1Bottom += paddleSpeed;
        paddle1CenterY += paddleSpeed;
      }
    }
  }

  // A noticeable limitation to the following two if blocks is that
  // if the top or bottom side of the paddle bumps the ball, then
  // the ball ricochets inside the paddle before being ejected out.

  // Paddle 1 collision.
  if (ballLeft < paddle1Right
    && paddle1Left < ballRight
    && ballTop < paddle1Bottom
    && paddle1Top < ballBottom) {
    ballXDirection *= -1;
  }

  // Paddle 2 collision.
  if (ballLeft < paddle2Right
    && paddle2Left < ballRight
    && ballTop < paddle2Bottom
    && paddle2Top < ballBottom) {
    ballXDirection *= -1;
  }
}