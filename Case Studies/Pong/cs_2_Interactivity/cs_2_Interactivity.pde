float paddle1Left, paddle1Top, paddle1Right, paddle1Bottom, 
  paddle1CenterX, paddle1CenterY, 

  paddle2Left, paddle2Top, paddle2Right, paddle2Bottom, 
  paddle2CenterX, paddle2CenterY, 

  screenPaddleMargin, 
  paddleHalfWidth, 
  paddleHalfHeight, 
  paddleSpeed, 

  ballCenterX, ballCenterY, ballRadius, 
  ballLeft, ballTop, ballRight, ballBottom;

color ball, paddle1, paddle2;

char keyIgnoreCase, paddle1UpKey = 'w', paddle1DownKey = 's';

void setup() {
  surface.setTitle("Pong");
  pixelDensity(displayDensity());
  size(680, 420);
  background(32);
  strokeWeight(1.5);
  stroke(255);
  ellipseMode(RADIUS);
  rectMode(CORNERS);

  ballRadius = min(width, height) / 40.0;
  ballCenterX = random((width * 0.25) + ballRadius, 
    (width * 0.75) - ballRadius);
  ballCenterY = random((height * 0.25) + ballRadius, 
    (height * 0.75) - ballRadius);
  ball = color(230, 0, 0, 204);

  screenPaddleMargin = width / 65.0; 
  paddleHalfWidth = width / 100.0; 
  paddleHalfHeight = height / 12.0;
  paddleSpeed = 5;

  paddle1Left = screenPaddleMargin;
  paddle1Right = paddle1Left + paddleHalfWidth * 2.0;
  paddle1CenterX = paddle1Left + paddleHalfWidth;
  paddle1CenterY = height / 2.0;
  paddle1Top = paddle1CenterY - paddleHalfHeight;
  paddle1Bottom = paddle1CenterY + paddleHalfHeight;
  paddle1 = color(0, 200, 0, 204);

  paddle2Right = width - screenPaddleMargin;
  paddle2Left = paddle2Right - paddleHalfWidth * 2.0;
  paddle2CenterX = paddle2Left + paddleHalfWidth;
  paddle2CenterY = height / 2.0;
  paddle2Top = paddle2CenterY - paddleHalfHeight;
  paddle2Bottom = paddle2CenterY + paddleHalfHeight;
  paddle2 = color(0, 0, 230, 204);
}

// A serious limitation of this game's interactivity is that if one
// player is moving, then the other player cannot. This means you
// could interrupt your opponent's attempt to get the ball.
void draw() {
  background(32);

  fill(ball);
  ellipse(ballCenterX, ballCenterY, ballRadius, ballRadius);
  point(ballCenterX, ballCenterY);

  fill(paddle1);
  rect(paddle1Left, paddle1Top, paddle1Right, paddle1Bottom);
  point(paddle1CenterX, paddle1CenterY);

  fill(paddle2);
  rect(paddle2Left, paddle2Top, paddle2Right, paddle2Bottom);
  point(paddle2CenterX, paddle2CenterY);

  // It is better to use if(keyPressed == true) { } in the draw
  // block than to define a void keyPressed() { } function to
  // listen for an event. This is because the latter leads to choppy
  // movement of the paddles.
  if (keyPressed) {
    // Special keys, such as the arrow, escape, delete and return
    // keys, are coded.
    if (key == CODED) {
      if (keyCode == UP && paddle2Top > 0) {
        paddle2Top -= paddleSpeed;
        paddle2Bottom -= paddleSpeed;
        paddle2CenterY -= paddleSpeed;
      }
      if (keyCode == DOWN && paddle2Bottom < height) {
        paddle2Top += paddleSpeed;
        paddle2Bottom += paddleSpeed;
        paddle2CenterY += paddleSpeed;
      }
    } else {
      // Caps lock is a problem for our game, since the character 'W'
      // does not equal 'w'. Character.toLowerCase(key) converts all
      // key presses to lower case before any checks happen.
      keyIgnoreCase = Character.toLowerCase(key);
      if (keyIgnoreCase == paddle1UpKey && paddle1Top > 0) {
        paddle1Top -= paddleSpeed;
        paddle1Bottom -= paddleSpeed;
        paddle1CenterY -= paddleSpeed;
      }
      if (keyIgnoreCase == paddle1DownKey && paddle1Bottom < height) {
        paddle1Top += paddleSpeed;
        paddle1Bottom += paddleSpeed;
        paddle1CenterY += paddleSpeed;
      }
    }
  }
}