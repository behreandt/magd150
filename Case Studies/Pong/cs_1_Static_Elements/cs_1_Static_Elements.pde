// Variables for the green paddle on left side of the screen.
float paddle1Left, paddle1Top, paddle1Right, paddle1Bottom, 
  paddle1CenterX, paddle1CenterY, 

  // Variables for the blue paddle on the right side of the screen.
  paddle2Left, paddle2Top, paddle2Right, paddle2Bottom, 
  paddle2CenterX, paddle2CenterY, 

  // Variables common to both paddles.
  screenPaddleMargin, 
  paddleHalfWidth, 
  paddleHalfHeight, 

  // Variables for the ball.
  ballCenterX, ballCenterY, ballRadius, 
  ballLeft, ballTop, ballRight, ballBottom;

color ball, paddle1, paddle2;

void setup() {
  surface.setTitle("Pong");
  pixelDensity(displayDensity());
  size(680, 420);
  background(32);
  strokeWeight(1.5);
  stroke(255);
  ellipseMode(RADIUS);
  rectMode(CORNERS);

  // Set ball properties.
  ballRadius = min(width, height) / 40.0;
  ballCenterX = random((width * 0.25) + ballRadius, 
    (width * 0.75) - ballRadius);
  ballCenterY = random((height * 0.25) + ballRadius, 
    (height * 0.75) - ballRadius);
  ball = color(230, 0, 0, 204);

  // Set universal paddle properties.
  screenPaddleMargin = width / 65.0; 
  paddleHalfWidth = width / 100.0; 
  paddleHalfHeight = height / 12.0; 

  // Set paddle1 properties (green paddle on left side of screen).
  paddle1Left = screenPaddleMargin;
  paddle1Right = paddle1Left + paddleHalfWidth * 2.0;
  paddle1CenterX = paddle1Left + paddleHalfWidth;
  paddle1CenterY = height / 2.0;
  paddle1Top = paddle1CenterY - paddleHalfHeight;
  paddle1Bottom = paddle1CenterY + paddleHalfHeight;
  paddle1 = color(0, 200, 0, 204);

  // Set paddle2 properties (blue paddle on right side of screen).
  paddle2Right = width - screenPaddleMargin;
  paddle2Left = paddle2Right - paddleHalfWidth * 2.0;
  paddle2CenterX = paddle2Left + paddleHalfWidth;
  paddle2CenterY = height / 2.0;
  paddle2Top = paddle2CenterY - paddleHalfHeight;
  paddle2Bottom = paddle2CenterY + paddleHalfHeight;
  paddle2 = color(0, 0, 230, 204);
}

void draw() {
  background(32);

  fill(ball);
  ellipse(ballCenterX, ballCenterY, ballRadius, ballRadius);
  point(ballCenterX, ballCenterY);

  fill(paddle1);
  rect(paddle1Left, paddle1Top, paddle1Right, paddle1Bottom);
  point(paddle1CenterX, paddle1CenterY);

  fill(paddle2);
  rect(paddle2Left, paddle2Top, paddle2Right, paddle2Bottom);
  point(paddle2CenterX, paddle2CenterY);
}