class SpaceShip {
  PVector pos, scl;

  float rot, 
    rotSpeed = 0.02,
    moveSpeed = 2.5,
    strkWgt = 0.04;

  color strk = color(255);

  // Constructor
  SpaceShip() {
    // Set the size of the spaceship.
    float shortEdge = min(width, height);
    scl = new PVector(shortEdge / 21.0, shortEdge / 21.0);

    // Randomize the ship's start position.
    pos = new PVector(random(scl.x, width - scl.x), 
      random(scl.y, height - scl.y));

    // Randomize the ship's start rotation.
    rot = random(TWO_PI);
  }

  void draw() {
    pushMatrix();
    move();
    screenWrap();
    translate(pos.x, pos.y);
    rotate(rot);
    scale(scl.x, scl.y);
    pushStyle();
    show();
    popStyle();
    popMatrix();
  }

  void move() {
    pos.x += cos(rot) * moveSpeed;
    pos.y += sin(rot) * moveSpeed;
    
    // Every 50 frames, rotate between -10 and 10 degrees.
    if(frameCount % 50 == 0) {
      float r = radians(10);
      rot += random(-r, r);
    }
  }

  void screenWrap() {
    // If the spaceship goes off the right edge,
    // set it to the left edge.
    if (pos.x > width) {
      pos.x = 0;
      // If the spaceship goes off the left edge,
      // set it to the right edge.
    } else if (pos.x < 0) {
      pos.x = width;
    }

    // If the spaceship goes off the bottom edge,
    // set it to the top edge.
    if (pos.y > height) {
      pos.y = 0;
      // If the spaceship goes off the top edge,
      // set it to the bottom edge.
    } else if (pos.y < 0) {
      pos.y = height;
    }
  }

  void show() {
    noFill();
    strokeWeight(strkWgt);
    stroke(strk);

    beginShape();
    vertex(0.5, 0); /* Cockpit */
    vertex(-0.333, 0.5); /* Right wing */
    vertex(-0.083, 0); /* Thruster */
    vertex(-0.333, -0.5); /* Left wing */
    endShape(CLOSE);
  }
}