SpaceShip s;

void setup() {
  surface.setTitle("Asteroids");
  pixelDensity(displayDensity());
  size(680, 420);
  background(64);
  s = new SpaceShip();
}

void draw() {
  background(0);
  s.draw();
}