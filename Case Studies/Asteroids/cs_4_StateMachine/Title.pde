class Title extends GameState {
  String header = "ASTEROIDS CLONE";
  String instruction = "PRESS ANY KEY TO BEGIN";

  void enter() {
  }

  void draw() {
    pushStyle();
    background(0);
    textAlign(CENTER, CENTER);
    textSize(42);
    fill(255);
    text(header, width * 0.5, height * 0.5);
    float h = textAscent() + textDescent();
    textSize(24);
    text(instruction, width * 0.5, height * 0.5 + h); 
    popStyle();
  }

  void exit() {
  }

  void keyPressed() {
  }
  
  void keyReleased() {
    machine.set("Playing");
  }
}