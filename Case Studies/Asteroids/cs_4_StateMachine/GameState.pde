abstract class GameState extends State {  

  void enter() {
    println(this + " entered.");
  }

  void exit() {
    println(this + " exited.");
  }

  void keyPressed() {
    println(keyCode + " " + key + " pressed.");
  }

  void keyReleased() {
    println(keyCode + " " + key + " released.");
  }
}