void setup() {
  surface.setTitle("Asteroids");
  pixelDensity(displayDensity());
  size(680, 420);
  background(64);
}

void draw() {
  background(0);
  pushMatrix();
  translate(width * 0.5, height * 0.5);
  rotate(frameCount / 90.0);
  scale(height * 0.5, height * 0.5);

  // Display the pivot point.
  strokeWeight(0.05);
  stroke(0, 255, 127);
  point(0, 0);

  // Display the bounding area.
  strokeWeight(0.025);
  stroke(255, 127, 0);
  noFill();
  ellipse(0, 0, 1, 1);

  pushStyle();
  noFill();
  strokeWeight(0.01);
  stroke(255);
  
  beginShape();
  vertex(0.5, 0); /* Cockpit */
  vertex(-0.333, 0.5); /* Right wing */
  vertex(-0.083, 0); /* Thruster */
  vertex(-0.333, -0.5); /* Left wing */
  endShape(CLOSE);
  popStyle();
  popMatrix();
}