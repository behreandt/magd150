class SpaceShip {
  PVector pos, scl;

  float rot = 0, 
    rotSpeed = 0.02, 
    initialRotSpeed = 0.02, 
    maxRotSpeed = 0.15, 
    rotAccel = 0.001, 

    moveSpeed = 2.5, 
    initialMoveSpeed = 2.5, 
    maxMoveSpeed = 5, 
    moveAccel = 0.01, 

    strkWgt = 0.04;

  color strk = color(255);

  KeyListener kl;

  SpaceShip() {
    // Set the size of the spaceship.
    float shortEdge = min(width, height);
    scl = new PVector(shortEdge / 21.0, shortEdge / 21.0);

    // Set the spaceship's start position to screen center.
    pos = new PVector(width * 0.5, height * 0.5);

    // Create a new key listener with the keys that will
    // control its movement.
    kl = new KeyListener(LEFT, RIGHT, UP, DOWN);
  }

  void draw() {
    pushMatrix();
    move();
    screenWrap();
    translate(pos.x, pos.y);
    rotate(rot);
    scale(scl.x, scl.y);
    pushStyle();
    show();
    popStyle();
    popMatrix();
  }

  void move() {
    if (kl.get(LEFT) || kl.get(RIGHT)) {

      // Rotate counter-clockwise.
      if (kl.get(LEFT)) {
        rot -= rotSpeed;
      }

      // Rotate clockwise.
      if (kl.get(RIGHT)) {
        rot += rotSpeed;
      }

      // Accelerate rotation the longer the key is pressed.
      if (rotSpeed <= maxRotSpeed) { 
        rotSpeed += rotAccel;
      }
    } else {
      // Revert to initial rotation speed on key release.
      rotSpeed = initialRotSpeed;
    }

    // Movement
    if (kl.get(UP) || kl.get(DOWN)) {
      if (kl.get(DOWN)) {
        pos.x -= cos(rot) * moveSpeed * 0.5;
        pos.y -= sin(rot) * moveSpeed * 0.5;
      }

      if (kl.get(UP)) {
        pos.x += cos(rot) * moveSpeed;
        pos.y += sin(rot) * moveSpeed;
      }

      // Accelerate.
      if (moveSpeed <= maxMoveSpeed) {
        moveSpeed += moveAccel;
      }
    } else {
      // Revert to initial speed on key release.
      moveSpeed = initialMoveSpeed;
    }
  }

  void screenWrap() {
    if (pos.x > width) {
      pos.x = 0;
    } else if (pos.x < 0) {
      pos.x = width;
    }

    if (pos.y > height) {
      pos.y = 0;
    } else if (pos.y < 0) {
      pos.y = height;
    }
  }

  void show() {
    noFill();
    strokeWeight(strkWgt);
    stroke(strk);

    beginShape();
    vertex(0.5, 0);
    vertex(-0.333, 0.5);
    vertex(-0.083, 0);
    vertex(-0.333, -0.5);
    endShape(CLOSE);
  }
}