static final Integer FIRE = 32;

class SpaceShip extends Destructible {
  PVector scl;

  int fireLimit = 100, 
    fireRate = 5;

  float rot = 0, 
    rotSpeed = 0.02, 
    initialRotSpeed = 0.02, 
    maxRotSpeed = 0.15, 
    rotAccel = 0.001, 

    moveSpeed = 2.5, 
    initialMoveSpeed = 2.5, 
    maxMoveSpeed = 5, 
    moveAccel = 0.01, 

    fireSpeed = 5.0, 

    strkWgt = 0.04;

  KeyListener kl;

  java.util.List<Laser> lasers = new ArrayList<Laser>();

  SpaceShip() {
    super();
    hpCurrent = hpMax = 100;
    float shortEdge = min(width, height);
    scl = new PVector(shortEdge / 20.0, shortEdge / 20.0);
    radius = shortEdge / 40.0;
    hue = 149;
    pos.set(width * 0.5, height * 0.5);
    kl = new KeyListener(UP, LEFT, DOWN, RIGHT, FIRE);
  }

  void diagnostic() {
    pushMatrix();
    pushStyle();
    colorMode(HSB, 359, 99, 99, 255);
    ellipseMode(RADIUS);
    stroke(hue, 99, 99, 255);
    strokeWeight(1);
    fill(hue, 99, 99, 0);
    ellipse(pos.x, pos.y, radius, radius);
    textAlign(CENTER, CENTER);
    textSize(10);
    fill(hue, 99, 99, 255);
    text(hpCurrent, pos.x, pos.y);
    popStyle();
    popMatrix();
  }

  void draw() {
    diagnostic();
    pushMatrix();
    move();
    screenWrap();
    shoot();
    translate(pos.x, pos.y);
    rotate(rot);
    scale(scl.x, scl.y);
    pushStyle();
    show();
    popStyle();
    popMatrix();
  }

  void move() {
    if (kl.get(LEFT) || kl.get(RIGHT)) {
      if (kl.get(LEFT)) {
        rot -= rotSpeed;
      }

      if (kl.get(RIGHT)) {
        rot += rotSpeed;
      }

      if (rotSpeed <= maxRotSpeed) { 
        rotSpeed += rotAccel;
      }
    } else {
      rotSpeed = initialRotSpeed;
    }

    if (kl.get(UP) || kl.get(DOWN)) {
      if (kl.get(DOWN)) {
        pos.x -= cos(rot) * moveSpeed * 0.5;
        pos.y -= sin(rot) * moveSpeed * 0.5;
      }

      if (kl.get(UP)) {
        pos.x += cos(rot) * moveSpeed;
        pos.y += sin(rot) * moveSpeed;
      }

      if (moveSpeed <= maxMoveSpeed) {
        moveSpeed += moveAccel;
      }
    } else {
      moveSpeed = initialMoveSpeed;
    }
  }

  void screenWrap() {
    if (pos.x > width) {
      pos.x = 0;
    } else if (pos.x < 0) {
      pos.x = width;
    }

    if (pos.y > height) {
      pos.y = 0;
    } else if (pos.y < 0) {
      pos.y = height;
    }
  }

  void shoot() {
    // If the fire key is pressed, add a new laser object to
    // the list of lasers. Additional limits are added to prevent
    // spamming. A modulo is used to delay the next laser; formerly,
    // only so many lasers could appear on the screen.
    if (kl.get(FIRE)
      && frameCount % fireRate == 0) {
      //&& lasers.size() < fireLimit) {
      lasers.add(new Laser(pos.x + scl.x * 0.5 * cos(rot), 
        pos.y + scl.y * 0.5 * sin(rot), 
        rot, fireSpeed));
    }

    // Draw the lasers in the list. If the laser goes off the
    // edge of the screen, then remove it.
    for (int i = lasers.size() - 1; i >= 0; --i) {
      Laser l = lasers.get(i);
      l.draw();
      if (l.pos.x > width || l.pos.y > height
        || l.pos.x < 0 || l.pos.y < 0) {
        lasers.remove(l);
      }
    }
  }

  void show() {
    noFill();
    strokeWeight(strkWgt);
    stroke(255);
    beginShape();
    vertex(0.5, 0);
    vertex(-0.333, 0.5);
    vertex(-0.083, 0);
    vertex(-0.333, -0.5);
    endShape(CLOSE);
  }
}