// An abstract class is not meant to be instantiated, but
// rather to provide a shared pool of qualities and behaviors
// for other classes which inherit from it.
abstract static class Destructible {
  PVector pos = new PVector();
  float radius = 1;
  int hpCurrent = 100, 
    hpMax = 100, 
    hue = 0, 
    id = 0;
    
  // Since this is a static class, it does not have access
  // to functions made available by Processing. So this is
  // an alternative way to get random numbers.
  java.util.Random rng = new java.util.Random();

  Destructible() {
    // Give the object a unique identifier in case it is
    // needed later to assist in collisions between
    // different kind of destructible objects.
    id = rng.nextInt(10000);
    hue = rng.nextInt(360);
    hpMax = rng.nextInt(25) + 10;
    hpCurrent = hpMax;
  }

  // Override to string to display the ID number.
  public String toString() {
    return String.format("%01d", id);
  }

  // An abstract function is one that child classes which
  // inherit from this class must define.
  abstract void diagnostic();

  // The static function to handle collision between two
  // destructible objects belongs not to any one objcet, but
  // to the blueprint or class itself, hence the term static.
  static boolean collide(Destructible a, Destructible b) {
    return dist(a.pos.x, a.pos.y, b.pos.x, b.pos.y)
      <= a.radius + b.radius;
  }
}