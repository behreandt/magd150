class Playing extends GameState {
  SpaceShip s;

  // Decide on number of asteroids to appear on screen.
  java.util.List<Asteroid> asteroids;
  int count = 15;

  void enter() {
    s = new SpaceShip();
    asteroids = new ArrayList<Asteroid>();
    for (int i = 0; i < count; ++i) {
      asteroids.add(new Asteroid());
    }
  }

  void draw() {
    background(0);
    
    // Draw the space ship.
    s.draw();
    
    // Draw all the asteroids, then check for collision
    // with both the spaceship and with lasers. If an
    // asteroid's hit points drops to 0, then remove it.
    for (int i = asteroids.size() - 1; i >= 0; i--) {
      Asteroid a = asteroids.get(i);
      a.draw();
      a.collide(s);
      a.collide(s.lasers);
      if (a.hpCurrent <= 0) {
        asteroids.remove(a);
      }
    }
    
    // Check to see if the state should be changed.
    // If the space ship's HP is 0, then go to defeat.
    // If there are no more asteroids, then go to victory.
    if (s.hpCurrent <= 0) {
      machine.set("Defeat");
    } else if(asteroids.size() <= 0) {
      machine.set("Victory");
    }
  }

  void exit() {
  }

  void keyPressed() {
    s.kl.keyPressed(keyCode);
  }

  void keyReleased() {
    s.kl.keyReleased(keyCode);
  }
}