class Laser extends Destructible {
  PVector speed;
  
  // The laser receives the direction it will travel
  // and its firespeed from the spaceship which fires it.
  Laser(float x, float y, float a, float fireSpeed) {
    super();
    hpCurrent = hpMax = 1;
    radius = 1;
    hue = 59;
    radius = 2;
    pos.set(x, y);
    speed = PVector.fromAngle(a);
    speed.setMag(fireSpeed);
  }

  void diagnostic() {
    pushMatrix();
    pushStyle();
    colorMode(HSB, 359, 99, 99);
    ellipseMode(RADIUS);
    stroke(hue, 99, 99, 255);
    strokeWeight(1);
    fill(hue, 99, 99, 0);
    ellipse(pos.x, pos.y, radius, radius);
    popStyle();
    popMatrix();
  }

  void draw() {
    diagnostic();
    pushMatrix();
    move();
    translate(pos.x, pos.y);
    pushStyle();
    show();
    popStyle();
    popMatrix();
  }

  void move() {
    pos.add(speed);
  }

  void show() {
    
  }
}