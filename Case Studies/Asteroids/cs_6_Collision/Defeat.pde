class Defeat extends GameState {
  String header = "GAME OVER";
  String instruction = "PRESS ANY KEY TO RESTART";

  void enter() {
  }

  void draw() {
    pushStyle();
    background(0);
    textAlign(CENTER, CENTER);
    textSize(42);
    fill(255);
    text(header, width * 0.5, height * 0.5);
    float h = textAscent() + textDescent();
    textSize(24);
    text(instruction, width * 0.5, height * 0.5 + h); 
    popStyle();
  }

  void exit() {
  }

  void keyPressed() {
  }
  
  void keyReleased() {
    machine.set("Title");
  }
}