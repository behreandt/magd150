// The class Destructible now handles features of a game
// object dealing with collision. An asteroid inherits
// and extends those features.
class Asteroid extends Destructible {
  PVector speed;

  Asteroid() {
    // The super constructor calls the constructor of
    // Asteroid's parent class, Destructible.
    super();
    
    // Give the asteroid a random radius, position on
    // the screen, direction and speed.
    radius = random(15, 20);
    pos.set(random(radius, width - radius), 
      random(radius, height - radius));
    speed = PVector.fromAngle(random(TWO_PI));
    speed.setMag(random(1, 4));
  }

  // If the asteroid collides with the player ship, then
  // both lose 5 hit points. Later, the 5 hit points should
  // be based on something like space ship shield strength.
  // The asteroid bumps the spaceship and reverses direction.
  void collide(SpaceShip s) {
    if (Destructible.collide(this, s)) {
      s.hpCurrent -= 5;
      hpCurrent -= 5;
      s.pos.add(speed);
      speed.mult(-1);
    }
  }

  // If the asteroid collides with a laser, then it loses
  // a hit point and the laser is removed from the screen.
  // Because the for-loop that iterates over all the lasers
  // potentially changes the list itself, it starts at the
  // end of the list rather than the beginning.
  void collide(java.util.List<Laser> lasers) {
    for (int i = lasers.size() - 1; i >= 0; --i) {
      Laser l = lasers.get(i);
      if (Destructible.collide(this, l)) {
        hpCurrent -= 1;
        lasers.remove(l);
      }
    }
  }

  void diagnostic() {
    pushMatrix();
    pushStyle();
    colorMode(HSB, 359, 99, 99);
    ellipseMode(RADIUS);
    stroke(hue, 99, 99, 255);
    strokeWeight(1);
    fill(hue, 99, 99, 0);
    ellipse(pos.x, pos.y, radius, radius);
    textAlign(CENTER, CENTER);
    textSize(10);
    fill(hue, 99, 99, 255);
    text(hpCurrent, pos.x, pos.y);
    popStyle();
    popMatrix();
  }

  void draw() {
    diagnostic();
    pushMatrix();
    move();
    translate(pos.x, pos.y);
    pushStyle();
    show();
    popStyle();
    popMatrix();
  }

  // If the asteroid hits the edge of the screen, then
  // it reverses direction.
  void move() {
    pos.add(speed);
    if (pos.x < radius || pos.x > width - radius) {
      speed.x *= -1;
    }
    if (pos.y < radius || pos.y > height - radius) {
      speed.y *= -1;
    }
  }

  void show() {
  }
}