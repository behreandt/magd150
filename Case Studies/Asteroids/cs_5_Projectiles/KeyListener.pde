class KeyListener {
  private java.util.Map<Integer, Boolean> pressed = 
    new java.util.HashMap<Integer, Boolean>();

  public KeyListener() {
    this(UP, /* 38 */
      DOWN, /* 40 */
      LEFT, /* 37 */
      RIGHT, /* 39 */
      SHIFT); /* 16 */
  }

  public KeyListener(Integer... admissibleKeys) {
    for (int i = 0, size = admissibleKeys.length; i < size; ++i) {
      pressed.put(admissibleKeys[i], false);
    }
  }

  public String toString() {
    StringBuilder s = new StringBuilder();
    for (Integer k : pressed.keySet()) {
      s.append(char(k) + " " + k + "\n");
    }
    return s.toString();
  }

  public Boolean get(Integer kc) {
    return pressed.containsKey(kc) ? pressed.get(kc) : false;
  }

  public void add(Integer... newKeys) {
    for (int i = 0, size = newKeys.length; i < size; ++i) {
      pressed.put(newKeys[i], false);
    }
  }

  public void keyPressed(Integer kc) {
    pressed.put(kc, true);
  }

  public void keyReleased(Integer kc) {
    pressed.put(kc, false);
  }
}