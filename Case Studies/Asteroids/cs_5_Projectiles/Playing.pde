class Playing extends GameState {
  SpaceShip s;

  void enter() {
    s = new SpaceShip();
  }

  void draw() {
    background(0);
    s.draw();
  }

  void exit() {
  }

  void keyPressed() {
    s.kl.keyPressed(keyCode);
  }

  void keyReleased() {
    s.kl.keyReleased(keyCode);
  }
}