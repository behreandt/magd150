class Laser {
  PVector pos;
  float rot, 
    moveSpeed = 3;

  Laser(float x, float y, float a) {
    pos = new PVector(x, y);
    rot = a;
  }

  void draw() {
    pushMatrix();
    move();
    translate(pos.x, pos.y);
    rotate(rot);
    //scale(scl.x, scl.y);
    pushStyle();
    show();
    popStyle();
    popMatrix();
  }

  void move() {
    pos.x += cos(rot) * moveSpeed;
    pos.y += sin(rot) * moveSpeed;
  }

  void show() {
    colorMode(HSB, 359, 99, 99);
    noFill();
    strokeWeight(3);
    stroke(random(0, 360), random(0, 99), 99);
    point(0, 0);
  }
}