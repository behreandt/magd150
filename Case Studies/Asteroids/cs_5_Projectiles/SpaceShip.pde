static final Integer FIRE = 32;

class SpaceShip {
  PVector pos, scl;

  int fireLimit = 30,
    hpCurrent,
    hpMax = 100;

  float rot = 0, 
    rotSpeed = 0.02, 
    initialRotSpeed = 0.02, 
    maxRotSpeed = 0.15, 
    rotAccel = 0.001, 

    moveSpeed = 2.5, 
    initialMoveSpeed = 2.5, 
    maxMoveSpeed = 5, 
    moveAccel = 0.01, 

    strkWgt = 0.04;

  color strk = color(255);

  KeyListener kl;

  java.util.List<Laser> lasers = new ArrayList<Laser>();

  SpaceShip() {
    float shortEdge = min(width, height);
    scl = new PVector(shortEdge / 21.0, shortEdge / 21.0);
    pos = new PVector(width * 0.5, height * 0.5);
    kl = new KeyListener(LEFT, RIGHT, UP, DOWN, FIRE);
    hpCurrent = hpMax;
  }

  void draw() {
    pushMatrix();
    move();
    screenWrap();
    shoot();
    translate(pos.x, pos.y);
    rotate(rot);
    scale(scl.x, scl.y);
    pushStyle();
    show();
    popStyle();
    popMatrix();
  }

  void move() {
    if (kl.get(LEFT) || kl.get(RIGHT)) {
      if (kl.get(LEFT)) {
        rot -= rotSpeed;
      }

      if (kl.get(RIGHT)) {
        rot += rotSpeed;
      }

      if (rotSpeed <= maxRotSpeed) { 
        rotSpeed += rotAccel;
      }
    } else {
      rotSpeed = initialRotSpeed;
    }

    if (kl.get(UP) || kl.get(DOWN)) {
      if (kl.get(DOWN)) {
        pos.x -= cos(rot) * moveSpeed * 0.5;
        pos.y -= sin(rot) * moveSpeed * 0.5;
      }

      if (kl.get(UP)) {
        pos.x += cos(rot) * moveSpeed;
        pos.y += sin(rot) * moveSpeed;
      }

      if (moveSpeed <= maxMoveSpeed) {
        moveSpeed += moveAccel;
      }
    } else {
      moveSpeed = initialMoveSpeed;
    }
  }

  void screenWrap() {
    if (pos.x > width) {
      pos.x = 0;
    } else if (pos.x < 0) {
      pos.x = width;
    }

    if (pos.y > height) {
      pos.y = 0;
    } else if (pos.y < 0) {
      pos.y = height;
    }
  }

  void shoot() {
    if (kl.get(FIRE) && lasers.size() < fireLimit) {
      lasers.add(new Laser(pos.x + scl.x * 0.5 * cos(rot), 
        pos.y + scl.y * 0.5 * sin(rot), 
        rot));
    }

    for (int i = lasers.size() - 1; i >= 0; --i) {
      Laser l = lasers.get(i);
      l.draw();
      if (l.pos.x > width || l.pos.y > height
        || l.pos.x < 0 || l.pos.y < 0) {
        lasers.remove(lasers.get(i));
      }
    }
  }

  void show() {
    noFill();
    strokeWeight(strkWgt);
    stroke(strk);

    beginShape();
    vertex(0.5, 0);
    vertex(-0.333, 0.5);
    vertex(-0.083, 0);
    vertex(-0.333, -0.5);
    endShape(CLOSE);
  }
}