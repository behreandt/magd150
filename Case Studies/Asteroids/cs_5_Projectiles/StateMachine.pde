// A State machine manages states, but a state
// could take many forms, including an animation
// or a game state (title screen, options, playing).
// To deal with multiple use-cases, we create
// a generic class. The < > brackets indicate that
// the state machine will work with any class that
// extends the State class.
class StateMachine<E extends State> {
  // The state machine will track previous and
  // current state.
  public E current;
  public E previous;

  // A Map uses key-value pairs to access each
  // state by its name.
  private java.util.Map<String, E> states =
    new java.util.HashMap<String, E>();

  // A constructor that accepts a variable number
  // of arguments, which are treated as an array.
  // This uses a for loop to go through the array
  // and place all the keys in the map/dictionary.
  StateMachine(E... sts) {
    int size = sts.length;
    for (int i = 0; i < size; ++i) {
      sts[i].machine = this;
      states.put(sts[i].toString(), sts[i]);
    }
    if (size > 0) {
      set(sts[0]);
    }
  }

  public String toString() {
    return states.values().toString();
  }

  public void add(E s) {
    
    // Each state added to this state machine is
    // told that this is the machine to which it belongs.
    s.machine = this;
    states.put(s.toString(), s);
  }

  public E remove(String request) {
    E removed = states.remove(request);
    if (removed != null) {
      removed.machine = null;
    }
    return removed;
  }

  public E get(String request) {
    return states.get(request);
  }

  public void set(String request) {
    
    // If the key yields a value, it will
    // return a non-null state.
    E s = states.get(request);
    if (s != null) {
      
      // The current is set to the previous
      // and the current receives the new state.
      previous = current;
      current = s;
      
      // If there was a previous state, then its
      // exit functionality is called.
      if (previous != null) {
        previous.exit();
      }
      current.enter();
    } else {
      println("The state " + request + " could not be found.");
    }
  }

  // A simplified version of the function above if the
  // state to set to current is already handy.
  public void set(E s) {
    previous = current;
    current = s;
    if (previous != null) {
      previous.exit();
    }
    current.enter();
  }
}