StateMachine<GameState> sm;

void setup() {
  surface.setTitle("Asteroids");
  pixelDensity(displayDensity());
  size(680, 420);
  background(64);
  sm = new StateMachine<GameState>(
    new Title(), 
    new Playing());
}

void draw() {
  background(0);
  sm.current.draw();
}

void keyPressed() {
  sm.current.keyPressed();
}

void keyReleased() {
  sm.current.keyReleased();
}