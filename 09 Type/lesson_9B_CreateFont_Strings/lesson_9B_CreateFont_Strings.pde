PFont sourceCodeBold;
PFont timesNewRoman;

String a = "abcdef";
String g = "ghiJKLMnopqr";

void setup() {
  surface.setResizable(true);
  surface.setTitle("Create Font, Strings");
  pixelDensity(displayDensity());
  size(420, 420);
  background(32);
  rectMode(CENTER);

  // If you want to see the list of available fonts on your
  // machine, you can request them with PFont.list, which
  // returns an array of Strings.
  printArray(PFont.list());

  // PFont variables will then be assigned the result of the createFont
  // function. createFont accepts either the name of the font as listed
  // from PFont.list or the index of the font. The second choice is a
  // problem, however, since some machines may have fewer or more fonts
  // than others, changing which font that index points to.
  sourceCodeBold = createFont("Source Code Pro Bold", 48, true);
  timesNewRoman = createFont(PFont.list()[352], 48, true);

  // Strings are secretly arrays of characters. They have a length;
  println("a's length: " + a.length());
  println("g's length: " + g.length());

  // You can access characters in a string using the .charAt() function.
  // Notice that the String counts its characters starting with 0 and
  // leading up to the length of the string - 1.
  println("a's middle char: " + a.charAt(a.length() / 2));
  println("g's 3rd char: " + g.charAt(3));

  // Strings can be strung together, or concatenated.
  println(a.concat(g));

  // Suppose you had a very long string, and you wanted to know
  // if it contains a keyword you're looking for. Use contains("")
  println("Does String a contain 'abc'? " + a.contains("abc"));

  // Suppose you wanted to remove sensitive information, or
  // clean up cusswords. The more sophisticated version of this
  // activity requires regular expressions, but this is the basic idea:
  println("Cusswordssosick".replace('s', '*'));

  textSize(48);
}

void draw() {
  background(32);

  textAlign(CENTER, CENTER);
  textFont(sourceCodeBold);
  fill(255, 0, 0);
  // toUpperCase() lets you capitalize all the characters in a string.
  text(a.toUpperCase(), width / 2.0, 54);

  textAlign(LEFT, BOTTOM);
  textFont(timesNewRoman);
  fill(255, 255, 127, 204);
  // ... while toLowerCase() does the opposite.
  text(g.toLowerCase(), 10, 120);
  stroke(0, 127, 255, 204);

  textAlign(CENTER, BASELINE);
  textSize(100);
  fill(255, 24);
  text(a.toUpperCase(), width / 2.0, height - 5);

  textFont(sourceCodeBold);
  textSize(50);
  fill(255, 36);
  text(g.toLowerCase(), width / 2.0, height - 60);
}