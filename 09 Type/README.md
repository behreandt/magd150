# Assignment 9
## Type
### 100 points

![Type](preview.png)

### References

* Daniel Shiffman, [_Learning Processing_](http://learningprocessing.com/examples/) Chapter 17
* Shiffman, [Video: Strings and Drawing Text](https://youtu.be/NLzne4XaR3M?list=PLRqwX-V7Uu6Y4F21kqaFLk6oGW2I5o7FY)
* [Libraries: PDF Export](https://processing.org/reference/libraries/pdf/index.html)
* Casey Reas and Ben Fry, [Typography](https://processing.org/tutorials/typography/)
* Casey Reas, [Print](https://processing.org/tutorials/print/)
* [Art of the Title](http://www.artofthetitle.com/)

### Instructions

1. Adhere to the theme to be determined in lab. (10 pts.)

2. [Create](https://processing.org/reference/createFont_.html) / [load](https://processing.org/reference/loadFont_.html) and [display](https://processing.org/reference/textFont_.html) at least 2 fonts. (10 pts.)

3. Change the [alignment](https://processing.org/reference/textAlign_.html) of text on the screen. (10 pts.)

4. Either animate the text or make it react to user input. (10 pts.)

5. [Load strings](https://processing.org/reference/loadStrings_.html) from an external file. (15 pts.)

6. Save the composition as a .pdf file. (15 pts.)

7. Ensure that the code contains at least 4 single- or multi-line comments, written in complete sentences, that explain what the code is doing at key steps. (10 pts.)

8. Ensure that the sketch runs without errors. The main tab of the sketch should be the only tab with global-scope setup and draw functions. (10 pts.)

9. Ensure that the main tab of the sketch shares the same name as the sketch folder. Name the sketch folder and main tab according to the following convention: s17_magd150_lab09_yourlastname. Compress the sketch folder into a .zip file and upload to D2L. (10 pts.)

__Any code or media within a submission are presumed to be the work of the student(s) unless otherwise stated. When media or code from outside sources are used, a link to the source and notice of attribution at the top of the main sketch in a comment should be provided. Media and code used are expected to be from the intellectual commons (i.e., not copyrighted or registered as a trademark) and to be altered substantially by the student(s). On first incident, students whose work does not meet this standard will have one week to resubmit the assignment for half credit. On second incident, students will be referred to Student Conduct Programs and will fail the course.__
