// An array of Strings is used to hold the information
// we are about to load from a text file.
String[] lines;
String fontFile = "Garamond-16.vlw";
String sourceFile = "loremIpsum.txt";
PFont garamond;
int count, textSize, 
  lineSpacing, margin;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Importing Txt File");
  pixelDensity(displayDensity());
  size(420, 420);
  background(64);

  // We can specify the file name to give to load strings.
  lines = loadStrings(sourceFile);
  count = lines.length;
  printArray(lines);

  garamond = loadFont(fontFile);
  textFont(garamond);

  // Since it's preferable for the display font size to
  // match the .vlw source font size, we can use the file
  // naming convention to get the appropriate size.

  String extractFontSize = fontFile.substring(
    fontFile.lastIndexOf("-") + 1, 
    fontFile.indexOf(".vlw"));

  // In case the extractFontSize variable cannot be
  // safely converted into an integer value, the size
  // 14 will be used as a backup with this ternary operator.

  textSize = int(extractFontSize) > 0
    ? int(extractFontSize)
    : 14;
  textSize(textSize);
  lineSpacing = 2;
  margin = 10;

  textAlign(LEFT, TOP);
}

void draw() {
  background(32);
  fill(255);

  // A for-loop can be used to display a number of lines.
  for (int i = 0; i < count; ++i) {
    text(lines[i], margin, 
      margin + (textSize + lineSpacing) * i);
  }
}