// A String can be created from an array of characters.
char[] name;
boolean nameEntryCompleted;
User user;

void setup() {
  pixelDensity(displayDensity());
  surface.setResizable(true);
  surface.setTitle("Text Entry");
  size(420, 420);
  background(64);
  textSize(16);

  name = new char[0];
  user = new User();
}

void draw() {
  background(32);
  requestName();
}

void keyTyped() {
  enterName(user);
}

void requestName() {

  // In a more sophisticated program, a state machine
  // would be better to handle these states than a boolean.
  if (!nameEntryCompleted) {
    fill(255);
    text("What is your name?", 10, height/2);
    fill(0, 127, 255);
    stroke(127, 204, 255);
    line(10, height / 2.0 + 24, 150, height / 2.0 + 24);
    text(new String(name), 10, height / 2.0 + 20);
  } else {
    displayNameAndSign();
  }
}

void displayNameAndSign() {
  fill(255, 255, 0);
  text(user.toString(), 10, height/2);
}

void enterName(User user) {
  if (!nameEntryCompleted) {

    // If the user hits a carriage return...
    if (key == '\n' || key == '\r') {

      // Convert the character array to a string and assign
      // it to the user's name variable.
      user.name = new String(name);
      user.calculateZodiac(month(), day());
      nameEntryCompleted = true;

      // If the user hits the delete or backspace key...
    } else if (key == '\b' || int(key) == 127) {
      if (name.length > 0) {

        // Shorten the array name, provided it's greater than 0.
        name = shorten(name);
      }
    } else {

      // Take the name array, append the key entered
      // to the end and assign it to the array.
      name = append(name, key);
    }
  }
}