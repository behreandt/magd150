String[] zodiac = {
  "Capricorn", /* December month 12 % 12 == 0 */
  "Acquarius", /* 1 */ 
  "Pisces", /* 2 */
  "Aries", /* 3 */
  "Taurus", /* 4 */
  "Gemini", /* 5 */
  "Cancer", /* 6 */
  "Leo", /* 7 */
  "Virgo", /* 8 */
  "Libra", /* 9 */
  "Scorpio", /* 10 */
 "Sagittarius" /* 11 */
};

String[] months = {
  "December", "January", "February", 
  "March", "April", "May", 
  "June", "July", "August", 
  "September", "October", "November" };

class User {
  String name;
  int birthMonth;
  int birthDay;
  String sign; 

  public String toString() {
    return name
      + "\r\n" + months[birthMonth]
      + " " + birthDay
      + "\r\n" + sign;
  }

  String calculateZodiac(int month, int day) {
    birthMonth = month;
    birthDay = day;

    // Zodiac signs are based on the 20th day of
    // the month, so if a birthday falls on or
    // before the 20th, then the earlier zodiac is
    // used; otherwise the later zodiac is used.

    if (day <= 20) {
      sign = zodiac[(month - 1) % 12];
    } else {
      sign = zodiac[month % 12];
    }
    return sign;
  }
}