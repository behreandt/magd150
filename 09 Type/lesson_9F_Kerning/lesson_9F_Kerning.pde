/* The font Trajan was downloaded from
 http://fontsgeek.com/fonts/Trajan-Pro-Regular/download */

/* The text of Herman Melville's Moby Dick was accessed at
https://www.gutenberg.org/files/2701/2701-h/2701-h.htm */

PFont trajan;
char[][] text;
float x, y, 
  halfWidth, halfHeight, 
  n, nIncr1, nIncr2;
color start, stop;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Text Kerning, Manual Text Leading");
  pixelDensity(displayDensity());
  size(840, 420);
  background(64);

  colorMode(HSB, 359, 99, 99);

  textAlign(LEFT, TOP);
  textSize(24);

  // Load and assign the font.
  trajan = loadFont("Trajan-24.vlw");
  textFont(trajan);

  // Create a new string from a .txt file where new lines
  // are separated by a carriage return.
  text = loadChars("mobydick.txt");

  // The change in noise per iteration in the nested for-loops.
  nIncr1 = 0.02;
  nIncr2 = 0.008;

  // Colors which form a color gradient.
  start = color(149, 59, 99);
  stop = color(209, 59, 99);
}

void draw() {
  background(32);
  
  halfWidth = width * 0.5;
  halfHeight = height * 0.5;
  
  x = map(mouseX, 0, width, 1, 32);
  y = map(mouseY, 0, height, 1, 100);
  
  float yoff = 0;
  for (int i = 0, size1 = text.length; i < size1; ++i) {
    float xoff = 0;
    for (int j = 0, size2 = text[i].length; j < size2; ++j) {
      n = noise(xoff, yoff, frameCount * nIncr2);
      fill(lerpColor(start, stop, i / (float)size1));
      //fill(map(n, 0, 1, 0, 360), 99, 99);
      text(text[i][j], x * j, y * i * n);
      xoff += nIncr1;
    }
    yoff += nIncr1;
  }
}

char[][] loadChars(String filePath) {
  String[] lines = loadStrings(filePath);
  int size = lines.length;
  char[][] chars = new char[size][];

  for (int i = 0; i < size; ++i) {
    chars[i] = lines[i].toCharArray();
  }

  return chars;
}