// The PFont object holds all the information about the font in memory.
PFont poorRichard;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Load Font");
  pixelDensity(displayDensity());
  size(640, 420);
  background(32);
  noStroke();

  // loadFont looks for a .vlw file created by going to Tools > Create
  // Font... in Processing's menu. The .vlw file is placed in the
  // sketche's data folder.
  poorRichard = loadFont("PoorRichard-Regular-48.vlw");

  // The alignment of the text relative to its registration or pivot.
  // The first parameter is horizontal alignment, the second is 
  // vertical alignment. Options include:
  // LEFT / CENTER / RIGHT
  // TOP / CENTER / BOTTOM / BASELINE
  textAlign(LEFT, CENTER);

  // The font face can now be fed the PFont variable which holds
  // the font.
  textFont(poorRichard);
  
  // The size of the font can be set independently of the size at
  // which the source .vlw file was created. How Processing treats
  // the font when there is a mismatch between the size at creation
  // and the size specified below will vary.
  textSize(48);
}

void draw() {
  fill(32, 10);
  rect(0, 0, width, height);

  // Like images and geometric primitives, text can be rotated.
  pushMatrix();
  translate(width / 2.0, height / 2.0);
  rotate(frameCount / 90.0);
  fill(127, 255, 255, 127);
  text("The quick brown\nfox jumps over\nthe lazy dog.", 0, 0);
  popMatrix();

  pushMatrix();
  translate(width / 2.0, height / 2.0);
  shearX(frameCount / 90.0);
  fill(255, 255, 127, 127);
  text("The quick brown\nfox jumps over\nthe lazy dog.", 0, 0);
  popMatrix();

  pushMatrix();
  translate(width / 2.0, height / 2.0);
  shearY(frameCount / 90.0);
  fill(255, 127, 255, 127);
  text("The quick brown\nfox jumps over\nthe lazy dog.", 0, 0);
  popMatrix();
}