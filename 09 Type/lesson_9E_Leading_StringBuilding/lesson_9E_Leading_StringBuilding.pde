/* The font Trajan was downloaded from
 http://fontsgeek.com/fonts/Trajan-Pro-Regular/download */

PFont trajan;
String text;

void setup() {
  surface.setResizable(true);
  surface.setTitle("Text Leading & String Building");
  pixelDensity(displayDensity());
  size(840, 420);
  background(64);

  textAlign(LEFT, TOP);
  textSize(24);
  
  // Leading is the amount of space between each line of a text,
  // which is represented as a '\n' in Strings.
  textLeading(24);

  // Load and assign the font.
  trajan = loadFont("Trajan-24.vlw");
  textFont(trajan);

  // Create a new string from a .txt file where new lines
  // are separated by a carriage return.
  text = loadString("loremIpsum.txt", "\n");
}

void draw() {
  background(32);
  textLeading(map(mouseY, 0, height, 1, 100));
  text(text, 0, 0);
}

String loadString(String filePath, String delimiter) {
  
  // The built-in loadStrings function returns an array of Strings,
  // which can be cumbersome. It may be preferable to work with one
  // large String instead.
  String[] lines = loadStrings(filePath);
  
  // Concatenating a new String out of two smaller strings, e.g.,
  // "sta" + "rt", is inefficient. If you want to convert an array
  // of Strings to one large String, it is better to create an object
  // called a StringBuilder. This appends lines and then is converted
  // to a string on return.
  StringBuilder sbuilder = new StringBuilder();
  for (int i = 0, size = lines.length; i < size; ++i) {
    sbuilder.append(lines[i]);
    sbuilder.append(delimiter);
  }
  return sbuilder.toString();
}