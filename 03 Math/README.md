# Assignment 3
## Math
### 100 points

![Math](preview.png)

### References

* Daniel Shiffman, [_Learning Processing_](http://learningprocessing.com/examples/) Chapters 3 - 4
* Shiffman, [Video: Variables](https://youtu.be/B-ycSR3ntik?list=PLRqwX-V7Uu6aFNOgoIMSbSYOkKNTo89uf)

### Instructions

1. Adhere to the theme to be determined in lab. (10 pts.)
    * Wednesday: Screensaver
    * Monday: Bubbles

2. Use a [Floating Point Decimal](https://processing.org/reference/float.html) at least twice in the sketch. (10 pts.)

3. Use the [Print Line](https://processing.org/reference/println_.html) function at least twice to display a key piece of information in the console. (10 pts.)

4. Use at least four of the following Processing key words: (20 pts.)
    * [Mouse X](https://processing.org/reference/mouseX.html)
    * [Mouse Y](https://processing.org/reference/mouseY.html)
    * [Previous Mouse X](https://processing.org/reference/pmouseX.html)
    * [Previous Mouse Y](https://processing.org/reference/pmouseY.html)
    * [Frame Count](https://processing.org/reference/frameCount.html)
    * [Width](https://processing.org/reference/width.html)
    * [Height](https://processing.org/reference/height.html)

5. Use at least four of the following math operators or comparisons: (10 pts.)
    * [Multiplication ( * )](https://processing.org/reference/multiply.html)
    * [Division ( / )](https://processing.org/reference/divide.html)
    * [Addition ( + )](https://processing.org/reference/addition.html)
    * [Subtraction ( - )](https://processing.org/reference/minus.html)
    * [Modulo ( % )](https://processing.org/reference/modulo.html)
    * [Equal to ( == )](https://processing.org/reference/equality.html)
    * [Greater Than ( > )](https://processing.org/reference/greaterthan.html)
    * [Less Than ( < )](https://processing.org/reference/lessthan.html)

6. Use at least three of the functions listed in the Processing reference under Calculation or Trigonometry. Some useful entries: (20 pts.)
    * [Maximum](https://processing.org/reference/max_.html)
    * [Minimum](https://processing.org/reference/min_.html)
    * [Constrain](https://processing.org/reference/constrain_.html)
    * [Map](https://processing.org/reference/map_.html)
    * [Distance](https://processing.org/reference/dist_.html)
    * [Linear Interpolation](https://processing.org/reference/lerp_.html)

7. Ensure that the sketch runs without errors. The main tab of the sketch should be the only tab with global-scope setup and draw functions. (10 pts.)

8. Ensure that the main tab of the sketch shares the same name as the sketch folder. Name the sketch folder and main tab according to the following convention: s17_magd150_lab03_yourlastname. Compress the sketch folder into a .zip file and upload to D2L. (10 pts.)

__Any code or media within a submission are presumed to be the work of the student(s) unless otherwise stated. When media or code from outside sources are used, a link to the source and notice of attribution at the top of the main sketch in a comment should be provided. Media and code used are expected to be from the intellectual commons (i.e., not copyrighted or registered as a trademark) and to be altered substantially by the student(s). On first incident, students whose work does not meet this standard will have one week to resubmit the assignment for half credit. On second incident, students will be referred to Student Conduct Programs and will fail the course.__
