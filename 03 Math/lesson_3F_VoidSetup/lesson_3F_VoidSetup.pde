// So far we've been programming in a static, single-state manner
// but in order to create animated, interactive software, we need
// variables to track changes in state.

// To turn our sketches into active programs, we need to add two lines:
// void setup() { }
// void draw() { }
// Setup sets the initial conditions of your sketch.
// Draw updates the sketch every frame of animation.
// These are actually function definitions reserved by processing.
// We define what happens inside these functions by entering statements
// between the curly braces { }, and Processing calls them for us.
// Some functions need to return information when they're done,
// but these return no information, so they return void.

// Global scoped variables, which both setup and draw know about
boolean flipFlopper; // default value is false

int red; // default value is 0
int green;
int blue;

float deltaMouseX; // default value is 0.0
float deltaMouseY;

float w = 30;
float h = 30;

void setup() {
  size(420, 420);
  //fullScreen();
  surface.setResizable(true);
  background(32);
}

void draw() {
  // If we don't include this in draw, then nothing will be erased.
  // comment it out/in to see what happens.
  // background(32);

  // This increases the integers which represent a color channel
  // each frame. However, values over 255 are meaningless, so the
  // modulo % operator resets them to 0 if they get too big.
  red = (red + 1) % 256;
  green = (green + 5) % 256;
  blue = (blue + 2) % 256;
  fill(red, green, blue);

  // Width and height of the ellipse increase in size until they
  // reach a third the height of the window, then reset to 0
  w = (w + 1) % (height / 3.5);
  h = (h + 1) % (height / 3.5);

  // mouseX and mouseY are special variables which track
  // the mouse position.
  ellipse(mouseX, mouseY, w, h);
  println("(" + mouseX + ", " + mouseY + ")");

  // pmouseX and pmouseY are the previous mouse position.
  // By subtracting pmouse from mouse you can get the change
  // in mouse position between frames.
  deltaMouseX = mouseX - pmouseX;
  deltaMouseY = mouseY - pmouseY;

  println("delta: (" + deltaMouseX + ", " + deltaMouseY + ")");

  flipFlopper = !flipFlopper;
  println("flippFlopper = " + flipFlopper);
}