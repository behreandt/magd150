// You can think of Processing like a graphing calculator.
// As such, it can perform mathematical calculations.

// You can use the console (the black screen on the bottom of
// the window) to view the results of such calculations.

println(2 + 2); // 4
println(5 - 3); // 2
println(3 * 3); // 9
println(5 / 4); // Why does this equal 1 and not 1.25?

// Processing defaults to using integer division,
// meaning any remainder is dropped and the result
// is truncated to a whole number. If you want
// decimal division, add a '.0' to your divisor.
println(5 / 4.0);

// What if you want the remainder that was dropped? Use the
// modulo operator.
println(8 / 3); // 2
println(8 % 3); // 2, so 8 / 3 = 2 + 2 / 3

// This is useful if you want to determine whether a number
// is even or odd.
println(4 % 2); // 0, therefore even.
println(5 % 2); // 1, therefore odd.

// You can also print Strings of characters to the console.
println("Hello world.");

// This is helpful when labeling the results of your
// calculations, but be careful: the plus sign here
// is used for concatenating Strings of characters
// ogether, not for adding numbers together.
println("The result is " + 4 % 2 + ", so 4 is even.");
println("The result of " + 4 + 4 + " is 8.");

// So you should use parentheses to group operations
println("The result of " + 4 + " + " + 4 + " is " + (4 + 4) + ".");

// PI is also fair game.
println("PI == " + PI); // 3.1415927
println("2 + TWO_PI == " + (2 + TWO_PI)); // 8.283186
println("10 - QUARTER_PI == " + (10 - QUARTER_PI)); // 9.2146015
println("3 * HALF_PI == " + (3 * HALF_PI)); // 4.712389
println("TAU == " + TAU);

// Keep Order of Operations (PEMDAS) in mind:
// Parentheses,
// Exponents,
// Multiplication,
// Division,
// Addition
// Subtraction.
println(6 + 2 * 5);
println((6 + 2) * 5);