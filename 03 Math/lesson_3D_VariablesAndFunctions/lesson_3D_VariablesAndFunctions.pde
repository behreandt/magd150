size(420, 420);
surface.setResizable(true);
// Adjusts the Processing sketch for high-density screens.
pixelDensity(displayDensity());
background(32);

// We can now use variables to remember math operations.
float x1 = width * 0.75;
float y1 = height * 0.25;

// Suppose we want our shape to scale according to the shorter of two
// edges of the window. We can use the min function to find the lesser
// of the two values.
float h1 = min(width, height) * 0.5;

// If we want the shape's width to equal its height, we can
// assign the value of one variable to another.
float w1 = h1;

strokeWeight(25);
stroke(#D64343);
fill(#436BD6);
ellipse(x1, y1, w1, h1);

float x2 = width * 0.333;
float y2 = height * 0.666;
float w2 = w1 * 1.25;
float h2 = constrain(w2, 0, height);

strokeWeight(15);
stroke(#4AAA3E);
fill(#923EAA);
ellipse(x2, y2, w2, h2);