// Variables store information while the sketch is running.
// Information is assigned to variables from RIGHT to LEFT,
// where the equals sign '=' is the assignment operator.

// Different kinds of variables store different kinds of information.
// These different kinds are called data types. Java, the language
// in which Processing is based, is strictly typed, meaning you must
// assign information of a certain type to an appropriate container.
// There are many variable types to explore: short, long, double...
// but most operations in Processing will rely on
// boolean: True or False, named after logician George Boole;
// byte: 8 bits (0 or 1 in binary), 2 ^ 8 means a range of 0 - 255.
// int: integer, a whole number with no decimal place.
// float: a floating-point real number, which can hold decimal places.
// char: a single character, such as 'a', 'B' or '?'.
// color: a data type unique to processing which holds color info.
// String: an object which contains a string of characters to form a phrase.

// Before you can use a variable you must declare it,
// i.e., you must set aside memory to remember it.
// When you declare it, you say what kind of data type it will be
// and give it a name.
color myNameForThisVariable;
// You don't have to assign a value to the variable right away...
// but if you do, you initialize the variable. If you want to,
// you can assign or re-assign a value later.
myNameForThisVariable = #AABBCC;

// You can't have two variables with the same name.
// color myNameForThisVariable = 5;
// you can't have variables that start with a number.
// color 1dafdf;
// don't use keywords or other reserved syntax like ';', '(', etc.
// color boolean();

// boolean, which can be true or false.
boolean a = true;

// byte, 8 bits.
// Useful for fine-grain manipulation of data.
byte b = 12;

println("Minimum possible value of byte: " + Byte.MIN_VALUE);
println("Maximum possible value of byte: " + Byte.MAX_VALUE);

// int, short for integer, a variable to hold whole numbers.
int c = 5;

println("Minimum possible value of int: " + Integer.MIN_VALUE);
println("Maximum possible value of int: " + Integer.MAX_VALUE);

// float, a variable to hold real numbers.
float d = 10f;

println("Minimum possible value of float: " + Float.MIN_VALUE);
println("Maximum possible value of float: " + Float.MAX_VALUE);

// color stores multiple channels of information in one place.
color red = #FF0000;
color blue = color(0,0,255);
color translucentGreen = 0xCC00FF00;

// Trying to print info about a color directly won't be very helpful
println("Red (not helpful): " + red);
// You need to convert it to hex code.
// The first two digits denote the alpha value of the color
println("Blue: " + hex(blue));
println("TranslucentGreen: " + hex(translucentGreen));

// char, short for character, holds single characters.
char e = 'e';

// Special char codes will be interpreted as carriage returns and tabulations.
char tab = '\t';
char carriageReturn = '\r';
char newLine = '\n';

// Strings are NOT primitive variables, as indicated by the capitalized
// first letter (they're actually objects, which we'll get to later).
// However, Strings of characters are so often needed that we'll
// include them here.
String hal = "Not for the rain in Spain, ";
String ibm = "not for all the maple syrup in Canada!";

println(hal);
println(ibm);

// You can also try to convert one type of data to another

// binary(value); converts int, byte, char, or color.
println("Red in binary: " + binary(red));

// boolean(value) converts Strings other than "true" to false.
// Useful when converting data from human-readable documents loaded
// from external sources to Processing-readable information.
println("\"true\" is " + boolean("true")); 

// byte(value) converts any primitive data type (those above aside from String).
println("A as a byte: " + byte('A'));

// char(value) converts to a character value, 'A' is 65, 'Z' is 90.
// This is particularly useful when reinterpreting keyboard input, such
// as the standard WASD used for movement.
println("87 as a char: " + char(87));
println("65 as a char: " + char(65));
println("83 as a char: " + char(83));
println("68 as a char: " + char(68));

// float(value) attempts to convert a String to a number.
// Caution: non-numeric characters, like '$' will lead to NaN (Not a Number).
println(float("$5.45"));
println(float("5.45E10") / 5);

// str(value) converts the value to a String representation,
// for example, this is not 156, but "150" concatenated with "6".
println(str(150) + 6); 