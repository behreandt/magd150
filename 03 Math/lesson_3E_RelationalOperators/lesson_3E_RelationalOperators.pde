// All relational operators - such as less than, greater than,
// equal to, etc., resolve to a Boolean value (true or false).

// '=' is used for assignment, so we use '==' for equals
// we're used to from regular math.
int a = 5;
int b = 6;

println(a == b); // Is it true that 5 equals 6? false.
println(a != b); // Is it true that 5 does not equal 6? true.
println(a > b); // Is it true that 5 is greater than 6? false.
println(a < b); // Is it true that 5 is less than 6? true.
println(a >= b); // Is it true that 5 is greater than or equal to 6? false.
println(a <= b); // Is it true that 5 is less than or equal to 6? true.

// You can also preface a Boolean statement with an
// exclamation point '!' to negate it.
boolean evaluateAB = a == b;
boolean negateGreaterThan = !(a > b); // !(a > b) == (a <= b)
println(evaluateAB); // false
println(negateGreaterThan); // true

// This is helpful when treating booleans as switches to flip based
// on the values of other variables.
boolean onSwitch = true;
// Suppose you don't know what values a and b will have, and so you
// don't know whether a <= b will be true or false.
onSwitch = a <= b;
println("onSwitch: " + onSwitch);
// Whatever onSwitch was on account of a and b, it will be switched
// to its opposite.
onSwitch = !onSwitch;
println("onSwitch: " + onSwitch);

// You can also combine checking conditions for truth or falsehood
// using and '&&' and inclusive-or '||'.
boolean beans = true;
boolean cornbread = false;
println(beans && cornbread);
beans = false;
println(beans && cornbread);
beans = true;
cornbread = true;
// For '&&' to be true, both sides must be true.
println(beans && cornbread);

// Inclusive-or means "I will either play golf tomorrow
// or I will mow the lawn tomorrow." So long as I do one or
// the other tomorrow, I will have spoken the truth.
boolean mowedTheLawn = true;
boolean playedGolf = false;
boolean tomorrow = mowedTheLawn || playedGolf;
println("tomorrow: " + tomorrow);

// Inclusive-or does not mean "You can have either Barbecue sauce
// or Honey-mustard sauce." Why not? Because it is implied that you
// cannot have either-or, but not both Barbecue and Honey-mustard,
// at least not without an extra charge.
boolean honeyMustard = true;
boolean barbecueSauce = true;
boolean incorrectInterpretation = honeyMustard || barbecueSauce;
boolean correctInterpretation = (honeyMustard || barbecueSauce) && !(honeyMustard && barbecueSauce);
println("condiment choice incorrect interpretation : " + incorrectInterpretation);
println("condiment choice correct interpretation: " + correctInterpretation);