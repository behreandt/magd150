float start;
float stop = 30000;

void setup() {
  // For high density displays, this line may be
  // necessary to get a smooth image.
  pixelDensity(displayDensity());
  
  // To make the window resizable, you must insert
  // the following:
  surface.setResizable(true);
  
  // You can also change the title of the sketch window.s
  surface.setTitle("Demoing Title Change!");

  size(680, 420, P2D);
  background(64);
  noStroke();
  rectMode(CORNERS);
  ellipseMode(RADIUS);
  start = millis();
}

void draw() {
  println((stop - millis()) / 1000.0);

  background(32);

  fill(45);
  rect(width * 0.15, height * 0.15, 
    width * 0.5, height * 0.825, 10);

  fill(map(mouseX, 0, width, 127, 255), 
    127, 
    map(mouseY, 0, height, 127, 255));

  ellipse(map(mouseX, 0, width, 
    width * 0.15 + min(width, height) * 0.062, 
    width * 0.5 - min(width, height) * 0.062), 
    map(mouseY, 0, height, 
    height * 0.15 + min(width, height) * 0.062, 
    height * 0.825 - min(width, height) * 0.062), 
    min(width, height) * 0.062, 
    min(width, height) * 0.062);

  // Track.
  beginShape();
  fill(64);
  vertex(width, height);
  fill(0);
  vertex(0, height);
  fill(0);
  vertex(0, height - 15);
  fill(64);
  vertex(width, height - 15);
  endShape(CLOSE);

  // Timer Bar
  beginShape();
  fill(255, 0, 0);
  vertex(map(stop - millis(), stop, start, width, 0), height);
  fill(255, 255, 0);
  vertex(0, height);
  fill(255, 255, 0);
  vertex(0, height - 15);
  fill(255, 0, 0);
  vertex(map(stop - millis(), stop, start, width, 0), height - 15);
  endShape(CLOSE);
}