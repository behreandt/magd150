int start = 0;
int stop = 0;
int elapsed = 0;

void setup() {
  size(420, 420);
  background(32);

  // You can also change the frame rate.
  // This helps in adjusting global animation speed.
  frameRate(12);

  // You can create a timer by taking a snapshot of the
  // time the program has been running in milliseconds at
  // a certain event. Later on, you can assign the updated
  // amount of millis() to a variable stop and then subtract
  // start from stop to get elapsed.
  start = millis();
  println("start: " + start);
}

void draw() {
  background(32);

  // Duration can be measured in two ways in Processing:
  // in the number of frames it takes for something to happen,
  // in the number of milliseconds (1/1000th of a second) it
  // takes for something to happen.
  // The code executed in void draw() { } happens in one frame.

  // You can check the frameRate by printing it to the
  // console. Notice that it is a floating decimal point
  // number, not an integer, and may not be exactly 60.

  println("frames: " + frameCount + ", milliseconds: " + millis() + ", frameRate: " + frameRate);

  // You can use the modulo operator to establish an interval
  // during which an event occurs.
  if (frameCount % 50 == 0) {
    fill(255, 127, 0);
    ellipse(width / 2.0, height / 2.0, 210, 210);
    stop = millis();
    elapsed = stop - start;
    start = stop;
    println("elapsed: " + elapsed);
  }
}