// Any line which has two forward slashes at the start
// is a comment. You can use it to write notes to yourself
// or to someone else. The computer skips over comments.

/* You can also write comments which last for multiple lines
 by putting a forward slash and asterisk before the comment
 and an asterisk forward slash after the comment. */

// If we click run without typing anything, we get a little
// gray box. Let's change the size of that window. We have
// to use whole numbers to specify the number of pixels.

// size(width, height);
size(400, 200);

// We could delete lines we don't like, but instead, let's
// comment them out until we know for sure we don't need them.

// size(200, 400); // There can be only one size(w, h);
// size(200); // Only one number, size needs two numbers.
// size(200 400); // This needs a comma between the numbers.
// size(200, 400) // No semi-colon!
// size200, 300); // No left parenthesis!

// Now let's give the window background color
// background(color); where color is a range
// from 0 (black) to 255 (white). we'll get into more
// complex color later.
background(255);
// background(0);
// background(204);