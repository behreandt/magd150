size(420, 420);
background(32);
ellipseMode(CENTER);
rectMode(CENTER);
noStroke();

// Circle in upper-left corner.
fill(64);
ellipse(105, 105, 200, 200);
fill(74);
ellipse(110, 95, 175, 175);
fill(84);
ellipse(115, 85, 150, 150);
fill(94);
ellipse(120, 75, 125, 125);
fill(104);
ellipse(125, 65, 100, 100);
fill(114);
ellipse(130, 55, 75, 75);

// Square in upper-right corner.
fill(255);
rect(315, 105, 200, 200);
fill(0);
rect(315, 105, 175, 175);
fill(223.125);
rect(315, 105, 150, 150);
fill(31.875);
rect(315, 105, 125, 125);
fill(191.25);
rect(315, 105, 100, 100);
fill(63.75);
rect(315, 105, 75, 75);
fill(170);
rect(315, 105, 50, 50);
fill(85);
rect(315, 105, 25, 25);
fill(127.5);
rect(315, 105, 12.5, 12.5);

// Pattern in bottom-left corner.
fill(223);
ellipse(105, 315, 200, 200);

fill(85);
ellipse(165, 315, 75, 75);
fill(75);
ellipse(105, 375, 75, 75);
fill(65);
ellipse(45, 315, 75, 75);
fill(55);
ellipse(105, 255, 75, 75);

fill(245);
ellipse(65, 315, 25, 25);
ellipse(45, 295, 25, 25);
ellipse(25, 315, 25, 25);
ellipse(45, 335, 25, 25);

fill(235);
ellipse(145, 315, 25, 25);
ellipse(185, 315, 25, 25);
ellipse(165, 295, 25, 25);
ellipse(165, 335, 25, 25);

fill(225);
ellipse(85, 255, 25, 25);
ellipse(125, 255, 25, 25);
ellipse(105, 235, 25, 25);
ellipse(105, 275, 25, 25);

fill(215);
ellipse(85, 375, 25, 25);
ellipse(125, 375, 25, 25);
ellipse(105, 355, 25, 25);
ellipse(105, 395, 25, 25);

// Pattern in bottom-right corner.
fill(0);
rect(315, 315, 200, 200, 5);
fill(25.5);
ellipse(310, 315, 195, 195);
fill(51);
rect(305, 315, 137.886, 137.886, 10);
fill(76.5);
ellipse(300, 315, 134.438, 134.428);
fill(102);
rect(295, 315, 95.055, 95.055, 15);
fill(127.5);
ellipse(290, 315, 92.679, 92.679);
fill(153);
rect(285, 315, 65.534, 65.534, 20);
fill(178.5);
ellipse(280, 315, 63.895, 63.895);
fill(204);
rect(275, 315, 45.181, 45.181, 25);
fill(255);
ellipse(270, 315, 33, 33);