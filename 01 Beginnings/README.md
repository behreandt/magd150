# Assignment 1
## Beginnings
### 100 points

![Beginnings](preview.png)

### References

* Daniel Shiffman, [_Learning Processing_](http://learningprocessing.com/examples/) Chapters 1 - 2
* Shiffman, [Video: Introduction](https://youtu.be/2VLaIr5Ckbs?list=PLRqwX-V7Uu6ZYJC7L-r6rX6utt6wwJCyi)
* Shiffman, [Video: Pixels](https://youtu.be/a562vsSI2Po?list=PLRqwX-V7Uu6bsRnSEJ9tRn4V_XCGXovs4)
* Shiffman, [Video: Processing Environment](https://youtu.be/5N31KNgOO0g?list=PLRqwX-V7Uu6Yo4VdQ4ZTtqRQ1AE4t_Ep9)
* [Tutorial: Drawing](https://processing.org/tutorials/drawing/)

### Instructions

1. Adhere to the theme to be determined in lab. (10 pts.)
    * Wednesday: Buildings
    * Monday: Outer-Space

2. Create a sketch with a minimum [size](https://processing.org/reference/size_.html) of 128 by 128 pixels. (10 pts.)

3. Give the sketch a [background](https://processing.org/reference/background_.html). (10 pts.)

4. Use only grayscale (0 - 255) colors for [stroke](https://processing.org/reference/stroke_.html) and [fill](https://processing.org/reference/fill_.html). (10 pts.)

5. Use at least two of each of the following primitives: (10 pts.)
    * [Point](https://processing.org/reference/point_.html)
    * [Line](https://processing.org/reference/line_.html)
    * [Ellipse](https://processing.org/reference/ellipse_.html)
    * [Rectangle](https://processing.org/reference/rect_.html)

6. Use primitives with either [no stroke](https://processing.org/reference/noStroke_.html) or [no fill](https://processing.org/reference/noFill_.html). (10 pts.)

7. Change the display mode for an [ellipse](https://processing.org/reference/ellipseMode_.html), [rectangle](https://processing.org/reference/rectMode_.html), stroke [cap](https://processing.org/reference/strokeCap_.html) or [join](https://processing.org/reference/strokeJoin_.html). (10 pts.)

8. Compose with the following in mind: (10 pts.)
    * Symmetry: If the sketch were folded, would it look the same on both sides of the fold?
    * Contrast: How much do the elements of the sketch stand out or blend in relationship to each other? In mathematical terms, what is the minimum and maximum difference between the fill and stroke color of the elements?
    * Grouping: Are like or unlike elements grouped together, repeated at regular or irregular intervals? Are they combined to form a symbol or representational image?
    * Scale: How do the relative sizes of the elements influence our perception of them?

9. Ensure that the sketch runs without errors. The main tab of the sketch should be the only tab with global-scope setup and draw functions. (10 pts.)

10. Ensure that the main tab of the sketch shares the same name as the sketch folder. Name the sketch folder and main tab according to the following convention: s17_magd150_lab01_yourlastname. Compress the sketch folder into a .zip file and upload to D2L. (10 pts.)

__Any code or media within a submission are presumed to be the work of the student(s) unless otherwise stated. When media or code from outside sources are used, a link to the source and notice of attribution at the top of the main sketch in a comment should be provided. Media and code used are expected to be from the intellectual commons (i.e., not copyrighted or registered as a trademark) and to be altered substantially by the student(s). On first incident, students whose work does not meet this standard will have one week to resubmit the assignment for half credit. On second incident, students will be referred to Student Conduct Programs and will fail the course.__
