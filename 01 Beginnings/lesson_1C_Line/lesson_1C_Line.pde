size(420, 420);
background(255);

strokeWeight(1);
// line(x1, y1, x2, y2);
line(10, 2, 350, 342);

// strokeCap(SPECIALWORD);
// Your options for the word to go inbetween
// the parentheses are:
// ROUND, SQUARE, PROJECT
// ROUND is the default option.
strokeCap(SQUARE);
strokeWeight(5);
line(10, 15, 350, 355);

strokeCap(ROUND);
strokeWeight(10);
line(10, 35, 350, 375);

strokeCap(PROJECT);
strokeWeight(15);
line(10, 65, 350, 405);

strokeCap(ROUND);
strokeWeight(20);
line(10, 105, 350, 445);