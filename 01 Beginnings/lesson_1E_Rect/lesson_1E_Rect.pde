size(420, 420);
background(32);

noStroke();
fill(64);
// rect(topLeftCornerX, topLeftCornerY, width, height);
rect(40, 20, 100, 100);

// rect(topLeftCornerX, topLeftCornerY, width, height, cornerRounding);
fill(74);
rect(160, 20, 100, 100, 15);

// rect(topLeftCornerX, topLeftCornerY, width, height,
// TLCornerRounding, TRCornerRounding, BRCornerRounding, BRCornerRounding);
fill(84);
rect(280, 20, 100, 100, 15, 25, 35, 45);



// rectMode(SPECIALMODE); CENTER, CORNER (default), CORNERS, RADIUS
// changes how the numbers you enter into rect() are interpreted
rectMode(CORNERS);
strokeWeight(15);
// strokeJoin(SPECIALMODE); MITER (default), BEVEL, ROUND
// changes how the stroke is drawn at corners
strokeJoin(MITER);
stroke(255);
fill(94);
// rect(topLeftCornerX, topLeftCornerY,
// bottomRightCornerX, bottomRightCornerY);
rect(40, 270, 140, 370);

rectMode(CENTER);
strokeJoin(BEVEL);
fill(104);
// rect(centerX, centerY, width, height);
rect(210, 320, 100, 100);

rectMode(RADIUS);
strokeJoin(ROUND);
fill(114);
// rect(centerX, centerY, radius, radius);
rect(330, 320, 50, 50);