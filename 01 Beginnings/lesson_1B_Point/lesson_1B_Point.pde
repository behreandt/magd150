size(420, 420);
background(255);

// strokeWeight(thickness);
// The larger the number, the more thick the stroke.
strokeWeight(2);

// Think of the display window as a graphing calculator,
// except the origin (0, 0) is in the upper left corner.
// x increases as you travel to the right.
// y increases as you travel to the bottom.
// point(x, y);
point(10, 10);
point(10, 50);
point(50, 50);
point(50, 10);

strokeWeight(4);
point(20, 30);

strokeWeight(8);
point(60, 40);

strokeWeight(10);
point(80, 120);

strokeWeight(20);
point(240, 160);

strokeWeight(25);
point(320, 420);

strokeWeight(30);
point(420, 420);

strokeWeight(35);
point(420, 320);